package unit.bloop.recycling;

import org.junit.Test;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import bloop.recycling.AbstractRecyclable;
import bloop.recycling.Recycler;
import bloop.recycling.SimpleRecycler;

public class RecyclerTests
{
	@Test
	public void typicalUse()
	{
		Recycler<SomethingRecyclable> recycler = new SimpleRecycler<>(SomethingRecyclable.class, 10);
		assertNull(recycler.get("something"));
		
		Set<SomethingRecyclable> recycledObjects = new HashSet<>();
		for(int i = 0; i < 10; i++)
		{
			SomethingRecyclable something = new SomethingRecyclable("something" + i);
			recycler.recycle(something);
			recycledObjects.add(something);
			assertTrue(something.isRecycled());
			assertEquals(i + 1, recycler.getSize());
		}
		
		for(int i = 0; i < 10; i++)
		{
			SomethingRecyclable something = recycler.get("something" + (10 + i));
			assertFalse(something.isRecycled());
			assertTrue(recycledObjects.contains(something));
			assertEquals(10 - i - 1, recycler.getSize());
		}
	}
	
	public static class SomethingRecyclable extends AbstractRecyclable
	{
		private String string;
		
		protected SomethingRecyclable()
		{
		}
		
		public SomethingRecyclable(String string)
		{
			this.string = string;
		}

		@Override
		public void fill(Object... arguments)
		{
			super.fill(arguments);
			if (arguments.length == 1)
				string = (String) arguments[0];
			else
				throw new IllegalArgumentException();
		}

		public String getString()
		{
			return string;
		}

		public void setString(String string)
		{
			this.string = string;
		}
	}
}
