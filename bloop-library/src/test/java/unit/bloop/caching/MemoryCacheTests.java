package unit.bloop.caching;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import static org.junit.Assert.*;

import bloop.caching.CacheCleaner;
import bloop.caching.CacheLoader;
import bloop.caching.CacheRecord;
import bloop.caching.MemoryCache;

public class MemoryCacheTests
{
	@Test
	public void memoryCache_noLimit_noLoader_strongRecord_timeCleaner()
	{
		MemoryCache<String, String> cache = new MemoryCache.Builder<String, String>().build();
		assertNull(cache.get("key1"));
		assertFalse(cache.isCached("key1"));

		cache.cacheDirect("key1", "value1");
		assertEquals("value1", cache.get("key1"));
		assertEquals("value1", cache.getIfCached("key1"));
		
		cache.cacheDirect("key1", "valueX");
		assertEquals("valueX", cache.get("key1"));
		assertEquals("valueX", cache.getIfCached("key1"));
		assertTrue(cache.isCached("key1"));

		cache.uncache("key1");
		assertNull(cache.get("key1"));
		assertFalse(cache.isCached("key1"));
	}
	
	@Test
	public void memoryCache_limit5_noLoader_strongRecord_timeCleaner()
	{
		MemoryCache<String, String> cache = new MemoryCache.Builder<String, String>()
				.limit(5)
				.build();

		for(int i = 0; i < 10; i++)
		{
			cache.cacheDirect("key" + i, "value" + i);
		}
		assertEquals(5, cache.getSize());
		assertEquals("value1", cache.get("key1"));
		assertNull(cache.get("key6"));
	}
	
	@Test
	public void memoryCache_noLimit_loader_strongRecord_timeCleaner()
	{
		BogusCacheLoader loader = new BogusCacheLoader();
		MemoryCache<String, String> cache = new MemoryCache.Builder<String, String>()
				.loader(loader)
				.build();
		
		assertNull(cache.getIfCached("key1"));
		assertEquals("key1:text", cache.get("key1"));
		assertEquals("key1:text", cache.getIfCached("key1"));
		
		loader.reload = true;
		
		assertEquals("key1:text2", cache.reload("key1"));
		assertEquals("key1:text2", cache.get("key1"));
		assertEquals("key1:text2", cache.getIfCached("key1"));
	}
	
	@Test
	public void memoryCache_limit5_loader_strongRecord_timeCleaner()
	{
		BogusCacheLoader loader = new BogusCacheLoader();
		MemoryCache<String, String> cache = new MemoryCache.Builder<String, String>()
				.loader(loader)
				.limit(5)
				.build();
		
		for(int i = 0; i < 10; i++)
		{
			assertEquals("key" + i + ":text", cache.get("key" + i));
		}
		
		assertEquals(5, cache.getSize());
		assertEquals("key1:text", cache.get("key1"));
		assertEquals("key6:text", cache.get("key6"));
		assertNull(cache.getIfCached("key6"));

		loader.reload = true;
		
		assertEquals("key1:text2", cache.reload("key1"));
		assertEquals("key1:text2", cache.get("key1"));
		assertEquals("key1:text2", cache.getIfCached("key1"));
		
		assertEquals("key6:text2", cache.reload("key6"));
		assertNull(cache.getIfCached("key6"));
	}
	
	@Test
	public void memoryCache_noLimit_loader_strongRecord_bogusCleaner()
	{
		MemoryCache<String, String> cache = new MemoryCache.Builder<String, String>()
				.loader(new BogusCacheLoader())
				.cleaner(new BogusCacheCleaner())
				.build();
		
		for(int i = 0; i < 10; i++)
		{
			assertEquals("key" + i + ":text", cache.get("key" + i));
		}
		
		cache.clean();
		assertEquals(5, cache.getSize());
		assertEquals("key0:text", cache.get("key0"));
		assertEquals("key2:text", cache.get("key2"));
		assertEquals("key4:text", cache.get("key4"));
		assertEquals("key6:text", cache.get("key6"));
		assertEquals("key8:text", cache.get("key8"));
	}
	
	public static class BogusCacheLoader implements CacheLoader<String, String>
	{
		private boolean reload = false;
		
		@Override
		public String load(String key)
		{
			if(reload)
				return key + ":text2";
			else
				return key + ":text";
		}

		@Override
		public Map<String, String> loadAll(Iterable<? extends String> keys)
		{
			Map<String, String> map = new HashMap<>();
			for(String key : keys)
			{
				if(reload)
					map.put(key, key + ":text2");
				else
					map.put(key, key + ":text");
			}
			return map;
		}
	}
	
	public static class BogusCacheCleaner implements CacheCleaner<String, String>
	{
		@Override
		public boolean shouldDelete(String key, CacheRecord<String> record)
		{
			if(Integer.valueOf(key.substring(3)) % 2 == 1)
				return true;
			else
				return false;
		}
	}

}
