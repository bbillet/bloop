package unit.bloop.state;

import org.junit.Test;

import bloop.state.FileStateStack;
import bloop.state.MemoryStateStack;
import static org.junit.Assert.*;


public class StackStateTests
{
	@Test
	public void memoryStackStateTests() throws Exception
	{
		MemoryStateStack<String> text = new MemoryStateStack<>();
		
		text.push("Hello, I am a text (rev. 1)");
		assertEquals(1, text.getSize());
		text.push("Hello, I am a text (rev. 2)");
		assertEquals(2, text.getSize());
		text.push("Hello, I am a text (rev. 3)");
		assertEquals(3, text.getSize());
		text.push("Hello, I am a text (rev. 4)");
		assertEquals(4, text.getSize());
		
		assertEquals("Hello, I am a text (rev. 4)", text.getCurrentState());
		
		assertEquals("Hello, I am a text (rev. 4)", text.pop());
		assertEquals("Hello, I am a text (rev. 3)", text.getCurrentState());

		assertEquals("Hello, I am a text (rev. 3)", text.pop());
		assertEquals("Hello, I am a text (rev. 2)", text.getCurrentState());

		assertEquals("Hello, I am a text (rev. 2)", text.pop());
		assertEquals("Hello, I am a text (rev. 1)", text.getCurrentState());
		
		assertEquals("Hello, I am a text (rev. 1)", text.pop());
		assertNull(text.getCurrentState());

		assertNull(text.pop());
		assertEquals(0, text.getSize());
	}
	
	@Test
	public void boundedMemoryStackStateTests() throws Exception
	{
		MemoryStateStack<String> text = new MemoryStateStack<>(2);
		
		text.push("Hello, I am a text (rev. 1)");
		assertEquals(1, text.getSize());
		text.push("Hello, I am a text (rev. 2)");
		assertEquals(2, text.getSize());
		text.push("Hello, I am a text (rev. 3)");
		assertEquals(2, text.getSize());
		text.push("Hello, I am a text (rev. 4)");
		assertEquals(2, text.getSize());
		
		assertEquals("Hello, I am a text (rev. 4)", text.getCurrentState());
		
		assertEquals("Hello, I am a text (rev. 4)", text.pop());
		assertEquals("Hello, I am a text (rev. 3)", text.getCurrentState());

		assertEquals("Hello, I am a text (rev. 3)", text.pop());
		assertNull(text.getCurrentState());

		assertNull(text.pop());
		assertNull(text.getCurrentState());
		
		assertNull(text.pop());
		assertNull(text.getCurrentState());

		assertNull(text.pop());
		assertEquals(0, text.getSize());
	}
	
	@Test
	public void fileStackStateTests() throws Exception
	{
		FileStateStack<String> text = new FileStateStack<>();
		
		text.push("Hello, I am a text (rev. 1)");
		assertEquals(1, text.getSize());
		text.push("Hello, I am a text (rev. 2)");
		assertEquals(2, text.getSize());
		text.push("Hello, I am a text (rev. 3)");
		assertEquals(3, text.getSize());
		text.push("Hello, I am a text (rev. 4)");
		assertEquals(4, text.getSize());
		
		assertEquals("Hello, I am a text (rev. 4)", text.getCurrentState());
		
		assertEquals("Hello, I am a text (rev. 4)", text.pop());
		assertEquals("Hello, I am a text (rev. 3)", text.getCurrentState());

		assertEquals("Hello, I am a text (rev. 3)", text.pop());
		assertEquals("Hello, I am a text (rev. 2)", text.getCurrentState());

		assertEquals("Hello, I am a text (rev. 2)", text.pop());
		assertEquals("Hello, I am a text (rev. 1)", text.getCurrentState());
		
		assertEquals("Hello, I am a text (rev. 1)", text.pop());
		assertNull(text.getCurrentState());

		assertNull(text.pop());
		assertEquals(0, text.getSize());
		
		text.clear();
	}
	
	@Test
	public void boundedFileStackStateTests() throws Exception
	{
		FileStateStack<String> text = new FileStateStack<>(2);

		text.push("Hello, I am a text (rev. 1)");
		assertEquals(1, text.getSize());
		text.push("Hello, I am a text (rev. 2)");
		assertEquals(2, text.getSize());
		text.push("Hello, I am a text (rev. 3)");
		assertEquals(2, text.getSize());
		text.push("Hello, I am a text (rev. 4)");
		assertEquals(2, text.getSize());
		
		assertEquals("Hello, I am a text (rev. 4)", text.getCurrentState());
		
		assertEquals("Hello, I am a text (rev. 4)", text.pop());
		assertEquals("Hello, I am a text (rev. 3)", text.getCurrentState());

		assertEquals("Hello, I am a text (rev. 3)", text.pop());
		assertNull(text.getCurrentState());

		assertNull(text.pop());
		assertNull(text.getCurrentState());
		
		assertNull(text.pop());
		assertNull(text.getCurrentState());

		assertNull(text.pop());
		assertEquals(0, text.getSize());
		
		text.clear();
	}
}
