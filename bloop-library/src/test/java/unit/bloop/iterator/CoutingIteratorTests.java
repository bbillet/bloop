package unit.bloop.iterator;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

import org.junit.Test;

import bloop.iterator.CountingIterator;

public class CoutingIteratorTests
{	
	@Test
	public void typicalUse() throws Exception
	{
		List<String> list = new ArrayList<>();
		list.add("a");
		list.add("b");
		list.add("c");
		CountingIterator<String> it = new CountingIterator<>(list.iterator());
		
		it.next();
		assertEquals(1, it.getCount());
		
		it.next();
		it.remove();
		assertEquals(2, it.getCount());
		assertEquals(1, it.getRemovedCount());
	}
}
