package unit.bloop.iterator;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

import org.junit.Test;

import bloop.iterator.CloningIterator;

public class CloningIteratorTests
{	
	@Test
	public void typicalUse() throws Exception
	{
		List<CloneableString> list = new ArrayList<>();
		list.add(new CloneableString("a"));
		list.add(new CloneableString("b"));
		list.add(new CloneableString("c"));
		CloningIterator<CloneableString> it = new CloningIterator<>(list.iterator(), CloneableString.class);
		
		CloneableString str = it.next();
		assertEquals("a", str.string);
		assertNotEquals(list.get(0).hashCode(), str.hashCode());
		
		str = it.next();
		assertEquals("b", str.string);
		assertNotEquals(list.get(1).hashCode(), str.hashCode());
		
		str = it.next();
		assertEquals("c", str.string);
		assertNotEquals(list.get(2).hashCode(), str.hashCode());
	}
	
	@Test
	public void failureCase() throws Exception
	{
		List<NonCloneable> list = new ArrayList<>();
		try
		{
			new CloningIterator<>(list.iterator(), NonCloneable.class);
			fail();
		}
		catch(CloneNotSupportedException e) { }
	}
	
	public static class CloneableString implements Cloneable
	{
		private String string;
		
		public CloneableString(String string)
		{
			this.string = string;
		}

		public CloneableString clone()
		{
			return new CloneableString(string);
		}
	}
	
	public static class NonCloneable implements Cloneable
	{
		// a bogus Cloneable implementation, which does not make the clone()
		// method public
	}
}
