package unit.bloop.java.io;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Test;

import bloop.java.io.CountingOutputStream;

public class CountingOutputStreamTests
{
	private ByteArrayOutputStream baos = new ByteArrayOutputStream();
	private CountingOutputStream os = new CountingOutputStream(baos);
	
	@After
	public void tearDown() throws IOException
	{
		baos.reset();
	}
	
	@Test
	public void writeOneByte_thenOneByte() throws Exception
	{
		os.write('a');
		assertEquals(1, os.getCount());
		
		os.write('b');
		assertEquals(2, os.getCount());
		
		assertEquals("ab", new String(baos.toByteArray(), StandardCharsets.UTF_8));
	}
	
	@Test
	public void writeTenBytes() throws Exception
	{
		byte[] buf = new byte[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j' };
		os.write(buf);
		assertEquals("abcdefghij", new String(baos.toByteArray(), StandardCharsets.UTF_8));
		assertEquals(10, os.getCount());
	}
}
