package unit.bloop.java.io;

import java.io.IOException;
import java.io.StringWriter;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bloop.java.io.CountingWriter;

public class CountingWriterTests
{
	private StringWriter strWriter = new StringWriter();
	private CountingWriter writer = null;
	
	@Before
	public void setUp()
	{
		writer = new CountingWriter(strWriter);
	}
	
	@After
	public void tearDown() throws IOException
	{
		writer.close();
		writer = null;
	}
	
	@Test
	public void writeOneChar_thenOneChar() throws Exception
	{
		writer.write('a');
		assertEquals(1, writer.getCount());
		
		writer.write('b');
		assertEquals(2, writer.getCount());
		
		assertEquals("ab", strWriter.getBuffer().toString());
	}
	
	@Test
	public void writeTenChars() throws Exception
	{
		char[] buf = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j' };
		writer.write(buf);
		assertEquals("abcdefghij", strWriter.getBuffer().toString());
		assertEquals(10, writer.getCount());
	}
}
