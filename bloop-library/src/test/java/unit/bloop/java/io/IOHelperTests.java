package unit.bloop.java.io;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.*;

import org.junit.Test;

import bloop.java.io.IOHelper;

public class IOHelperTests
{	
	@Test
	public void readExact_typical() throws IOException
	{
		ByteArrayInputStream is = new ByteArrayInputStream("this is a string".getBytes(StandardCharsets.UTF_8));
		
		byte[] buf = new byte[10];
		int read = IOHelper.readExact(is, buf);
		assertEquals(10, read);
		assertEquals("this is a ", new String(buf, StandardCharsets.UTF_8));
		
		is.close();
	}
	
	@Test
	public void readExact_limits() throws IOException
	{
		ByteArrayInputStream is = new ByteArrayInputStream("this is a string".getBytes(StandardCharsets.UTF_8));
		
		byte[] buf = new byte[0];
		int read = IOHelper.readExact(is, buf);
		assertEquals(0, read);
		
		buf = new byte[32];
		read = IOHelper.readExact(is, buf);
		assertEquals(16, read);
		assertEquals("this is a string", new String(buf, 0, read, StandardCharsets.UTF_8));

		is.close();
	}
	
	@Test
	public void ignore_typical() throws IOException
	{
		ByteArrayInputStream is = new ByteArrayInputStream("this is a string".getBytes(StandardCharsets.UTF_8));
		
		int ignored = IOHelper.ignore(is, 10);
		assertEquals(10, ignored);
		assertEquals('s', (char) is.read());
		
		is.close();
	}
	
	@Test
	public void ignore_limits() throws IOException
	{
		ByteArrayInputStream is = new ByteArrayInputStream("this is a string".getBytes(StandardCharsets.UTF_8));
		
		int ignored = IOHelper.ignore(is, 0);
		assertEquals(0, ignored);
		assertEquals('t', (char) is.read());
		
		ignored = IOHelper.ignore(is, 32);
		assertEquals(15, ignored);
		
		is.close();
	}
	
	@Test
	public void readUntilPattern_typical() throws IOException
	{
		ByteArrayInputStream is = new ByteArrayInputStream("this is a string\r\nwith two lines".getBytes(StandardCharsets.UTF_8));
		
		String line = IOHelper.readUntilPattern(is, new byte[] { '\r', '\n' }, StandardCharsets.UTF_8);
		assertEquals("this is a string", line);
		
		line = IOHelper.readUntilPattern(is, new byte[] { '\r', '\n' }, StandardCharsets.UTF_8);
		assertEquals("with two lines", line);
		
		line = IOHelper.readUntilPattern(is, new byte[] { '\r', '\n' }, StandardCharsets.UTF_8);
		assertNull(line);
		
		is.close();
	}
	
	@Test
	public void readAll_typical() throws IOException
	{
		ByteArrayInputStream is = new ByteArrayInputStream("this is a string".getBytes(StandardCharsets.UTF_8));
		
		String content = IOHelper.readAll(is, 32, StandardCharsets.UTF_8);
		assertEquals("this is a string", content);
		
		is.reset();	
		
		content = IOHelper.readAll(is, 4, StandardCharsets.UTF_8);
		assertEquals("this is a string", content);
		
		is.reset();	
		
		ByteArrayOutputStream os = IOHelper.readAllBytesAsStream(is, 32);
		content = new String(os.toByteArray(), 0, os.size(), StandardCharsets.UTF_8);
		assertEquals("this is a string", content);

		is.close();
	}
	
	@Test
	public void copyAll_typical() throws IOException
	{
		ByteArrayInputStream is = new ByteArrayInputStream("this is a string".getBytes(StandardCharsets.UTF_8));
		ByteArrayOutputStream os = new ByteArrayOutputStream();

		IOHelper.copyAll(is, os, 32);
		String content = new String(os.toByteArray(), 0, os.size(), StandardCharsets.UTF_8);
		assertEquals("this is a string", content);
		
		is.reset();
		
		IOHelper.copyAll(is, os, 32);
		content = new String(os.toByteArray(), 0, os.size(), StandardCharsets.UTF_8);
		assertEquals("this is a stringthis is a string", content);

		is.close();
	}
}
