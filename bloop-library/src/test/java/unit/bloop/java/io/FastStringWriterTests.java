package unit.bloop.java.io;

import static org.junit.Assert.*;

import org.junit.Test;

import bloop.java.io.FastStringWriter;

public class FastStringWriterTests
{	
	@Test
	public void typicalUse() throws Exception
	{
		try(FastStringWriter writer = new FastStringWriter())
		{
			writer.write('a');
			writer.write(new char[] { 'b', 'c' });
			writer.write("defghij");
			writer.append('k');
			writer.append("lmnopqrstuvwxyz");
			
			String result = writer.toString();
			assertEquals("abcdefghijklmnopqrstuvwxyz", result);
		}
	}
}
