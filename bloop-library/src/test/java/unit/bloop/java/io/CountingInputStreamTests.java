package unit.bloop.java.io;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bloop.java.io.CountingInputStream;

public class CountingInputStreamTests
{
	private byte[] data = "this is a string".getBytes(StandardCharsets.UTF_8);
	private CountingInputStream is = null;
	
	@Before
	public void setUp()
	{
		is = new CountingInputStream(new ByteArrayInputStream(data));
	}
	
	@After
	public void tearDown() throws IOException
	{
		is.close();
		is = null;
	}
	
	@Test
	public void readOneByte_thenOneByte() throws Exception
	{
		int read = is.read();
		assertEquals('t', read);
		assertEquals(1, is.getCount());
		
		read = is.read();
		assertEquals('h', read);
		assertEquals(2, is.getCount());
	}
	
	@Test
	public void readTenBytes() throws Exception
	{
		byte[] buf = new byte[10];
		int read = is.read(buf);
		assertEquals(10, read);
		assertEquals("this is a ", new String(buf, StandardCharsets.UTF_8));
		assertEquals(10, is.getCount());
	}
	
	@Test
	public void markAfterTenBytes() throws Exception
	{
		byte[] buf = new byte[10];
		
		is.read(buf);
		is.mark(50);
		is.read();
		
		assertEquals(11, is.getCount());
		
		is.reset();
		assertEquals(10, is.getCount());
		
		int read = is.read();
		assertEquals('s', read);
		assertEquals(11, is.getCount());
	}
}
