package unit.bloop.java.io;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;

import bloop.java.io.MultiPrintStream;

public class MultiPrintStreamTests
{	
	@Test
	public void typicalUse() throws Exception
	{
		ByteArrayOutputStream sysout = new ByteArrayOutputStream();
		System.setOut(new PrintStream(sysout));
		
		ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
		ByteArrayOutputStream baos2 = new ByteArrayOutputStream();

		MultiPrintStream printer = new MultiPrintStream(true, 
				new PrintStream(baos1),
				new PrintStream(baos2));
		printer.print("abcdefg");
		printer.flush();
		
		assertEquals("abcdefg", sysout.toString());
		assertEquals("abcdefg", baos1.toString());
		assertEquals("abcdefg", baos2.toString());
		
		printer.println("hijklmnopqrstuvwxyz");
		printer.flush();
		
		assertEquals("abcdefghijklmnopqrstuvwxyz" + System.lineSeparator(), sysout.toString());
		assertEquals("abcdefghijklmnopqrstuvwxyz" + System.lineSeparator(), baos1.toString());
		assertEquals("abcdefghijklmnopqrstuvwxyz" + System.lineSeparator(), baos2.toString());

		printer.close();
	}
}
