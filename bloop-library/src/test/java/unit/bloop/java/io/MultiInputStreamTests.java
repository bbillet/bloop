package unit.bloop.java.io;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

import org.junit.Test;

import bloop.java.io.IOHelper;
import bloop.java.io.MultiInputStream;

public class MultiInputStreamTests
{	
	@Test
	public void typicalUse() throws Exception
	{
		MultiInputStream is = new MultiInputStream(
				new ByteArrayInputStream("abcdefg".getBytes(StandardCharsets.UTF_8)),
				new ByteArrayInputStream("hijklmn".getBytes(StandardCharsets.UTF_8)),
				new ByteArrayInputStream("opqrstu".getBytes(StandardCharsets.UTF_8)),
				new ByteArrayInputStream("vwxyz".getBytes(StandardCharsets.UTF_8)));
		
		String s = IOHelper.readAll(is, 32, StandardCharsets.UTF_8);
		assertEquals("abcdefghijklmnopqrstuvwxyz", s);
		
		is.close();
	}
}
