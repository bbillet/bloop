package unit.bloop.java.io;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;

import org.junit.Test;

import bloop.java.io.MultiOutputStream;

public class MultiOutputStreamTests
{	
	@Test
	public void typicalUse() throws Exception
	{
		ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
		ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
		
		MultiOutputStream os = new MultiOutputStream(baos1, baos2);
		Writer writer = new OutputStreamWriter(os, StandardCharsets.UTF_8);
		writer.write("abcdefg");
		writer.flush();
		
		assertEquals("abcdefg", baos1.toString());
		assertEquals("abcdefg", baos2.toString());
		
		writer.write("hijklmnopqrstuvwxyz");
		writer.flush();
		
		assertEquals("abcdefghijklmnopqrstuvwxyz", baos1.toString());
		assertEquals("abcdefghijklmnopqrstuvwxyz", baos2.toString());

		os.close();
	}
}
