package unit.bloop.java.io;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;

import bloop.java.io.TabulatedPrintStream;

public class TabulatedPrintStreamTests
{	
	@Test
	public void typicalUse() throws Exception
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		TabulatedPrintStream printer = new TabulatedPrintStream(new PrintStream(baos), 0);
		printer.print("abcdefg");
		printer.flush();
		
		assertEquals("abcdefg", baos.toString());
		
		printer.incrementTabLevel();
		
		printer.println("hijklmnopqrstuvwxyz");
		printer.flush();
		
		assertEquals("abcdefghijklmnopqrstuvwxyz" + System.lineSeparator(), baos.toString());
		
		printer.print("abcdefg");
		printer.flush();
		
		assertEquals("abcdefghijklmnopqrstuvwxyz" + System.lineSeparator() + "\tabcdefg", baos.toString());
		
		printer.println();
		printer.print("hijklm");
		printer.print("nopqrstuvwxyz");
		printer.flush();
				
		assertEquals("abcdefghijklmnopqrstuvwxyz" + System.lineSeparator() + "\tabcdefg" + System.lineSeparator() + "\thijklmnopqrstuvwxyz", baos.toString());

		printer.close();
	}
}
