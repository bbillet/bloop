package unit.bloop.java.io;

import static org.junit.Assert.*;

import org.junit.Test;

import bloop.java.io.FastCharArrayWriter;

public class FastCharArrayWriterTests
{	
	@Test
	public void typicalUse() throws Exception
	{
		try(FastCharArrayWriter writer = new FastCharArrayWriter())
		{
			writer.write('a');
			writer.write(new char[] { 'b', 'c' });
			writer.write("defghij");
			writer.append('k');
			writer.append("lmnopqrstuvwxyz");
			
			String result = writer.toString();
			assertEquals("abcdefghijklmnopqrstuvwxyz", result);
			assertEquals("abcdefghijklmnopqrstuvwxyz", new String(writer.toCharArray()));
		}
	}
}
