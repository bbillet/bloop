package unit.bloop.java.io;

import java.io.IOException;
import java.io.StringReader;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bloop.java.io.CountingReader;

public class CountingReaderTests
{
	private CountingReader reader = null;
	
	@Before
	public void setUp()
	{
		reader = new CountingReader(new StringReader("this is a string"));
	}
	
	@After
	public void tearDown() throws IOException
	{
		reader.close();
		reader = null;
	}
	
	@Test
	public void readOneChar_thenOneChar() throws Exception
	{
		int read = reader.read();
		assertEquals('t', read);
		assertEquals(1, reader.getCount());
		
		read = reader.read();
		assertEquals('h', read);
		assertEquals(2, reader.getCount());
	}
	
	@Test
	public void readTenChars() throws Exception
	{
		char[] buf = new char[10];
		int read = reader.read(buf);
		assertEquals(10, read);
		assertEquals("this is a ", new String(buf));
		assertEquals(10, reader.getCount());
	}
	
	@Test
	public void markAfterTenChars() throws Exception
	{
		char[] buf = new char[10];
		
		reader.read(buf);
		reader.mark(50);
		reader.read();
		
		assertEquals(11, reader.getCount());
		
		reader.reset();
		assertEquals(10, reader.getCount());
		
		int read = reader.read();
		assertEquals('s', read);
		assertEquals(11, reader.getCount());
	}
}
