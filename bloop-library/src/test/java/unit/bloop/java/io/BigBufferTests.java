package unit.bloop.java.io;

import java.io.File;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.*;

import org.junit.Test;

import bloop.java.io.BigBuffer;
import bloop.java.io.IOHelper;

public class BigBufferTests
{	
	@Test
	public void bigBuffer_thresholdEquals10() throws Exception
	{
		try(BigBuffer buffer = new BigBuffer(10))
		{
			buffer.write("abcde".getBytes(StandardCharsets.UTF_8));
			assertNull(buffer.getOutputFile());
			
			try(InputStream input = buffer.getInputStream())
			{
				String s = IOHelper.readAll(buffer.getInputStream(), 32, StandardCharsets.UTF_8);
				assertEquals("abcde", s);
			}
			
			buffer.write("fghijklmnopqrstuvwxyz".getBytes(StandardCharsets.UTF_8));
			buffer.flush();
			File file = buffer.getOutputFile();
			assertNotNull(file);
			assertTrue(file.exists());
			
			try(InputStream input = buffer.getInputStream())
			{
				String s = IOHelper.readAll(buffer.getInputStream(), 32, StandardCharsets.UTF_8);
				assertEquals("abcdefghijklmnopqrstuvwxyz", s);
			}
			
			buffer.clear();
			assertFalse(file.exists());
			
			try(InputStream input = buffer.getInputStream())
			{
				assertEquals("", IOHelper.readAll(buffer.getInputStream(), 32, StandardCharsets.UTF_8));
			}
		}
	}
}
