package unit.bloop.java.nio;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.CharBuffer;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import bloop.java.io.CharIOHelper;
import bloop.java.nio.CharBufferReader;
import bloop.java.nio.CharBufferWriter;
import static org.junit.Assert.*;


public class CharBufferTests
{
	@Test
	public void inputStreamTests() throws Exception
	{
		List<CharBuffer> input = Arrays.asList(
				CharBuffer.wrap("this is ".toCharArray()),
				CharBuffer.wrap("a ".toCharArray()),
				CharBuffer.wrap("string!".toCharArray()));
		
		StringWriter writer = new StringWriter();
		CharBufferReader reader = new CharBufferReader(input);
		CharIOHelper.copyAll(reader, writer, 32);
		
		assertEquals("this is a string!", writer.toString());		
	}
	
	@Test
	public void outputStreamTests() throws Exception
	{
		CharBuffer b1 = CharBuffer.allocate(16);
		CharBuffer b2 = CharBuffer.allocate(16);
		CharBuffer b3 = CharBuffer.allocate(16);
		
		CharBufferWriter writer = new CharBufferWriter(b1, b2, b3);
		writer.write('t'); // single char
		writer.write("his is a string!".toCharArray());
		writer.write(" And ".toCharArray());
		writer.write("this is another string!".toCharArray());
		
		char[] data = new char[16];
		
		b1.flip();
		b1.get(data);
		assertEquals("this is a string", new String(data));
		
		b2.flip();
		b2.get(data);
		assertEquals("! And this is an", new String(data));
		
		b3.flip();
		b3.get(data, 0, b3.limit());
		assertEquals("other string!", new String(data, 0, b3.limit()));
		
		// switch to write mode
		int oldLimit = b3.limit();
		b3.clear();
		b3.position(oldLimit);
		try
		{
			// overflow the stream
			writer.write(" Woot!".toCharArray());
			fail();
		}
		catch(IOException e) { }
		
		b3.flip();
		b3.get(data, 0, b3.limit());
		
		assertEquals("other string! Wo", new String(data));
		
		writer.close();
	}
}
