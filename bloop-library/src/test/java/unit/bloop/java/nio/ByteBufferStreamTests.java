package unit.bloop.java.nio;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import bloop.java.io.IOHelper;
import bloop.java.nio.ByteBufferInputStream;
import bloop.java.nio.ByteBufferOutputStream;
import static org.junit.Assert.*;


public class ByteBufferStreamTests
{
	@Test
	public void inputStreamTests() throws Exception
	{
		List<ByteBuffer> input = Arrays.asList(
				ByteBuffer.wrap("this is ".getBytes(StandardCharsets.UTF_8)),
				ByteBuffer.wrap("a ".getBytes(StandardCharsets.UTF_8)),
				ByteBuffer.wrap("string!".getBytes(StandardCharsets.UTF_8)));
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ByteBufferInputStream bbis = new ByteBufferInputStream(input);
		IOHelper.copyAll(bbis, baos, 32);
		
		assertEquals("this is a string!", baos.toString());		
	}
	
	@Test
	public void outputStreamTests() throws Exception
	{
		ByteBuffer b1 = ByteBuffer.allocate(16);
		ByteBuffer b2 = ByteBuffer.allocate(16);
		ByteBuffer b3 = ByteBuffer.allocate(16);
		
		ByteBufferOutputStream bbos = new ByteBufferOutputStream(b1, b2, b3);
		bbos.write('t'); // single byte
		bbos.write("his is a string!".getBytes(StandardCharsets.UTF_8));
		bbos.write(" And ".getBytes(StandardCharsets.UTF_8));
		bbos.write("this is another string!".getBytes(StandardCharsets.UTF_8));
		
		byte[] data = new byte[16];
		
		b1.flip();
		b1.get(data);
		assertEquals("this is a string", new String(data, StandardCharsets.UTF_8));
		
		b2.flip();
		b2.get(data);
		assertEquals("! And this is an", new String(data, StandardCharsets.UTF_8));
		
		b3.flip();
		b3.get(data, 0, b3.limit());
		assertEquals("other string!", new String(data, 0, b3.limit(), StandardCharsets.UTF_8));
		
		// switch to write mode
		int oldLimit = b3.limit();
		b3.clear();
		b3.position(oldLimit);
		try
		{
			// overflow the stream
			bbos.write(" Woot!".getBytes(StandardCharsets.UTF_8));
			fail();
		}
		catch(IOException e) { }
		
		b3.flip();
		b3.get(data, 0, b3.limit());
		
		assertEquals("other string! Wo", new String(data, StandardCharsets.UTF_8));
		
		bbos.close();
	}
}
