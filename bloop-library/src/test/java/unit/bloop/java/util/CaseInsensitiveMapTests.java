package unit.bloop.java.util;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.Test;

import bloop.java.util.CaseInsensitiveMap;
import bloop.java.util.MapBuilder;

public class CaseInsensitiveMapTests
{	
	@Test
	public void typicalUse() throws Exception
	{
		Map<String, String> map = new CaseInsensitiveMap<>();
		map.put("KEY1", "VALUE1");
		map.put("key1", "value1");
		assertEquals(1, map.size());
		assertEquals("value1", map.get("KEY1"));
		assertEquals("value1", map.get("key1"));
		assertEquals("value1", map.get("kEy1"));
		
		Map<String, String> anotherMap = new MapBuilder<String, String>()
				.entry("Key2", "Value2")
				.entry("key3", "value3")
				.entry("KEY3", "VALUE3")
				.build();
		
		map.putAll(anotherMap);
		assertEquals(3, map.size());
		assertEquals("Value2", map.get("key2"));
		assertEquals("VALUE3", map.get("key3"));
	}
}
