package unit.bloop.java.util;

import static org.junit.Assert.*;

import java.util.BitSet;

import org.junit.Test;

import bloop.java.util.Bytes;

public class BytesTests
{	
	@Test
	public void intShortLongConversions() throws Exception
	{
		byte[] shortBytes = new byte[] {
				(byte) 0b00000101, 
				(byte) 0b00111001
		};
		short aShort = Bytes.toShort(shortBytes);
		assertEquals(1337, aShort);
		
		
		byte[] intBytes = new byte[] {
				(byte) 0b00000111,
				(byte) 0b11111000, 
				(byte) 0b10010100, 
				(byte) 0b00001001
		};
		int anInt = Bytes.toInt(intBytes);
		assertEquals(133731337, anInt);
		
		
		byte[] longBytes = new byte[] {
				(byte) 0, 
				(byte) 0b00000001, 
				(byte) 0b00011101, 
				(byte) 0b00000010, 
				(byte) 0b11011110, 
				(byte) 0b01010001, 
				(byte) 0b01110110, 
				(byte) 0b00001001
		};
		long aLong = Bytes.toLong(longBytes);
		assertEquals(313373133731337l, aLong);
		
		
		byte[] bytes = Bytes.shortToBytes(aShort);
		assertArrayEquals(shortBytes, bytes);
		
		bytes = Bytes.intToBytes(anInt);
		assertArrayEquals(intBytes, bytes);
		
		bytes = Bytes.longToBytes(aLong);
		assertArrayEquals(longBytes, bytes);
	}
	
	@Test
	public void bitsets() throws Exception
	{
		byte b = 0b01010101;
		assertTrue(Bytes.isBitSet(b, 0));
		assertFalse(Bytes.isBitSet(b, 1));
		assertTrue(Bytes.isBitSet(b, 2));
		assertFalse(Bytes.isBitSet(b, 3));
		assertTrue(Bytes.isBitSet(b, 4));
		assertFalse(Bytes.isBitSet(b, 5));
		assertTrue(Bytes.isBitSet(b, 6));
		assertFalse(Bytes.isBitSet(b, 7));
		
		BitSet bitset = new BitSet(8);
		Bytes.toBitset(b, bitset, 0, 8);
		assertEquals(b, bitset.toByteArray()[0]);
		
		byte b2 = Bytes.bitsetToByte(bitset);
		assertEquals(b, b2);
	}
}
