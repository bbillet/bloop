package unit.bloop.java.util;

import static org.junit.Assert.*;

import org.junit.Test;

import bloop.java.util.Strings;

public class StringsTests
{	
	@Test
	public void concatTests() throws Exception
	{
		assertEquals("abcdef", Strings.concat("ab", "c", "def"));
		assertEquals("abnullcdef", Strings.concat("ab", null, "cdef"));
		assertEquals("abcdef", Strings.concat("abcdef"));
		assertEquals("null", Strings.concat((String) null));
	}
	
	@Test
	public void delimitTests() throws Exception
	{
		assertEquals("ab,c,def", Strings.delimit(",", "ab", "c", "def"));
		assertEquals("ab,null,cdef", Strings.delimit(",", "ab", null, "cdef"));
		assertEquals("abcdef", Strings.delimit(",", "abcdef"));
		assertEquals("null", Strings.delimit(",", (String) null));
	}
	
	@Test
	public void lengthTests() throws Exception
	{
		assertEquals(6, Strings.length("ab", "c", "def"));
		assertEquals(10, Strings.length("ab", null, "cdef"));
		assertEquals(4, Strings.length((String) null));
	}
	
	@Test
	public void emptynessTests() throws Exception
	{
		assertTrue(Strings.isNullOrEmpty(null));
		assertTrue(Strings.isNullOrEmpty(""));
		assertFalse(Strings.isNullOrEmpty(" "));
		assertFalse(Strings.isNullOrEmpty("abcdef"));
		
		assertFalse(Strings.isNotNullOrEmpty(null));
		assertFalse(Strings.isNotNullOrEmpty(""));
		assertTrue(Strings.isNotNullOrEmpty(" "));
		assertTrue(Strings.isNotNullOrEmpty("abcdef"));
		
		assertTrue(Strings.isNullOrWhitespace(null));
		assertTrue(Strings.isNullOrWhitespace(""));
		assertTrue(Strings.isNullOrWhitespace(" \t\n\r"));
		assertFalse(Strings.isNullOrWhitespace("abcdef"));
		
		assertFalse(Strings.isNotNullOrWhitespace(null));
		assertFalse(Strings.isNotNullOrWhitespace(""));
		assertFalse(Strings.isNotNullOrWhitespace(" \t\n\r"));
		assertTrue(Strings.isNotNullOrWhitespace("abcdef"));

	}
	
	@Test
	public void trimTests() throws Exception
	{
		assertNull(Strings.trim(null));
		
		assertEquals("abcdef", Strings.trim("abcdef"));
		assertEquals("abcdef", Strings.trim("abcdef", null));
		assertEquals("abcdef", Strings.trim("abcdef", 'x'));
		
		assertEquals("bcde", Strings.trim("abcdef", 'a', 'f'));
		assertEquals("abcde", Strings.trim("abcdef", 'f'));
		assertEquals("bcdef", Strings.trim("abcdef", 'a'));
		
		assertEquals("abcdef", Strings.leftTrim("abcdef", 'f'));
		assertEquals("abcdef", Strings.rightTrim("abcdef", 'a'));
		
		assertEquals("", Strings.trim("aaaaa", 'a'));
		assertEquals("", Strings.leftTrim("aaaaa", 'a'));
		assertEquals("", Strings.rightTrim("aaaaa", 'a'));
	}
	
	@Test
	public void padTests() throws Exception
	{
		assertEquals("aaaaabcdef", Strings.leftPad("bcdef", 'a', 10));
		assertEquals("abcdefffff", Strings.rightPad("abcde", 'f', 10));
		assertEquals("abcdef", Strings.leftPad("abcdef", 'a', 0));
		assertEquals("abcdef", Strings.rightPad("abcdef", 'f', 0));
	}
	
	@Test
	public void startsWithTests() throws Exception
	{
		assertTrue(Strings.startsWithIgnoreCase("abcdef", "a"));
		assertTrue(Strings.startsWithIgnoreCase("ABCDEF", "a"));
		assertTrue(Strings.startsWithIgnoreCase("abcdef", "A"));
		assertTrue(Strings.startsWithIgnoreCase("ABCDEF", "A"));
		assertFalse(Strings.startsWithIgnoreCase("abcdef", "x"));
		assertFalse(Strings.startsWithIgnoreCase("abcdef", null));
	}
	
	@Test
	public void endsWithTests() throws Exception
	{
		assertTrue(Strings.endsWithIgnoreCase("abcdef", "f"));
		assertTrue(Strings.endsWithIgnoreCase("ABCDEF", "f"));
		assertTrue(Strings.endsWithIgnoreCase("abcdef", "F"));
		assertTrue(Strings.endsWithIgnoreCase("ABCDEF", "F"));
		assertFalse(Strings.endsWithIgnoreCase("abcdef", "x"));
		assertFalse(Strings.endsWithIgnoreCase("abcdef", null));
	}

	@Test
	public void capitalizeTests() throws Exception
	{
		assertEquals("Abcdef", Strings.firstUpper("abcdef", false));
		assertEquals("ABCDEF", Strings.firstUpper("ABCDEF", false));
		assertEquals("Abcdef", Strings.firstUpper("Abcdef", false));

		assertEquals("Abcdef", Strings.capitalize("abcdef"));
		assertEquals("Abcdef", Strings.capitalize("ABCDEF"));
		assertEquals("Abcdef", Strings.capitalize("Abcdef"));
	}
	
	@Test
	public void countTests() throws Exception
	{
		assertEquals(1, Strings.countOccurences("abcdef", "c"));
		assertEquals(3, Strings.countOccurences("abcccdef", "c"));
		assertEquals(0, Strings.countOccurences("abcdef", "x"));
		assertEquals(0, Strings.countOccurences("abcdef", null));
	}
	
	@Test
	public void stringBetweenTests() throws Exception
	{
		assertEquals("cd", Strings.stringBetween("abcdef", "ab", "ef"));
		assertEquals("cdef", Strings.stringBetween("abcdef", "ab", null));
		assertEquals("cdef", Strings.stringBetween("abcdef", "ab", ""));
		assertEquals("abcd", Strings.stringBetween("abcdef", "", "ef"));
		assertEquals("abcd", Strings.stringBetween("abcdef", null, "ef"));
		assertNull(Strings.stringBetween("abcdef", "xx", "ef"));
		assertNull(Strings.stringBetween("abcdef", "ab", "xx"));
		assertEquals("abcdef", Strings.stringBetween("abcdef", null, null));
		assertEquals("abcdef", Strings.stringBetween("abcdef", "", ""));
	}
}
