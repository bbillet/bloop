package unit.bloop.java.util;

import static org.junit.Assert.*;

import java.nio.charset.StandardCharsets;

import org.junit.Test;

import bloop.java.util.Hex;

public class HexTests
{	
	@Test
	public void encode() throws Exception
	{
		byte[] data = "this is a string!".getBytes(StandardCharsets.UTF_8);
		String hexEncoded = Hex.encodeHexString(data);
		assertEquals("74686973206973206120737472696e6721", hexEncoded.toLowerCase());
	}
	
	@Test
	public void decode() throws Exception
	{
		byte[] data = Hex.decodeHexString("74686973206973206120737472696e6721");
		assertEquals("this is a string!", new String(data, StandardCharsets.UTF_8));
	}
}
