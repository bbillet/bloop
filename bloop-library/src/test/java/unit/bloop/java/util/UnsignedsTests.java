package unit.bloop.java.util;

import static org.junit.Assert.*;

import java.math.BigInteger;

import org.junit.Test;

import bloop.java.util.Bytes;
import bloop.java.util.Unsigneds;

public class UnsignedsTests
{	
	@Test
	public void toUnsigned() throws Exception
	{
		byte[] shortBytes = new byte[] {
				(byte) 0b10000000, 
				0
		};
		int aShort1 = Bytes.toShort(shortBytes);
		assertEquals(-32768, aShort1);
		int aShort2 = Unsigneds.toUnsignedShort(shortBytes);
		assertEquals(32768, aShort2);
		
		
		byte[] intBytes = new byte[] {
				(byte) 0b10000000,
				0, 
				0, 
				0
		};
		long anInt1 = Bytes.toInt(intBytes);
		assertEquals(-2147483648, anInt1);
		long anInt2 = Unsigneds.toUnsignedInt(intBytes);
		assertEquals(2147483648l, anInt2);
		
		
		byte[] longBytes = new byte[] {
				(byte) 0b10000000, 
				0, 
				0, 
				0, 
				0, 
				0, 
				0, 
				0
		};
		long aLong1 = Bytes.toLong(longBytes);
		assertEquals(-9223372036854775808l, aLong1);
		BigInteger aLong2 = Unsigneds.toUnsignedLong(longBytes);
		assertEquals(new BigInteger("9223372036854775808"), aLong2);
	}
}
