package unit.bloop.java.util;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Test;

import bloop.java.util.AutoKeyMap;
import bloop.java.util.AutoKeyMap.KeyGetter;

public class AutoKeyMapTests
{	
	@Test
	public void typicalUse() throws Exception
	{
		AutoKeyMap<String, Something> map = new AutoKeyMap<>(new HashMap<String, Something>(), new Getter());
		map.put(new Something("key1", "a", "b"));
		map.put(new Something("key2", "c", "d"));
		
		assertEquals(2, map.size());
		assertEquals("a", map.get("key1").getValue1());
		assertEquals("c", map.get("key2").getValue1());
	}
	
	private static class Getter implements KeyGetter<String, Something>
	{
		@Override
		public String getKey(Something value)
		{
			return value.getKey();
		}
	}
	
	protected static class Something
	{
		private String key;
		private String value1;
		private String value2;
		
		public Something(String key, String value1, String value2)
		{
			this.key = key;
			this.value1 = value1;
			this.value2 = value2;
		}

		public String getKey()
		{
			return key;
		}

		public String getValue1()
		{
			return value1;
		}

		public String getValue2()
		{
			return value2;
		}
	}
}
