package unit.bloop.java.util;

import static org.junit.Assert.*;

import org.junit.Test;

import bloop.java.util.DualMaps;
import bloop.java.util.DualMaps.DualMap;

public class DualMapTests
{	
	@Test
	public void typicalUse() throws Exception
	{
		DualMap<String, String> dualMap = DualMaps.dualHashMap();
		dualMap.put("key1", "value1");
		dualMap.put("key2", "value2");
		
		assertEquals(2, dualMap.size());
		assertEquals("value1", dualMap.get("key1"));
		assertEquals("value2", dualMap.get("key2"));
		assertEquals("key1", dualMap.reverseGet("value1"));
		assertEquals("key2", dualMap.reverseGet("value2"));
		
		dualMap.put("key3", "value1");
		
		assertEquals(2, dualMap.size());
		assertEquals("value1", dualMap.get("key3"));
		assertEquals("value2", dualMap.get("key2"));
		assertEquals("key2", dualMap.reverseGet("value2"));
		assertEquals("key3", dualMap.reverseGet("value1"));
		
		dualMap.put("key1", "key1");
		assertEquals(3, dualMap.size());
		assertEquals("key1", dualMap.get("key1"));
		assertEquals("value1", dualMap.get("key3"));
		assertEquals("value2", dualMap.get("key2"));
		assertEquals("key1", dualMap.reverseGet("key1"));
		assertEquals("key2", dualMap.reverseGet("value2"));
		assertEquals("key3", dualMap.reverseGet("value1"));
	}
}
