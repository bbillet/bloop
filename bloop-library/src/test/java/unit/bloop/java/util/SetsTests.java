package unit.bloop.java.util;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import org.junit.Test;

import bloop.java.util.Sets;

public class SetsTests
{	
	@Test
	public void testUnion() throws Exception
	{
		SortedSet<String> set1 = new TreeSet<>(Arrays.asList("a", "b", "c"));
		SortedSet<String> set2 = new TreeSet<>(Arrays.asList("d", "e", "f"));
		SortedSet<String> resultSet = new TreeSet<>(Arrays.asList("a", "b", "c", "d", "e", "f"));
		areSetEquals(resultSet, new TreeSet<>(Sets.union(set1, set2)));
		
		set1.clear();
		set2.clear();
		resultSet.clear();
		areSetEquals(resultSet, new TreeSet<>(Sets.union(set1, set2)));
	}
	
	@Test
	public void testIntersection() throws Exception
	{
		SortedSet<String> set1 = new TreeSet<>(Arrays.asList("a", "b", "c"));
		SortedSet<String> set2 = new TreeSet<>(Arrays.asList("d", "e", "f"));
		SortedSet<String> resultSet = new TreeSet<>();
		areSetEquals(resultSet, new TreeSet<>(Sets.intersection(set1, set2)));
		
		set1 = new TreeSet<>(Arrays.asList("a", "b", "c"));
		set2 = new TreeSet<>(Arrays.asList("b", "c", "d"));
		resultSet = new TreeSet<>(Arrays.asList("b", "c"));
		areSetEquals(resultSet, new TreeSet<>(Sets.intersection(set1, set2)));
		
		set1.clear();
		set2.clear();
		resultSet.clear();
		areSetEquals(resultSet, new TreeSet<>(Sets.intersection(set1, set2)));
	}
	
	@Test
	public void testDifference() throws Exception
	{
		SortedSet<String> set1 = new TreeSet<>(Arrays.asList("a", "b", "c"));
		SortedSet<String> set2 = new TreeSet<>(Arrays.asList("d", "e", "f"));
		SortedSet<String> resultSet = new TreeSet<>(Arrays.asList("a", "b", "c"));
		areSetEquals(resultSet, new TreeSet<>(Sets.difference(set1, set2)));
		
		set1 = new TreeSet<>(Arrays.asList("a", "b", "c"));
		set2 = new TreeSet<>(Arrays.asList("b", "c", "d"));
		resultSet = new TreeSet<>(Arrays.asList("a"));
		areSetEquals(resultSet, new TreeSet<>(Sets.difference(set1, set2)));
		
		set1.clear();
		set2.clear();
		resultSet.clear();
		areSetEquals(resultSet, new TreeSet<>(Sets.difference(set1, set2)));
	}
	
	@Test
	public void testReverseDifference() throws Exception
	{
		SortedSet<String> set1 = new TreeSet<>(Arrays.asList("a", "b", "c"));
		SortedSet<String> set2 = new TreeSet<>(Arrays.asList("d", "e", "f"));
		SortedSet<String> resultSet = new TreeSet<>(Arrays.asList("d", "e", "f"));
		areSetEquals(resultSet, new TreeSet<>(Sets.reverseDifference(set1, set2)));
		
		set1 = new TreeSet<>(Arrays.asList("a", "b", "c"));
		set2 = new TreeSet<>(Arrays.asList("b", "c", "d"));
		resultSet = new TreeSet<>(Arrays.asList("d"));
		areSetEquals(resultSet, new TreeSet<>(Sets.reverseDifference(set1, set2)));
		
		set1.clear();
		set2.clear();
		resultSet.clear();
		areSetEquals(resultSet, new TreeSet<>(Sets.reverseDifference(set1, set2)));
	}
	
	@Test
	public void testSymmetricDifference() throws Exception
	{
		SortedSet<String> set1 = new TreeSet<>(Arrays.asList("a", "b", "c"));
		SortedSet<String> set2 = new TreeSet<>(Arrays.asList("d", "e", "f"));
		SortedSet<String> resultSet = new TreeSet<>(Arrays.asList("a", "b", "c", "d", "e", "f"));
		areSetEquals(resultSet, new TreeSet<>(Sets.union(set1, set2)));

		set1 = new TreeSet<>(Arrays.asList("a", "b", "c"));
		set2 = new TreeSet<>(Arrays.asList("b", "c", "d"));
		resultSet = new TreeSet<>(Arrays.asList("a", "d"));
		areSetEquals(resultSet, new TreeSet<>(Sets.symmetricDifference(set1, set2)));
		
		set1.clear();
		set2.clear();
		resultSet.clear();
		areSetEquals(resultSet, new TreeSet<>(Sets.symmetricDifference(set1, set2)));
	}	
	
	private <T> void areSetEquals(SortedSet<T> set1, SortedSet<T> set2)
	{
		assertEquals(set1.size(), set2.size());
		Iterator<T> it1 = set1.iterator();
		Iterator<T> it2 = set2.iterator();
		while(it1.hasNext())
		{
			assertEquals(it1.next(), it2.next());
		}
	}
}
