package unit.bloop.java.util;

import static org.junit.Assert.*;

import org.junit.Test;

import bloop.java.util.Arrays;

public class ArraysTests
{	
	@Test
	public void merge() throws Exception
	{
		byte[] byteArray1 = new byte[] { 1, 2 };
		byte[] byteArray2 = new byte[] { 3, 4, 5 };
		assertArrayEquals(
				new byte[] { 1, 2, 3, 4, 5 },
				Arrays.merge(byteArray1, byteArray2));
		
		short[] shortArray1 = new short[] { 1, 2 };
		short[] shortArray2 = new short[] { 3, 4, 5 };
		assertArrayEquals(
				new short[] { 1, 2, 3, 4, 5 },
				Arrays.merge(shortArray1, shortArray2));
		
		int[] intArray1 = new int[] { 1, 2 };
		int[] intArray2 = new int[] { 3, 4, 5 };
		assertArrayEquals(
				new int[] { 1, 2, 3, 4, 5 },
				Arrays.merge(intArray1, intArray2));
		
		long[] longArray1 = new long[] { 1, 2 };
		long[] longArray2 = new long[] { 3, 4, 5 };
		assertArrayEquals(
				new long[] { 1, 2, 3, 4, 5 },
				Arrays.merge(longArray1, longArray2));
		
		float[] floatArray1 = new float[] { 1, 2 };
		float[] floatArray2 = new float[] { 3, 4, 5 };
		assertArrayEquals(
				new float[] { 1, 2, 3, 4, 5 },
				Arrays.merge(floatArray1, floatArray2), 0);
		
		double[] doubleArray1 = new double[] { 1, 2 };
		double[] doubleArray2 = new double[] { 3, 4, 5 };
		assertArrayEquals(
				new double[] { 1, 2, 3, 4, 5 },
				Arrays.merge(doubleArray1, doubleArray2), 0);
		
		boolean[] booleanArray1 = new boolean[] { true, false };
		boolean[] booleanArray2 = new boolean[] { false, true, true };
		assertArrayEquals(
				new boolean[] { true, false, false, true, true },
				Arrays.merge(booleanArray1, booleanArray2));
		
		char[] charArray1 = new char[] { '1', '2' };
		char[] charArray2 = new char[] { '3', '4', '5' };
		assertArrayEquals(
				new char[] { '1', '2', '3', '4', '5' },
				Arrays.merge(charArray1, charArray2));
		
		String[] stringArray1 = new String[] { "1", "2" };
		String[] stringArray2 = new String[] { "3", "4", "5" };
		assertArrayEquals(
				new String[] { "1", "2", "3", "4", "5" },
				Arrays.merge(stringArray1, stringArray2));
	}
}
