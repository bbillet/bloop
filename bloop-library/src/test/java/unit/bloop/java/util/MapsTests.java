package unit.bloop.java.util;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import bloop.java.util.Maps;

public class MapsTests
{	
	@Test
	public void putAllRaw() throws Exception
	{
		Map<String, String> map = new HashMap<>();
		Maps.putAllRaw(map, "key1", "value1", "key2", "value2");
		assertEquals(2, map.size());
		assertEquals("value1", map.get("key1"));
		assertEquals("value2", map.get("key2"));
	}
	
	@Test
	public void putAll() throws Exception
	{
		Map<String, String> map = new HashMap<>();
		Maps.putAll(map, 
				new String[] { "key1", "key2" }, 
				new String[] { "value1", "value2"});
		assertEquals(2, map.size());
		assertEquals("value1", map.get("key1"));
		assertEquals("value2", map.get("key2"));
	}
}
