package unit.bloop.java.util;

import static org.junit.Assert.*;

import java.util.Comparator;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Map.Entry;

import org.junit.Test;

import bloop.java.util.ComparatorChain;
import static bloop.java.util.ReverseComparator.reverse;


public class ComparatorChainTests
{	
	@Test
	public void comparatorChain() throws Exception
	{
		Comparator<Entry<String, String>> comparator = new ComparatorChain<>(
				SortParameter.KEY_ASC, 
				SortParameter.VALUE_ASC);
		
		SortedSet<Entry<String, String>> entries = new TreeSet<>(comparator);
		entries.add(new EntryImpl("key2", "value2"));
		entries.add(new EntryImpl("key1", "value1"));
		entries.add(new EntryImpl("key1", "value1"));
		entries.add(new EntryImpl("key1", "value0"));
		
		Iterator<Entry<String, String>> it = entries.iterator();
		Entry<String, String> e = it.next();
		assertEquals("key1", e.getKey());
		assertEquals("value0", e.getValue());
		
		e = it.next();
		assertEquals("key1", e.getKey());
		assertEquals("value1", e.getValue());
		
		e = it.next();
		assertEquals("key2", e.getKey());
		assertEquals("value2", e.getValue());
	}
	
	@Test
	public void withReverseComparator() throws Exception
	{
		Comparator<Entry<String, String>> comparator = new ComparatorChain<>(
				SortParameter.KEY_ASC, 
				SortParameter.VALUE_ASC);
		
		SortedSet<Entry<String, String>> entries = new TreeSet<>(reverse(comparator));
		entries.add(new EntryImpl("key2", "value2"));
		entries.add(new EntryImpl("key1", "value1"));
		entries.add(new EntryImpl("key1", "value1"));
		entries.add(new EntryImpl("key1", "value0"));
		
		Iterator<Entry<String, String>> it = entries.iterator();
		Entry<String, String> e = it.next();
		assertEquals("key2", e.getKey());
		assertEquals("value2", e.getValue());
		
		e = it.next();
		assertEquals("key1", e.getKey());
		assertEquals("value1", e.getValue());
		
		e = it.next();
		assertEquals("key1", e.getKey());
		assertEquals("value0", e.getValue());
	}
	
	private static enum SortParameter implements Comparator<Entry<String, String>> 
	{
		KEY_ASC 
	    {
			@Override
			public int compare(Entry<String, String> value1, Entry<String, String> value2)
			{
				return value1.getKey().compareTo(value2.getKey());
			}
		},
	    VALUE_ASC
		{
			@Override
			public int compare(Entry<String, String> value1, Entry<String, String> value2)
			{
				return value1.getValue().compareTo(value2.getValue());
			}
		}
	};
	
	private static class EntryImpl implements Entry<String, String>
	{
		private String key;
		private String value;
		
		public EntryImpl(String key, String value)
		{
			this.key = key;
			this.value = value;
		}
		
		@Override
		public String getKey()
		{
			return key;
		}
		@Override
		public String getValue()
		{
			return value;
		}
		@Override
		public String setValue(String value)
		{
			this.value = value;
			return value;
		}
	}
}
