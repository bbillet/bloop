package perf.bloop.java.io;

import java.io.StringReader;

import org.junit.Test;

import bloop.java.io.FastStringReader;

public class FastStringReaderPerf
{
	// please note that this test is a microbenchmark, replacing
	// StringReader by FastStringReader will probably have a very
	// small impact on your performance, except in very specific
	// cases (e.g., thousands of small reads).
	
	@Test
	public void benchmark() throws Exception
	{
		int nbRuns = 100000000;
		
		long time = System.nanoTime();
		try(FastStringReader reader = new FastStringReader("abcdefghijklmnopqrstuvwxyz"))
		{
			for(int i = 0; i < nbRuns; i++)
			{
				reader.read();
			}
		}
		long delta = System.nanoTime() - time;
		System.out.println((double) delta / (1000 * 1000 * 1000));
		
		
		time = System.nanoTime();
		try(StringReader reader = new StringReader("abcdefghijklmnopqrstuvwxyz"))
		{
			for(int i = 0; i < nbRuns; i++)
			{
				reader.read();
			}
		}
		delta = System.nanoTime() - time;
		System.out.println((double) delta / (1000 * 1000 * 1000));
	}
}
