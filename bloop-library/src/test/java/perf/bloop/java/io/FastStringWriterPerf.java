package perf.bloop.java.io;

import java.io.StringWriter;

import org.junit.Test;

import bloop.java.io.FastStringWriter;

public class FastStringWriterPerf
{
	// please note that this test is a microbenchmark, replacing
	// StringWriter by FastStringWriter will probably have a very
	// small impact on your performance, except in very specific
	// cases (e.g., thousands of small writes).
	
	@Test
	public void benchmark() throws Exception
	{
		int nbRuns = 10000;
		int dataSize = 10000;
		
		long time = System.nanoTime();
		for(int i = 0; i < nbRuns; i++)
		{
			try(FastStringWriter writer = new FastStringWriter())
			{
				for(int j = 0; j < dataSize; j++)
				{
					writer.write('a');
				}
			}
		}
		long delta = System.nanoTime() - time;
		System.out.println((double) delta / (1000 * 1000 * 1000));
		
		
		time = System.nanoTime();
		for(int i = 0; i < nbRuns; i++)
		{
			try(StringWriter writer = new StringWriter())
			{
				for(int j = 0; j < dataSize; j++)
				{
					writer.write('a');
				}
			}
		}
		delta = System.nanoTime() - time;
		System.out.println((double) delta / (1000 * 1000 * 1000));
	}
}
