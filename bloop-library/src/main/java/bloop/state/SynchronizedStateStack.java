/*  
 * The Bloop Library.
 * Copyright (c) 2012 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.state;

/**
 * An implementation of {@link StateStack} that encapsulates an existing
 * {@link StateStack} and protects it against concurrent modifications.
 * 
 * @author Benjamin Billet
 * @version 0.1
 * @param <T> The type of elements managed by this stack.
 * @see StateStack
 */
public class SynchronizedStateStack<T> implements StateStack<T>
{
	private final StateStack<T> stack;
	private final Object lock = new Object();
	
	/**
	 * Creates a new {@code SynchronizedStateStack}.
	 * @param stack The stack to synchronize.
	 * @throws IllegalStateException If {@code stack} is already synchronized.
	 */
	public SynchronizedStateStack(StateStack<T> stack)
	{
		if(stack instanceof SynchronizedStateStack)
			throw new IllegalStateException("already synchronized");
		this.stack = stack;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void push(T state)
	{
		synchronized(lock)
		{
			stack.push(state);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public T pop()
	{
		synchronized(lock)
		{
			return stack.pop();
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public T pop(int n)
	{
		synchronized(lock)
		{
			return stack.pop(n);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear()
	{
		synchronized(lock)
		{
			stack.clear();
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getSize()
	{
		synchronized(lock)
		{
			return stack.getSize();
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getCurrentState()
	{
		synchronized(lock)
		{
			return stack.getCurrentState();
		}
	}
}
