/*  
 * The Bloop Library.
 * Copyright (c) 2012 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.state;

/**
 * A stack of states that can be used to restore objects to a past state (LIFO).<br>
 * When {@link StateStack#push(Object)} is called, this stack stores the given
 * object as the current state. This current state can be read by calling
 * {@link StateStack#getCurrentState()}<br>
 * By calling {@link StateStack#pop()} or {@link StateStack#pop(int)}, this
 * stack removes one or several states (the most recent ones). As a consequence,
 * the current state of the stack is rollbacked to a previous one.<br>
 * 
 * <pre>{@code
 * StateStack<String> stack = ...;
 * 
 * state.push("this is my first revision");
 * state.getCurrentState(); // returns 'this is my first revision'
 * 
 * state.push("this is my second revision");
 * state.getCurrentState(); // returns 'this is my second revision'
 * 
 * state.pop();
 * state.getCurrentState(); // returns 'this is my first revision'
 * }</pre>
 * 
 * @author Benjamin Billet
 * @version 0.1
 * @param <T> the type of elements managed by this stack.
 */
public interface StateStack<T>
{
	/**
	 * Pushes a new state on top of this stack. This state becomes the current
	 * state of the stack, which can be retrieved by calling
	 * {@link StateStack#getCurrentState()}.
	 * @param state The state.
	 */
	public void push(T state);
	
	/**
	 * Removes the last state saved at the top of this stack and returns it.
	 * @return The removed state, or {@code null} if this stack is empty.
	 */
	public T pop();
	
	/**
	 * Removes the {@code n} last states saved at the top of this stack and
	 * returns the last removed state, or {@code null} is {@code n} is greater
	 * than the size of this stack.
	 * @param n The number of elements to remove.
	 * @return The n-th removed state, or {@code null}.
	 */
	public T pop(int n);
	
	/**
	 * Returns the number of states saved in this stack.
	 */
	public int getSize();
	
	/**
	 * Removes all the states saved in this stack.
	 */
	public void clear();

	/**
	 * Returns the current state of the object managed by this stack.
	 * @return The current state, or {@code null} if there is no state pushed
	 *         into this stack.
	 */
	public T getCurrentState();
}
