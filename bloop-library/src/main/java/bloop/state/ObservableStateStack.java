/*  
 * The Bloop Library.
 * Copyright (c) 2012 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.state;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * An implementation of {@link StateStack} that encapsulates an existing
 * {@link StateStack} and manages a set of listeners. Every time the stack is
 * modified, i.e., {@link StateStack#push(Object)}, {@link StateStack#pop()},
 * {@link StateStack#pop(int)} or {@link StateStack#clear()} is called, the
 * registered listeners are notified.
 * 
 * @author Benjamin Billet
 * @version 0.1
 * @param <T> The type of elements managed by this stack.
 * @see OnStateChange
 * @see StateStack
 */
public class ObservableStateStack<T> implements StateStack<T>
{
	private final StateStack<T> stack;
	private final List<OnStateChange<T>> listeners;
	
	/**
	 * Creates a new {@code ObservableStateStack}.
	 * @param stack The stack to observe.
	 * @param listeners An array of listeners.
	 * @see OnStateChange
	 */
	@SafeVarargs
	public ObservableStateStack(StateStack<T> stack, OnStateChange<T>... listeners)
	{
		this(stack, Arrays.asList(listeners));
	}
	
	/**
	 * Creates a new {@code ObservableStateStack}.
	 * @param stack The stack to observe.
	 * @param listeners A collection of listeners.
	 * @see OnStateChange
	 */
	public ObservableStateStack(StateStack<T> stack, Collection<OnStateChange<T>> listeners)
	{
		this.listeners = new ArrayList<>(listeners);
		this.stack = stack;
	}
	
	/**
	 * Creates a new {@code ObservableStateStack}, without registered listeners.
	 * @param stack The stack to observe.
	 */
	public ObservableStateStack(StateStack<T> stack)
	{
		this.stack = stack;
		this.listeners = new ArrayList<>();
	}
	
	/**
	 * Registers a new listener to this stack.
	 * @param listener The listener.
	 * @see OnStateChange
	 */
	public void registerListener(OnStateChange<T> listener)
	{
		this.listeners.add(listener);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void push(T state)
	{
		stack.push(state);
		notifyListeners();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public T pop()
	{
		T result = stack.pop();
		notifyListeners();
		return result;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public T pop(int n)
	{
		T result = stack.pop(n);
		notifyListeners();
		return result;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear()
	{
		stack.clear();
		notifyListeners();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getSize()
	{
		return stack.getSize();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getCurrentState()
	{
		return stack.getCurrentState();
	}
	
	private void notifyListeners()
	{
		T currentState = getCurrentState();
		for(OnStateChange<T> listener : listeners)
		{
			listener.onStateChange(this, currentState);
		}
	}
	
	/**
	 * A listener for state changes in {@link ObservableStateStack}.
	 * @author Benjamin Billet
	 * @version 0.1
	 * @param <T> The type of state.
	 * @see ObservableStateStack
	 */
	public static interface OnStateChange<T>
	{
		public void onStateChange(StateStack<T> stack, T currentState);
	}
}
