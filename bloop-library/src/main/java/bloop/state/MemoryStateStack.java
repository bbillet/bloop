/*  
 * The Bloop Library.
 * Copyright (c) 2012 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.state;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * An implementation of {@link StateStack} that stores the past states in
 * memory.
 * 
 * <pre>{@code
 * StateStack<String> stack = new MemoryStateStack();
 * 
 * state.push("this is my first revision");
 * state.getCurrentState(); // returns 'this is my first revision'
 * 
 * state.push("this is my second revision");
 * state.getCurrentState(); // returns 'this is my second revision'
 * 
 * state.pop();
 * state.getCurrentState(); // returns 'this is my first revision'
 * }</pre>
 * 
 * This stack implementation has a limited size of {@link Integer#MAX_VALUE} by
 * default, which can be set using the relevant constructor. If
 * {@link StateStack#push(Object)} is called while the stack is full, the oldest
 * state will be removed before pushing the new state.
 * 
 * @author Benjamin Billet
 * @version 0.1
 * @param <T> The type of elements managed by this stack.
 * @see StateStack
 */
public class MemoryStateStack<T> implements StateStack<T>
{
	private Deque<T> states;
	private int max;
	
	/**
	 * Creates a new {@code MemoryStateStack} which can stores
	 * {@link Integer#MAX_VALUE} elements.
	 */
	public MemoryStateStack()
	{
		this(Integer.MAX_VALUE);
	}
	
	/**
	 * Creates a new {@code MemoryStateStack} which can stores
	 * {@code 'max'} elements.
	 * @param max The maximum number of elements.
	 */
	public MemoryStateStack(int max)
	{
		this.states = new ArrayDeque<>();
		this.max = max;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void push(T state)
	{
		if(states.size() == max)
			states.pollLast();
		states.push(state);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public T pop()
	{
		return states.pollFirst();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public T pop(int n)
	{
		T result = null;
		if(states.size() >= n)
		{
			for(int i = 0; i < n; i++)
			{
				result = pop();
			}
		}
		else
			states.clear();
		
		return result;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear()
	{
		states.clear();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getSize()
	{
		return states.size();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getCurrentState()
	{
		if(states.size() != 0)
			return states.peek();
		else
			return null;
	}
}
