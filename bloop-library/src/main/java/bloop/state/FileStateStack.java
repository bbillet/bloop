/*  
 * The Bloop Library.
 * Copyright (c) 2012 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.state;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import bloop.java.util.RollingInteger;

/**
 * An implementation of {@link StateStack} that stores the past states in
 * temporary files.
 * 
 * <pre>{@code
 * StateStack<String> stack = new FileStateStack();
 * 
 * state.push("this is my first revision");
 * state.getCurrentState(); // returns 'this is my first revision'
 * 
 * state.push("this is my second revision");
 * state.getCurrentState(); // returns 'this is my second revision'
 * 
 * state.pop();
 * state.getCurrentState(); // returns 'this is my first revision'
 * }</pre>
 * 
 * This stack implementation has a limited size of {@link Integer#MAX_VALUE} by
 * default, which can be set using the relevant constructor. If
 * {@link StateStack#push(Object)} is called while the stack is full, the oldest
 * state will be removed before pushing the new state.<br>
 * By default, the files are stored in the temporary folder given by the system 
 * property 'java.io.tmpdir'.
 * 
 * @author Benjamin Billet
 * @version 0.1
 * @param <T> The type of elements managed by this stack.
 * @see StateStack
 */
public class FileStateStack<T extends Serializable> implements StateStack<T>
{	
	private RollingInteger current;
	private int max;
	private int size;
	
	private String filePrefix;
	private Path stateFolder;
	
	/**
	 * Creates a new {@code FileStateStack} which can stores
	 * {@link Integer#MAX_VALUE} elements, into the operating system temporary
	 * folder given by the system property 'java.io.tmpdir'.
	 * @throws IOException If the temporary folder does not exists and cannot be
	 *             created.
	 */
	public FileStateStack() throws IOException
	{
		this(Integer.MAX_VALUE, Paths.get(System.getProperty("java.io.tmpdir")));
	}
	
	/**
	 * Creates a new {@code FileStateStack} which can stores 'max' elements,
	 * into the operating system temporary folder given by the system property
	 * 'java.io.tmpdir'.
	 * @param max The maximum number of states managed by this stack.
	 * @throws IOException If the temporary folder does not exists and cannot be
	 *             created.
	 */
	public FileStateStack(int max) throws IOException
	{
		this(max, Paths.get(System.getProperty("java.io.tmpdir")));
	}
	
	/**
	 * Creates a new {@code FileStateStack} which can stores
	 * {@link Integer#MAX_VALUE} elements, into the 'stateFolder' folder.
	 * @param stateFolder The folder that will be used to store the states.
	 * @throws IOException If 'stateFolder' does not exists and cannot be
	 *             created.
	 */
	public FileStateStack(Path stateFolder) throws IOException
	{
		this(Integer.MAX_VALUE, stateFolder);
	}
	
	/**
	 * Creates a new {@code FileStateStack} which can stores 'max' elements,
	 * into the 'stateFolder' folder.
	 * @param max The maximum number of states managed by this stack.
	 * @param stateFolder The folder that will be used to store the states.
	 * @throws IOException If 'stateFolder' does not exists and cannot be
	 *             created.
	 */
	public FileStateStack(int max, Path stateFolder) throws IOException
	{
		if(max < 1)
			throw new IllegalArgumentException("'max' must be greater than zero");
		
		this.filePrefix = UUID.randomUUID().toString();
		this.stateFolder = stateFolder;
		this.max = max;
		
		this.current = new RollingInteger(1, 1, max);
		
		Files.createDirectories(stateFolder);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void push(T state)
	{	
		try(ObjectOutputStream oos = getOutputStream(current.getValue()))
		{
			oos.writeObject(state);
			oos.flush();
			
			current.increment();
			size++;
			if(size > max)
				size = max;
		}
		catch(IOException e)
		{
			throw new IllegalStateException(e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public T pop()
	{		
		current.decrement();
		size--;
		if(size < 0)
			size = 0;
	
		T result = null;
		
		try(ObjectInputStream ois = getInputStream(current.getValue()))
		{
			result = (T) ois.readObject();
			Files.deleteIfExists(getFile(current.getValue()));
		}
		catch(FileNotFoundException e) { }
		catch(Exception e)
		{
			throw new IllegalStateException(e);
		}
		
		return result;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public T pop(int n)
	{
		T result = null;
		if(size > n)
		{
			for(int i = 0; i < n; i++)
			{
				result = pop();
			}
		}
		else
			clear();
		return result;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear()
	{
		try
		{
			current.reset();
			size = 0;
			
			try (DirectoryStream<Path> dir = Files.newDirectoryStream(stateFolder, filePrefix + "*"))
			{
				for (Path file : dir)
				{
					Files.delete(file);
				}
			}
		}
		catch (IOException e)
		{
			throw new IllegalStateException(e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getSize()
	{
		return size;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public T getCurrentState()
	{
		current.decrement();
		
		try(ObjectInputStream ois = getInputStream(current.getValue()))
		{
			return (T) ois.readObject();
		}
		catch(FileNotFoundException e) { }
		catch(Exception e)
		{
			throw new IllegalStateException(e);
		}
		finally
		{
			current.increment();
		}
		
		return null;
	}

	private Path getFile(int state)
	{
		return stateFolder.resolve(filePrefix + "." + state + ".state");
	}
	
	private ObjectOutputStream getOutputStream(int state) throws IOException
	{
		Path tmp = getFile(state);
		Files.deleteIfExists(tmp);
		return new ObjectOutputStream(new FileOutputStream(tmp.toFile()));
	}
	
	private ObjectInputStream getInputStream(int state) throws IOException
	{
		Path tmp = getFile(state);
		return new ObjectInputStream(new FileInputStream(tmp.toFile()));
	}
}
