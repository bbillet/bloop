/*  
 * The Bloop Library.
 * Copyright (c) 2012 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.caching;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeSet;

/**
 * A memory cache for maps, which maintains:
 * <ul>
 * <li>The unmodifiable version of the map.
 * <li>An array corresponding to the map values.
 * <li>An array corresponding to the map keys.
 * <li>An unmodifiable set of strings, each string being the result of
 * calling {@link String#valueOf(Object)} on each map key.
 * <li>An unmodifiable collection of strings, each string being the result of
 * calling {@link String#valueOf(Object)} on each map value.
 * <li>An array of strings, each string being the result of calling
 * {@link String#valueOf(Object)} on each map key.
 * <li>An array of strings, each string being the result of calling
 * {@link String#valueOf(Object)} on each map value.
 * </ul>
 * <b>Warning</b>: For guaranteeing cache integrity, you must not modify the
 * original map nor the cached arrays.
 * @author Benjamin Billet
 * @version 0.1
 * @param <K> The type of keys of the cached map.
 * @param <V> The type of values of the cached map.
 */
public class MapCache<K, V>
{
	private Map<K, V> map;
	private Set<String> keyStrings;
	private Collection<String> valueStrings;
	private final String[] keyStringArray;
	private final String[] valueStringArray;
	private final K[] keyArray;
	private final V[] valueArray;

	/**
	 * Creates a new cache for a map.<br>
	 * <b>Warning</b>: The map must not be modified. If the map is modified, this cache
	 * is not guaranteed to be consistent.
	 * @param map The map to cache.
	 */
	@SuppressWarnings("unchecked")
	public MapCache(Map<K, V> map)
	{
		this.map = Collections.unmodifiableMap(map);
		this.keyArray = (K[]) map.keySet().toArray();
		this.valueArray = (V[]) map.values().toArray();
		
		if(map instanceof SortedMap<?, ?>)
			this.keyStrings = new TreeSet<>();
		else
			this.keyStrings = new HashSet<>(map.size());
		
		this.valueStrings = new ArrayList<>();
		for(Entry<K, V> e : map.entrySet())
		{
			this.keyStrings.add(String.valueOf(e.getKey()));
			this.valueStrings.add(String.valueOf(e.getValue()));
		}
		this.keyStrings = Collections.unmodifiableSet(this.keyStrings);
		this.valueStrings = Collections.unmodifiableCollection(this.valueStrings);

		this.keyStringArray = this.keyStrings.toArray(new String[0]);
		this.valueStringArray = this.valueStrings.toArray(new String[0]);
	}

	/**
	 * Returns the unmodifiable version of the cached map.
	 */
	public Map<K, V> getMap()
	{
		return map;
	}
	
	/**
	 * Returns the unmodifiable set of keys.
	 */
	public Set<K> getMapKeys()
	{
		return map.keySet();
	}
	
	/**
	 * Returns the unmodifiable collection of values.
	 */
	public Set<K> getMapValues()
	{
		return map.keySet();
	}

	/**
	 * Returns the cached result of calling {@link String#valueOf(Object)} on
	 * each key of the cached map.
	 */
	public Set<String> getStringKeys()
	{
		return keyStrings;
	}
	
	/**
	 * Returns the cached result of calling {@link String#valueOf(Object)} on
	 * each value of the cached map.
	 */
	public Collection<String> getStringValues()
	{
		return valueStrings;
	}

	/**
	 * Returns the cached array containing each key of the cached map.
	 */
	public K[] getKeyArray()
	{
		return keyArray;
	}

	/**
	 * Returns the cached array containing each value of the cached map.
	 */
	public V[] getValueArray()
	{
		return valueArray;
	}

	/**
	 * Returns the cached result of calling {@link String#valueOf(Object)} on
	 * each key of the cached map.
	 */
	public String[] getStringKeyArray()
	{
		return keyStringArray;
	}
	
	/**
	 * Returns the cached result of calling {@link String#valueOf(Object)} on
	 * each value of the cached map.
	 */
	public String[] getStringValueArray()
	{
		return valueStringArray;
	}
}
