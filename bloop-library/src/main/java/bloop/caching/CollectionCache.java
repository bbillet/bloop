/*  
 * The Bloop Library.
 * Copyright (c) 2012 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.caching;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * A memory cache for collections, which maintains:
 * <ul>
 * <li>The unmodifiable version of the collection.
 * <li>An array corresponding to the collection values.
 * <li>An unmodifiable collection of strings, each string being the result of
 * calling {@link String#valueOf(Object)} on each collection value.
 * <li>An array of strings, each string being the result of calling
 * {@link String#valueOf(Object)} on each collection value.
 * </ul>
 * <b>Warning</b>: For guaranteeing cache integrity, you must not modify the
 * original collection nor the cached arrays.
 * @author Benjamin Billet
 * @version 0.1
 * @param <T> The type of elements in the cached collection.
 */
public class CollectionCache<T>
{
	private Collection<T> collection;
	private Collection<String> stringCollection;
	private final T[] array;
	private final String[] stringArray;
	
	/**
	 * Creates a new cache for a collection.<br>
	 * <b>Warning</b>: The collection must not be modified. If the collection is
	 * modified, this cache is not guaranteed to be consistent.
	 * @param collection The collection to cache.
	 */
	@SuppressWarnings("unchecked")
	public CollectionCache(Collection<T> collection)
	{
		this.collection = Collections.unmodifiableCollection(collection);
		this.array = (T[]) collection.toArray();
		
		this.stringCollection = new ArrayList<>(collection.size());
		for(T element : collection)
		{
			this.stringCollection.add(String.valueOf(element));
		}
		this.stringCollection = Collections.unmodifiableCollection(this.stringCollection);
		this.stringArray = this.stringCollection.toArray(new String[0]);
	}

	/**
	 * Returns the unmodifiable version of the cached collection.
	 */
	public Collection<T> getCollection()
	{
		return collection;
	}

	/**
	 * Returns the cached result of calling {@link String#valueOf(Object)} on
	 * each value of the cached collection.
	 */
	public Collection<String> getStringCollection()
	{
		return stringCollection;
	}

	/**
	 * Returns the cached array containing each element of the cached
	 * collection.
	 */
	public T[] getArray()
	{
		return array;
	}

	/**
	 * Returns the cached result of calling {@link String#valueOf(Object)} on
	 * each value of the cached collection.
	 */
	public String[] getStringArray()
	{
		return stringArray;
	}
	
	/**
	 * A builder for {@link CollectionCache}.
	 * @author Benjamin Billet
	 * @param <T> The type of elements in the cached collection.
	 * @version 0.1
	 */
	public static class Builder<T>
	{
		private Collection<T> collection = new ArrayList<>();
		
		/**
		 * Adds a value into the collection.
		 * @param value The value.
		 * @return This builder.
		 */
		public Builder<T> value(T value)
		{
			collection.add(value);
			return this;
		}
		
		/**
		 * Adds several values into the collection.
		 * @param values The values.
		 * @return This builder.
		 */
		@SuppressWarnings("unchecked")
		public Builder<T> values(T... values)
		{
			Collections.addAll(collection, values);
			return this;
		}
		
		/**
		 * Adds several values into the collection.
		 * @param values The values.
		 * @return This builder.
		 */
		public Builder<T> values(Collection<T> values)
		{
			collection.addAll(values);
			return this;
		}
		
		/**
		 * Builds a new {@link CollectionCache} from the values added to this builder.
		 * @return The cached set.
		 */
		public CollectionCache<T> build()
		{
			return new CollectionCache<T>(collection);
		}
	}
}
