/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.caching;

import java.util.concurrent.TimeUnit;

/**
 * A {@link CacheRecordFactory} implementation which produces records that
 * expire when they are too old, based on the insertion time or the last access
 * time.
 * @author Benjamin Billet
 * @version 0.1
 * @param <K> The type of keys managed by the cache.
 * @param <V> The type of values managed by the cache.
 */
public class LifetimeRecordFactory<K, V> implements CacheRecordFactory<K, V>
{
	private long lifetime;
	private boolean basedOnAccessTime;
	
	public LifetimeRecordFactory(int lifetime, TimeUnit unit)
	{
		this(lifetime, unit, false);
	}
	
	public LifetimeRecordFactory(int lifetime, TimeUnit unit, boolean basedOnAccessTime)
	{
		if(lifetime <= 0)
			throw new IllegalArgumentException();
		
		this.lifetime = unit.toNanos(lifetime);
		this.basedOnAccessTime = basedOnAccessTime;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public CacheRecord<V> createRecord(K key, V value)
	{
		return new LifetimeCacheRecord(value, lifetime);
	}
	
	/**
	 * A {@link CacheRecord} implementation which stores a value that will
	 * expire when too old.
	 * @author Benjamin Billet
	 * @version 0.1
	 * @param <V> The type of values stored in the record.
	 */
	class LifetimeCacheRecord extends CacheRecord<V>
	{
		private V value;
		
		public LifetimeCacheRecord(V value, long insertionTime)
		{
			super(insertionTime, insertionTime);
			this.value = value;
		}
		
		public LifetimeCacheRecord(V value)
		{
			this(value, System.nanoTime());
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public V getValue()
		{
			long time = getInsertionTime();
			if(basedOnAccessTime)
				time = getLastAccessTime();
			
			if(System.nanoTime() - time <= lifetime)
				return value;
			else
				return null;
		}
	}
}
