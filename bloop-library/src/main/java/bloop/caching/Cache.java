/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.caching;

import java.util.Map;

/**
 * Interface for a {@link Cache}.
 * @author Benjamin Billet
 * @version 0.1
 *
 * @param <K> The type of keys managed by the cache.
 * @param <V> The type of values managed by the cache.
 */
public interface Cache<K, V>
{
	/**
	 * Returns the cached value associated to this key. <br>
	 * If the {@link Cache} implementation provides auto-loading capabilities
	 * through a {@link CacheLoader}, the loaded value is cached when this
	 * method returns. There is no guarantee that the returned value is actually
	 * cached, for example if the cache has a limited size and is currently
	 * full.<br>
	 * If the {@link Cache} does not provide auto-loading capabilities,
	 * {@code null} is returned if the value does not exists into the cache.<br>
	 * In any case, the last access time of the returned record is updated.
	 * @param key The key associated to the cached value to retrieve.
	 * @return The cached value, or {@code null} if the cache does not support
	 *         auto-loading and the value does not exists into the cache.
	 */
	public V get(K key);

	/**
	 * Returns the cached values associated to a set of keys. <br>
	 * If the {@link Cache} implementation provides auto-loading capabilities
	 * through a {@link CacheLoader}, the loaded values are cached when this
	 * method returns. There is no guarantee that the returned values are
	 * actually cached, for example if the cache has a limited size and is
	 * currently full. <br>
	 * If the {@link Cache} does not provide auto-loading capabilities,
	 * {@code null} is returned for each value which does not exists into the
	 * cache.<br>
	 * In any case, the last access times of the returned records are updated.
	 * @param keys The keys associated to the cached values to retrieve.
	 * @return The cached values. May contains {@code null} values if the cache
	 *         does not support auto-loading and the value does not exists into
	 *         the cache.
	 */
	public Map<K, V> getAll(Iterable<K> keys);

	/**
	 * Returns the cached value associated to this key, if any.
	 * @param key The key associated to the cached value to retrieve.
	 * @return The cached value, or {@code null} if the value is not currently
	 *         cached.
	 */
	public V getIfCached(K key);

	/**
	 * Returns the cached values associated to a set of keys, if any.
	 * @param keys The keys associated to the cached values to retrieve.
	 * @return The cached values. May contains {@code null} for each value that
	 *         is not currently cached.
	 */
	public Map<K, V> getAllIfCached(Iterable<K> keys);

	/**
	 * Returns {@code true} if the value associated to this key is currently
	 * cached.
	 * @param key The key to check.
	 */
	public boolean isCached(K key);

	/**
	 * Reloads a cached value. <br>
	 * If the {@link Cache} implementation provides auto-loading capabilities
	 * through a {@link CacheLoader}, the value is (re)loaded and (re)inserted
	 * into the cache.<br>
	 * If the {@link Cache} does not provide auto-loading capabilities, only the
	 * last access time of the record is updated.
	 * @param key The key to reload.
	 * @return The reloaded value.
	 */
	public V reload(K key);

	/**
	 * Reloads a set of cached values. <br>
	 * If the {@link Cache} implementation provides auto-loading capabilities
	 * through a {@link CacheLoader}, the values are (re)loaded and (re)inserted
	 * into the cache.<br>
	 * If the {@link Cache} does not provide auto-loading capabilities, only the
	 * last access times of the records are updated.
	 * @param key The key to reload.
	 * @return The reloaded value.
	 */
	public Map<K, V> reload(Iterable<K> keys);

	/**
	 * Removes a value from this cache.
	 * @param key The key associated to the value to remove.
	 */
	public void uncache(K key);

	/**
	 * Removes a set of values from this cache.
	 * @param keys The keys associated to the values to remove.
	 */
	public void uncacheAll(Iterable<K> keys);

	/**
	 * Removes all the values contained into this cache.
	 */
	public void uncacheAll();

	/**
	 * Stores directly a value into the cache.
	 * @param key The key.
	 * @param value The value to store.
	 */
	public void cacheDirect(K key, V value);

	/**
	 * Stores directly a set of values into the cache.
	 * @param values The values to store.
	 */
	public void cacheAllDirect(Map<? extends K, ? extends V> values);

	/**
	 * Returns the number of element currently cached.
	 */
	public int getSize();

	/**
	 * Returns the maximum number of element that this cache can store.
	 */
	public long getLimit();

	/**
	 * Cleans the cache using the default {@link CacheCleaner}. For each element
	 * removed from the cache, all the {@link OnCleaned} listeners are called.
	 * <br>
	 * If no default cleaner is defined, the cache is not modified. In any case,
	 * the last clean time is updated.
	 * @see OnCleaned
	 */
	public void clean();

	/**
	 * Cleans the cache using a {@link CacheCleaner}. For each element removed
	 * from the cache, all the {@link OnCleaned} listeners are called.
	 * @param cleaner The cleaner.
	 * @see CacheCleaner
	 * @see OnCleaned
	 */
	public void clean(CacheCleaner<K, V> cleaner);

	/**
	 * Returns the last time (nanoseconds) this cache was cleaned.
	 */
	public long getLastCleanTime();

	/**
	 * A listener for being notified that an element was removed from a
	 * {@link Cache} after applying a {@link CacheCleaner}.
	 * @author Benjamin Billet
	 * @version 0.1
	 * @param <K> The type of keys managed by the cache.
	 * @param <V> The type of values managed by the cache.
	 */
	public static interface OnCleaned<K, V>
	{
		/**
		 * This method is called when a value is removed from the cache.
		 * @param key The key.
		 * @param value The value.
		 */
		public void onCleaned(K key, V value);
	}
	
	// TODO add a listener for being notified when a cached value is expired (i.e., garbage collected) ?
}
