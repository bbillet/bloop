/*  
 * The Bloop Library.
 * Copyright (c) 2012 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.caching;

import java.util.Map;

/**
 * A decorator for {@link Cache}, which protects an existing cache against
 * concurrent access: each method of this cache is synchronized.
 * @author Benjamin Billet
 * @version 0.1
 * @param <K> The type of keys managed by the cache.
 * @param <V> The type of values managed by the cache.
 */
public class SynchronizedCache<K, V> implements Cache<K, V>
{
	private Cache<K, V> cache;

	public SynchronizedCache(Cache<K, V> cache)
	{
		this.cache = cache;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized V get(K key)
	{
		return cache.get(key);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized Map<K, V> getAll(Iterable<K> keys)
	{
		return cache.getAll(keys);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized V getIfCached(K key)
	{
		return cache.getIfCached(key);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized Map<K, V> getAllIfCached(Iterable<K> keys)
	{
		return cache.getAllIfCached(keys);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean isCached(K key)
	{
		return cache.isCached(key);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized V reload(K key)
	{
		return cache.reload(key);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized Map<K, V> reload(Iterable<K> keys)
	{
		return cache.reload(keys);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void uncache(K key)
	{
		cache.uncache(key);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void uncacheAll(Iterable<K> keys)
	{
		cache.uncacheAll(keys);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void uncacheAll()
	{
		cache.uncacheAll();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void cacheDirect(K key, V value)
	{
		cache.cacheDirect(key, value);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void cacheAllDirect(Map<? extends K, ? extends V> elements)
	{
		cache.cacheAllDirect(elements);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized int getSize()
	{
		return cache.getSize();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized long getLimit()
	{
		return cache.getLimit();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void clean()
	{
		cache.clean();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void clean(CacheCleaner<K, V> cleaner)
	{
		cache.clean(cleaner);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized long getLastCleanTime()
	{
		return cache.getLastCleanTime();
	}
}
