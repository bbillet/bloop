/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.caching;

/**
 * Interface for a factory that creates {@link CacheRecord} objects for a
 * {@link Cache}.
 * @author Benjamin Billet
 * @version 0.1
 * @param <K> The type of keys managed by the cache.
 * @param <V> The type of values managed by the cache.
 * @see Cache
 */
public interface CacheRecordFactory<K, V>
{
	/**
	 * Creates a new record from a given key and value. This method MUST never
	 * return {@code null}. Prefer throwing a {@link RuntimeException}.
	 * @param key The key.
	 * @param value The value.
	 * @return The new record.
	 */
	public CacheRecord<V> createRecord(K key, V value);
}
