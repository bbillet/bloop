/*  
 * The Bloop Library.
 * Copyright (c) 2012 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.caching;

import java.util.Map;

/**
 * A decorator for {@link Cache}, which automatically applies the default
 * cleaner when a method is called. The
 * {@link AutoCleanCache#AutoCleanCache(Cache, long)} constructor allow you to
 * specify a minimum time between two cleaning.
 * @author Benjamin Billet
 * @version 0.1
 * @param <K> The type of keys managed by the cache.
 * @param <V> The type of values managed by the cache.
 */
public class AutoCleanCache<K, V> implements Cache<K, V>
{
	private Cache<K, V> cache;
	private long interval;
	private CacheCleaner<K, V> cleaner;

	/**
	 * Creates a new {@link AutoCleanCache} which decorates an existing
	 * {@code cache}.
	 * @param cache The cache to decorate.
	 * @param interval Minimum time spent between two cleans.
	 */
	public AutoCleanCache(Cache<K, V> cache, long interval)
	{
		this(cache, null, interval);
	}
	
	/**
	 * Creates a new {@link AutoCleanCache} which decorates an existing
	 * {@code cache}.
	 * @param cache The cache to decorate.
	 * @param cleaner A specific cleaner that will be used instead of the
	 *        default cleaner defined by {@code cache}.
	 * @param interval Minimum time spent between two cleans.
	 */
	public AutoCleanCache(Cache<K, V> cache, CacheCleaner<K, V> cleaner, long interval)
	{
		this.cache = cache;
		this.interval = interval;
		this.cleaner = cleaner;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public V get(K key)
	{
		cleanIfRequired();
		return cache.get(key);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<K, V> getAll(Iterable<K> keys)
	{
		cleanIfRequired();
		return cache.getAll(keys);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public V getIfCached(K key)
	{
		cleanIfRequired();
		return cache.getIfCached(key);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<K, V> getAllIfCached(Iterable<K> keys)
	{
		cleanIfRequired();
		return cache.getAllIfCached(keys);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isCached(K key)
	{
		cleanIfRequired();
		return cache.isCached(key);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public V reload(K key)
	{
		cleanIfRequired();
		return cache.reload(key);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<K, V> reload(Iterable<K> keys)
	{
		cleanIfRequired();
		return cache.reload(keys);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void uncache(K key)
	{
		cleanIfRequired();
		cache.uncache(key);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void uncacheAll(Iterable<K> keys)
	{
		cleanIfRequired();
		cache.uncacheAll(keys);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void uncacheAll()
	{
		cache.uncacheAll();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void cacheDirect(K key, V value)
	{
		cleanIfRequired();
		cache.cacheDirect(key, value);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void cacheAllDirect(Map<? extends K, ? extends V> elements)
	{
		cleanIfRequired();
		cache.cacheAllDirect(elements);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getSize()
	{
		cleanIfRequired();
		return cache.getSize();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getLimit()
	{
		cleanIfRequired();
		return cache.getLimit();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clean()
	{
		cache.clean();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clean(CacheCleaner<K, V> cleaner)
	{
		cache.clean(cleaner);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getLastCleanTime()
	{
		return cache.getLastCleanTime();
	}
	
	private void cleanIfRequired()
	{
		if(System.nanoTime() - cache.getLastCleanTime() > interval)
		{
			if(cleaner == null)
				clean();
			else
				clean(cleaner);
		}
	}
}
