/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.caching;

import java.util.Map;

/**
 * Interface for a loader used by a {@link Cache} for auto-loading elements.
 * @author Benjamin Billet
 * @version 0.1
 * @param <K> The type of keys managed by the cache.
 * @param <V> The type of values managed by the cache.
 * @see Cache
 */
public interface CacheLoader<K, V> 
{
	/**
	 * Loads the value that must be associated to a given key into the cache.
	 * This method MUST never return {@code null}. In case of problem, prefer
	 * throwing a {@link RuntimeException}.
	 * @param key The key.
	 * @return The loaded value.
	 */
	public V load(K key);
	
	/**
	 * Loads a set of values associated to a set of keys. This method MUST never
	 * return {@code null}, and the returned {@link Map} MUST not contains a
	 * null key or a null value. In case of problem, prefer throwing
	 * {@link RuntimeException}.
	 * @param keys The set of keys.
	 * @return The loaded values.
	 */
	public Map<K, V> loadAll(Iterable<? extends K> keys);
}
