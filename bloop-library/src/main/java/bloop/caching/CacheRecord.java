/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.caching;

/**
 * A skeleton class for cache records. Typically, you should not instantiate
 * records by yourself ; records are built automatically by a {@link Cache}
 * using a {@link CacheRecordFactory}.<br>
 * <b>Warning</b>: A {@link CacheRecord} is not allowed to store {@code null}
 * values, as a {@code null} value means that a record is expired.
 * 
 * @author Benjamin Billet
 * @version 0.1
 * @param <V> The type of value stored by these records.
 * @see Cache
 */
public abstract class CacheRecord<V>
{
	private long insertionTime;
	private long lastAccessTime;
	
	protected CacheRecord(long insertionTime, long lastAccessTime)
	{
		this.insertionTime = insertionTime;
		this.lastAccessTime = lastAccessTime;
	}
	
	/**
	 * Returns the value stored by this record, or {@code null} if the record is
	 * expired.
	 */
	public abstract V getValue();
	
	/**
	 * Returns the insertion time (nanoseconds) of this record into the cache.
	 */
	public long getInsertionTime()
	{
		return insertionTime;
	}
	
	/**
	 * Returns the last time (nanoseconds) the cache accessed this record (i.e.,
	 * the last time the user asked for this record).
	 */
	public long getLastAccessTime()
	{
		return lastAccessTime;
	}
	
	/**
	 * Sets the last access time of this record to {@link System#nanoTime()}.
	 */
	protected void updateLastAccessTime()
	{
		lastAccessTime = System.nanoTime();
	}
}
