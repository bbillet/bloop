/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.caching;

import java.util.concurrent.TimeUnit;

/**
 * A {@link CacheCleaner} implementation which removes old entries from a cache,
 * based on the insertion time or the last access time.
 * @author Benjamin Billet
 * @version 0.1
 *
 * @param <K> The type of keys managed by the cache.
 * @param <V> The type of values managed by the cache.
 */
public class TimeCleaner<K, V> implements CacheCleaner<K, V>
{
	private long lifetime;
	private boolean basedOnAccessTime;
		
	public TimeCleaner(int lifetime, TimeUnit unit, boolean basedOnAccessTime)
	{
		if(lifetime <= 0)
			throw new IllegalArgumentException();
		
		this.lifetime = unit.toNanos(lifetime);
		this.basedOnAccessTime = basedOnAccessTime;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean shouldDelete(K key, CacheRecord<V> record) 
	{
		long current = System.nanoTime();
		long time = record.getInsertionTime();
		if(basedOnAccessTime)
			time = record.getLastAccessTime();
		
		return (current - time) < lifetime;
	}
}
