/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.caching;

import java.lang.ref.WeakReference;

/**
 * A {@link CacheRecordFactory} implementation which produces records that store
 * values using {@link WeakReference}.
 * @author Benjamin Billet
 * @version 0.1
 * @param <K> The type of keys managed by the cache.
 * @param <V> The type of values managed by the cache.
 */
public class WeakRecordFactory<K, V> implements CacheRecordFactory<K, V>
{
	/**
	 * {@inheritDoc}
	 */
	@Override
	public CacheRecord<V> createRecord(K key, V value)
	{
		return new WeakCacheRecord<>(value);
	}
	
	/**
	 * A {@link CacheRecord} implementation which stores a value using a
	 * {@link WeakReference}.
	 * @author Benjamin Billet
	 * @version 0.1
	 * @param <V> The type of values stored in the record.
	 */
	static class WeakCacheRecord<V> extends CacheRecord<V>
	{
		private WeakReference<V> ref;
		
		public WeakCacheRecord(V value, long insertionTime)
		{
			super(insertionTime, insertionTime);
			this.ref = new WeakReference<>(value);
		}
		
		public WeakCacheRecord(V value)
		{
			this(value, System.nanoTime());
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public V getValue()
		{
			return ref.get();
		}
	}
}
