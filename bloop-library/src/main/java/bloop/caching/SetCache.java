/*  
 * The Bloop Library.
 * Copyright (c) 2012 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.caching;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * A memory cache for sets, which maintains:
 * <ul>
 * <li>The unmodifiable version of the set.
 * <li>An array corresponding to the set values.
 * <li>An unmodifiable set of strings, each string being the result of calling
 * {@link String#valueOf(Object)} on each set value.
 * <li>An array of strings, each string being the result of calling
 * {@link String#valueOf(Object)} on each set value.
 * </ul>
 * <br>
 * <b>Warning</b>: For guaranteeing cache integrity, you must not modify the
 * original set nor the cached arrays.
 * 
 * @author Benjamin Billet
 * @version 0.1
 * @param <T> The type of elements in the cached set.
 */public class SetCache<T>
{
	private Set<T> set;
	private Set<String> stringSet;
	private T[] array;
	private String[] stringArray;
	
	/**
	 * Creates a new cache for a set.<br>
	 * <b>Warning</b>: The set must not be modified. If the set is modified, this cache
	 * is not guaranteed to be consistent.
	 * @param set The set to cache.
	 */
	@SuppressWarnings("unchecked")
	public SetCache(Set<T> set)
	{
		this.set = Collections.unmodifiableSet(set);
		this.array = (T[]) set.toArray();
		
		this.stringSet = new HashSet<>(set.size());
		for(T element : set)
		{
			this.stringSet.add(String.valueOf(element));
		}
		this.stringSet = Collections.unmodifiableSet(this.stringSet);
		this.stringArray = this.stringSet.toArray(new String[0]);
	}
	
	/**
	 * Returns the unmodifiable version of the cached set.
	 */
	public Set<T> getSet()
	{
		return set;
	}

	/**
	 * Returns the cached result of calling {@link String#valueOf(Object)} on
	 * each value of the cached set.
	 */
	public Set<String> getStringSet()
	{
		return stringSet;
	}

	/**
	 * Returns the cached array containing each element of the cached
	 * set.
	 */
	public T[] getArray()
	{
		return array;
	}

	/**
	 * Returns the cached result of calling {@link String#valueOf(Object)} on
	 * each value of the cached set.
	 */
	public String[] getStringArray()
	{
		return stringArray;
	}
	
	/**
	 * A builder for {@link SetCache}.
	 * @author Benjamin Billet
	 * @param <T> The type of elements in the cached set.
	 * @version 0.1
	 */
	public static class Builder<T>
	{
		private Set<T> set = new HashSet<>();
		
		/**
		 * Adds a value into the set.
		 * @param value The value.
		 * @return This builder.
		 */
		public Builder<T> value(T value)
		{
			set.add(value);
			return this;
		}
		
		/**
		 * Adds several values into the set.
		 * @param values The values.
		 * @return This builder.
		 */
		@SuppressWarnings("unchecked")
		public Builder<T> values(T... values)
		{
			Collections.addAll(set, values);
			return this;
		}
		
		/**
		 * Adds several values into the set.
		 * @param values The values.
		 * @return This builder.
		 */
		public Builder<T> values(Collection<T> values)
		{
			set.addAll(values);
			return this;
		}
		
		/**
		 * Builds a new {@link SetCache} from the values added to this builder.
		 * @return The cached set.
		 */
		public SetCache<T> build()
		{
			return new SetCache<T>(set);
		}
	}
}
