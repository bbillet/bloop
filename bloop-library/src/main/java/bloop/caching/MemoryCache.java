/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.caching;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

/**
 * An implementation of {@link Cache} which stores the cached elements directly
 * into memory, using an {@link HashMap}. Use {@link MemoryCache.Builder} for
 * creating and configuring new instances of {@link MemoryCache}.
 * @author Benjamin Billet
 * @version 0.1
 * @param <K> The type of keys managed by the cache.
 * @param <V> The type of values managed by the cache.
 * @see Cache
 */
public class MemoryCache<K, V> implements Cache<K, V>
{
	private Map<K, CacheRecord<V>> cache;

	private long limit;
	private CacheLoader<K, V> loader;
	private CacheCleaner<K, V> cleaner;
	private CacheRecordFactory<K, V> recordFactory;
	private List<OnCleaned<K, V>> cleanListeners;

	private long lastClean;

	private MemoryCache(long limit, int initialCapacity, float loadFactor,
			CacheRecordFactory<K, V> recordFactory, CacheLoader<K, V> loader,
			CacheCleaner<K, V> cleaner, List<OnCleaned<K, V>> cleanListeners)
	{
		if (limit <= 0)
			throw new IllegalArgumentException();

		this.limit = limit;
		this.cache = new HashMap<>(initialCapacity, loadFactor);

		this.recordFactory = recordFactory;
		this.loader = loader;

		this.cleaner = cleaner;
		this.cleanListeners = cleanListeners;
		this.lastClean = System.nanoTime();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public V get(K key)
	{
		V value = getIfCached(key);
		if (value != null)
			return value;

		if (loader == null)
			return null;

		// use the loader to get the new value
		value = loader.load(key);
		if (value != null)
		{
			// TODO what to do when full: remove the oldest element ? perform a
			// cleaning task ?
			if (cache.size() < limit)
				cache.put(key, recordFactory.createRecord(key, value));
			return value;
		}
		else
			throw new NullPointerException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<K, V> getAll(Iterable<K> keys)
	{
		Map<K, V> values = new HashMap<>();
		for (K key : keys)
		{
			values.put(key, get(key));
		}
		return values;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public V getIfCached(K key)
	{
		CacheRecord<V> record = cache.get(key);
		if (record != null)
		{
			// if the record has expired, remove it from the cache
			if (record.getValue() == null)
				cache.remove(key);

			record.updateLastAccessTime();
			return record.getValue();
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<K, V> getAllIfCached(Iterable<K> keys)
	{
		Map<K, V> values = new HashMap<K, V>();
		for (K key : keys)
		{
			values.put(key, getIfCached(key));
		}
		return values;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public V reload(K key)
	{
		if (loader == null)
			return getIfCached(key);

		V value = loader.load(key);
		if (value != null)
		{
			// TODO what to do when full: remove the oldest element ? perform a
			// cleaning task ?
			if (cache.containsKey(key) || cache.size() < limit)
				cache.put(key, recordFactory.createRecord(key, value));
			return value;
		}
		else
			throw new NullPointerException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<K, V> reload(Iterable<K> keys)
	{
		Map<K, V> values = new HashMap<>();
		for (K key : keys)
		{
			values.put(key, reload(key));
		}
		return values;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void cacheDirect(K key, V value)
	{
		if (key == null || value == null)
			throw new NullPointerException();

		if (cache.size() < limit)
			cache.put(key, recordFactory.createRecord(key, value));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void cacheAllDirect(Map<? extends K, ? extends V> elements)
	{
		for (Entry<? extends K, ? extends V> e : elements.entrySet())
		{
			cacheDirect(e.getKey(), e.getValue());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void uncache(K key)
	{
		cache.remove(key);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void uncacheAll(Iterable<K> keys)
	{
		for (K key : keys)
		{
			cache.remove(key);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void uncacheAll()
	{
		cache.clear();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clean()
	{
		if (cleaner != null)
			clean(cleaner);
		else
			lastClean = System.nanoTime();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clean(CacheCleaner<K, V> cleaner)
	{
		Iterator<Entry<K, CacheRecord<V>>> it = cache.entrySet().iterator();
		while (it.hasNext())
		{
			Entry<K, CacheRecord<V>> cached = it.next();
			if (cached.getValue().getValue() != null)
			{
				if (cleaner.shouldDelete(cached.getKey(), cached.getValue()))
				{
					it.remove();
					notifyCleanListeners(cached.getKey(),
							cached.getValue().getValue());
				}
			}
			else // expired
				it.remove();
		}
		lastClean = System.nanoTime();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isCached(K key)
	{
		CacheRecord<V> record = cache.get(key);
		if (record == null)
			return false;
		else if (record.getValue() == null)
		{
			cache.remove(key);
			return false;
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getSize()
	{
		return cache.size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getLimit()
	{
		return limit;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getLastCleanTime()
	{
		return lastClean;
	}

	private void notifyCleanListeners(K key, V value)
	{
		for (OnCleaned<K, V> listener : cleanListeners)
		{
			listener.onCleaned(key, value);
		}
	}

	/**
	 * A builder for {@link MemoryCache}. The default configuration is:
	 * <ul>
	 * <li>Limited size: {@link Long#MAX_VALUE}.
	 * <li>Cleaner: a {@link TimeCleaner}, which removes an element if its last
	 * access time is older than one hour.
	 * <li>Record factory: {@link SimpleRecordFactory}.
	 * <li>Loader: No loader.
	 * <li>{@link OnCleaned} listeners: No listeners.
	 * <li>Storage: {@link HashMap}, with a load factor of 0.75 and an initial
	 * capacity of 16.
	 * </ul>
	 * @author Benjamin Billet
	 * @version 0.1
	 * @param <K> The type of keys managed by the cache.
	 * @param <V> The type of values managed by the cache.
	 * @see MemoryCache
	 */
	public static class Builder<K, V>
	{
		private CacheLoader<K, V> loader = null;
		private CacheRecordFactory<K, V> recordFactory = new SimpleRecordFactory<>();
		private CacheCleaner<K, V> cleaner = new TimeCleaner<>(1, TimeUnit.HOURS, true);
		private long limit = Long.MAX_VALUE;
		private int initialCapacity = 16;
		private float loadFactor = 0.75f;
		private List<OnCleaned<K, V>> cleanListeners = new ArrayList<>();

		/**
		 * Specifies a {@link CacheLoader} for the {@link MemoryCache} built by
		 * this builder.
		 * @param loader The loader (may be null, if auto-loading is not required).
		 * @return this.
		 */
		public Builder<K, V> loader(CacheLoader<K, V> loader)
		{
			this.loader = loader;
			return this;
		}

		/**
		 * Specifies a {@link CacheRecordFactory} for the {@link MemoryCache}
		 * built by this builder.
		 * @param recordFactory The factory.
		 * @return this.
		 */
		public Builder<K, V> recordFactory(CacheRecordFactory<K, V> recordFactory)
		{
			if (recordFactory == null)
				throw new IllegalArgumentException();

			this.recordFactory = recordFactory;
			return this;
		}

		/**
		 * Specifies a {@link CacheCleaner} for the {@link MemoryCache} built by
		 * this builder.
		 * @param cleaner The cleaner (may be null, if cleaning is not required).
		 * @return this.
		 */
		public Builder<K, V> cleaner(CacheCleaner<K, V> cleaner)
		{
			this.cleaner = cleaner;
			return this;
		}

		/**
		 * Specifies a size limit for the {@link MemoryCache} built by this
		 * builder.
		 * @param limit The limit.
		 * @return this.
		 */
		public Builder<K, V> limit(long limit)
		{
			if (limit <= 0)
				throw new IllegalArgumentException();

			this.limit = limit;
			return this;
		}

		/**
		 * Specifies the initial capacity of the {@link HashMap} managed by the
		 * {@link MemoryCache} built by this builder.
		 * @param initialCapacity The initialCapacity.
		 * @return this.
		 * @see HashMap
		 */
		public Builder<K, V> initialCapacity(int initialCapacity)
		{
			if(initialCapacity <= 0)
				throw new IllegalArgumentException();
			
			this.initialCapacity = initialCapacity;
			return this;
		}

		/**
		 * Specifies the load factor the {@link HashMap} managed by the
		 * {@link MemoryCache} built by this builder.
		 * @param loadFactor The loadFactor.
		 * @return this.
		 * @see HashMap
		 */
		public Builder<K, V> loadFactor(float loadFactor)
		{
			if(loadFactor <= 0)
				throw new IllegalArgumentException();
			
			this.loadFactor = loadFactor;
			return this;
		}

		/**
		 * Adds a {@link OnCleaned} listener to the {@link MemoryCache}
		 * built by this builder.
		 * @param listener The listener.
		 * @return this.
		 */
		public Builder<K, V> onCleanedListener(OnCleaned<K, V> listener)
		{
			if (listener == null)
				throw new IllegalArgumentException();

			this.cleanListeners.add(listener);
			return this;
		}
		
		/**
		 * Adds a set of {@link OnCleaned} listeners to the {@link MemoryCache}
		 * built by this builder.
		 * @param listeners The listeners.
		 * @return this.
		 */
		public Builder<K, V> onCleanedListener(Iterable<OnCleaned<K, V>> listeners)
		{
			for(OnCleaned<K, V> listener : listeners)
			{
				onCleanedListener(listener);
			}
			return this;
		}

		/**
		 * Builds a new {@link MemoryCache} based on the configuration given to
		 * this builder.
		 * @return The new cache.
		 */
		public MemoryCache<K, V> build()
		{
			return new MemoryCache<>(limit, initialCapacity, loadFactor,
					recordFactory, loader, cleaner, cleanListeners);
		}
	}
}
