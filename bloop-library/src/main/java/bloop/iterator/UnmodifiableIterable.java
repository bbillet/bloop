package bloop.iterator;
import java.util.Iterator;

/**
 * An {@link Iterable} implementation that returns {@link UnmodifiableIterator} instances.
 * @author Benjamin Billet
 * @version 0.1
 * @see Iterator
 * @see UnmodifiableIterator
 * @param <T> The type of elements managed by this iterable.
 */
public final class UnmodifiableIterable<T> implements Iterable<T>
{
	private final Iterable<T> internal;
	
	/**
	 * Creates a new unmodifiable iterator, enclosing an existing
	 * {@link Iterator}.
	 * @param source The source iterator.
	 */
	public UnmodifiableIterable(Iterable<T> source)
	{
		this.internal = source;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<T> iterator() 
	{
		return internal.iterator();
	}
}