/*  
 * The Bloop Library.
 * Copyright (c) 2015 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.iterator;

import java.util.Iterator;

import bloop.func.WrapFunction;


/**
 * An implementation of {@link Iterator} which lazily wraps the elements
 * returned by an existing {@code Iterator<T1>} into elements of type T2,
 * using a {@link WrapFunction}.
 * 
 * <pre>
 * {@code
 * Collection<InputStream> streams = ...;
 * ...
 * Iterator<Reader> readers = new IteratorWrapper<>(streams.iterator(), new WrapFunction<InputStream, Reader>()
 * {
 * 	public Reader wrap(InputStream stream)
 * 	{
 * 		return new InputStreamReader(stream);
 * 	}
 * });
 * }
 * </pre>
 * 
 * @author Benjamin Billet
 * @version 0.1
 * 
 * @see Iterator
 * @see WrapFunction
 * @param <T1> The type of instances returned by the original iterator.
 * @param <T2> The type of instances returned by this iterator.
 */
public class IteratorWrapper<T1, T2> implements Iterator<T2>
{
	private Iterator<T1> iterator;
	private WrapFunction<T1, T2> wrapFunction;
	
	public IteratorWrapper(Iterator<T1> iterator, WrapFunction<T1, T2> wrapFunction)
	{
		this.iterator = iterator;
		this.wrapFunction = wrapFunction;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext()
	{
		return iterator.hasNext();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
    public void remove()
    {
        throw new UnsupportedOperationException("remove");
    }

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T2 next()
	{
		return wrapFunction.wrap(iterator.next()); // TODO recycling ?
	}
}