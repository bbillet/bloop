/*  
 * The Bloop Library.
 * Copyright (c) 2015 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.iterator;

import java.util.Iterator;

import bloop.func.WrapFunction;

/**
 * An implementation of {@link Iterable} which wraps the elements of an
 * existing {@code Iterable<T1>} into elements of type T2, using a
 * {@link WrapFunction}.
 * 
 * <pre>{@code
 * Collection<InputStream> streams = ...;
 * ...
 * Iterable<Reader> readers = new IterableWrapper<>(streams, new WrapFunction<InputStream, Reader>()
 * {
 * 	public Reader adapt(InputStream stream)
 * 	{
 * 		return new InputStreamReader(stream);
 * 	}
 * });
 * }</pre>
 * 
 * @author Benjamin Billet
 * @version 0.1
 * 
 * @see Iterable
 * @see WrapFunction
 * @see IteratorWrapper
 * @param <T1> The type of instances managed by the original iterable.
 * @param <T2> The type of instances managed by this iterable.
 */
public class IterableWrapper<T1, T2> implements Iterable<T2>
{
	private Iterable<T1> iterable;
	private WrapFunction<T1, T2> wrapFunction;
	
	public IterableWrapper(Iterable<T1> iterable, WrapFunction<T1, T2> wrapFunction)
	{
		this.iterable = iterable;
		this.wrapFunction = wrapFunction;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<T2> iterator()
	{
		return new IteratorWrapper<>(iterable.iterator(), wrapFunction);
	}
	
	/**
	 * Returns the original iterable encapsulated by this iterable.
	 */
	public Iterable<T1> getIterable()
	{
		return iterable;
	}
	
	/**
	 * Mutate this iterable wrapper into another type of iterable wrapper.
	 * The following codes are equivalent:
	 * <pre>
	 * {@code
	 * IterableWrapper<InputStream, Reader> iterable = ...;
	 * IterableWrapper<InputStream, PrintStream> newIterable = iterable.mutate(new WrapFunction<InputStream, PrintStream>()
	 * 	...
	 * });
	 * }
	 * </pre>
	 * <pre>
	 * {@code
	 * IterableWrapper<InputStream, Reader> iterable = ...;
	 * IterableWrapper<InputStream, PrintStream> newIterable = new IterableWrapper<>(iterable.getIterable(), new WrapFunction<InputStream, PrintStream>()
	 * {
	 * 	...
	 * });
	 * }
	 * </pre>
	 * 
	 * @param wrapFunction The new wrap function to use.
	 * @return The new iterable wrapper.
	 */
	public <T> IterableWrapper<T1, T> mutate(WrapFunction<T1, T> wrapFunction)
	{
		return new IterableWrapper<>(iterable, wrapFunction);
	}
}