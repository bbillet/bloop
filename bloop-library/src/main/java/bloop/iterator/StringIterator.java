/*  
 * The Bloop Library.
 * Copyright (c) 2014 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.iterator;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

/**
 * A {@link NavigableIterator} that iterates over a {@link CharSequence} (such
 * as a {@link java.lang.String} or a {@link java.lang.StringBuilder}).<br>
 * If the underlying {@code CharSequence} is modified while iterating, a
 * {@link ConcurrentModificationException} is thrown.
 * 
 * @see CharSequence
 * @see NavigableIterator
 * @author Benjamin Billet
 * @version 0.1
 */
public class StringIterator implements NavigableIterator<Character>
{
	public CharSequence sequence;
	private int current = 0;
	private int length;
	
	public StringIterator(CharSequence sequence)
	{
		this.sequence = sequence;
		this.length = sequence.length();
	}
	
	/** 
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext()
	{
		return current < length - 1;
	}

	/** 
	 * {@inheritDoc}
	 */
	@Override
	public Character next()
	{
		checkModifications();
		if(current < length - 1)
		{
			current++;
			return sequence.charAt(current);
		}
		else
			throw new NoSuchElementException();
	}

	/** 
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasPrevious()
	{
		return current > 0;
	}

	/** 
	 * {@inheritDoc}
	 */
	@Override
	public Character previous()
	{
		checkModifications();
		if(current > 0)
		{
			current--;
			return sequence.charAt(current);
		}
		else
			throw new NoSuchElementException();
	}

	/** 
	 * {@inheritDoc}
	 */
	@Override
	public Character current()
	{
		checkModifications();
		return sequence.charAt(current);
	}

	/** 
	 * {@inheritDoc}
	 */
	@Override
	public int index()
	{
		return current;
	}

	/** 
	 * {@inheritDoc}
	 */
	@Override
	public int size()
	{
		return length;
	}

	/** 
	 * {@inheritDoc}
	 */
	@Override
	public boolean indexExists(int index)
	{
		return index >= 0 && index < length;
	}

	/** 
	 * {@inheritDoc}
	 */
	@Override
	public void jumpTo(int index)
	{
		if(indexExists(index) == false)
			throw new IllegalArgumentException();
		
		this.current = index;
	}
	
	/** 
	 * {@inheritDoc}
	 */
	@Override
	public void jump(int nb)
	{
		int newCurrent = current + nb;
		if(indexExists(newCurrent) == false)
			throw new IllegalArgumentException();
		
		this.current = newCurrent;
	}
	
	/**
	 * Iterates until the given character is met.
	 * @param character The character.
	 * @return The index of the character, or -1 if the character was not found.
	 */
	public int jumpToNextCharacter(char character)
	{
		while(hasNext())
		{
			if(next() == character)
				return current;
		}
		return -1;
	}
	
	/**
	 * Reverse iterates until the given character is met.
	 * @param character The character.
	 * @return The index of the character, or -1 if the character was not found.
	 */
	public int jumpToPreviousCharacter(char character)
	{
		while(hasPrevious())
		{
			if(previous() == character)
				return current;
		}
		return -1;
	}
	
	private void checkModifications()
	{
		// only check the length
		if(length != sequence.length())
			throw new ConcurrentModificationException();
	}
	
	/**
	 * @throws UnsupportedOperationException if called.
	 */
	@Override @Deprecated
	public void remove()
	{
		throw new UnsupportedOperationException("remove");
	}
}
