/*  
 * The Bloop Library.
 * Copyright (c) 2014 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.iterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A set of helper functions related to iterables.
 * 
 * @see Iterable
 * @author Benjamin Billet
 * @version 0.1
 */
public final class Iterables
{
	private Iterables() { }
	
	private static final Iterable<Object> EMPTY_ITERABLE = new Iterable<Object>()
	{
		@Override
		public Iterator<Object> iterator()
		{
			return Iterators.emptyIterator();
		}
	};
	
	/**
	 * Returns an empty {@link Iterable}.
	 * @return the iterable.
	 * @param <T> the type of elements in the iterable.
	 */
	@SuppressWarnings("unchecked")
	public static <T> Iterable<T> emptyIterable()
	{
		return (Iterable<T>) EMPTY_ITERABLE;
	}
	
	/**
	 * Returns an unmodifiable {@link Iterable}.
	 * @return The iterable.
	 * @param <T> The type of elements in the iterable.
	 */
	public static <T> Iterable<T> unmodifiableIterator(Iterable<T> iterable)
	{
		return new UnmodifiableIterable<>(iterable);
	}
	
	/**
	 * Builds an array list from an iterable.
	 * @param iterable The source iterable.
	 * @return The list.
	 * @param <T> The type of elements in the iterable.
	 */
	public static <T> List<T> toArrayList(Iterable<T> iterable)
	{
		List<T> list = new ArrayList<T>();
		for(T element : iterable)
		{
			list.add(element);
		}
		return list;
	}
}
