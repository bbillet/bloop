/*  
 * The Bloop Library.
 * Copyright (c) 2014 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.iterator;

import java.lang.reflect.Method;
import java.util.Iterator;

import bloop.java.lang.reflect.Methods;

/**
 * An iterable which returns {@link CloningIterator}.
 * 
 * @author Benjamin Billet
 * @param <T> The type of elements managed by this iterable.
 * @see Iterable
 * @see CloningIterator
 * @version 0.1
 */
public class CloningIterable<T extends Cloneable> implements Iterable<T>
{
	private Iterable<T> iterable;
	private Method cloneMethod;
	
	/**
	 * Constructs a new cloning iterable.
	 * @param iterable The underlying iterable.
	 * @param contentClass The type of the elements contained in the iterable.
	 * @throws CloneNotSupportedException If the type of objects are not
	 *             providing a public {@code clone()} method.
	 */
	public CloningIterable(Iterable<T> iterable, Class<T> contentClass) throws CloneNotSupportedException
	{
		this.iterable = iterable;
		this.cloneMethod = Methods.findCloneMethod(contentClass);
		if(cloneMethod == null)
			throw new CloneNotSupportedException("'clone()' method not found");
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<T> iterator()
	{
		return new CloningIterator<T>(iterable.iterator(), cloneMethod);
	}
}
