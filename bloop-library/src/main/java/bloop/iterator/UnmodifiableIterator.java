package bloop.iterator;
import java.util.Iterator;

/**
 * An {@link Iterator} implementation that iterates over a collection that can
 * not be modified, i.e., the {@link Iterator#remove()} method will always throw
 * {@link UnsupportedOperationException}. In practice, this class encloses an
 * existing {@link Iterator}.
 * @author Benjamin Billet
 * @version 0.1
 * @see Iterator
 * @param <T> The type of elements in the source iterator.
 */
public final class UnmodifiableIterator<T> implements Iterator<T>
{
	private final Iterator<T> internal;
	
	/**
	 * Creates a new unmodifiable iterator, enclosing an existing
	 * {@link Iterator}.
	 * @param source The source iterator.
	 */
	public UnmodifiableIterator(Iterator<T> source)
	{
		this.internal = source;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext() 
	{
		return internal.hasNext();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T next() 
	{
		return internal.next();
	}

	/**
	 * @throws UnsupportedOperationException if called.
	 */
	@Override @Deprecated
	public void remove()
	{
		throw new UnsupportedOperationException("remove");
	}
}