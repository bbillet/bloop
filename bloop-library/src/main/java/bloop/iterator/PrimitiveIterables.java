/*  
 * The Bloop Library.
 * Copyright (c) 2014 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.iterator;

import java.util.Iterator;

import bloop.java.util.PrimitiveCollections;

/**
 * A set of iterables for encapsulating arrays of primitive types.
 * 
 * @author Benjamin Billet
 * @version 0.1
 */
public class PrimitiveIterables
{
	private PrimitiveIterables() { }
	
	/**
	 * Creates a new Byte iterable from a byte array.
	 * @param array The array.
	 * @return The iterable.
	 */
	public static Iterable<Byte> iterable(byte[] array)
	{
		return new ByteIterable(array);
	}
	
	/**
	 * Creates a new Short iterable from a short array.
	 * @param array The array.
	 * @return The iterable.
	 */
	public static Iterable<Short> iterable(short[] array)
	{
		return new ShortIterable(array);
	}
	
	/**
	 * Creates a new Integer iterable from an int array.
	 * @param array The array.
	 * @return The iterable.
	 */
	public static Iterable<Integer> iterable(int[] array)
	{
		return new IntegerIterable(array);
	}
	
	/**
	 * Creates a new Long iterable from a long array.
	 * @param array The array.
	 * @return The iterable.
	 */
	public static Iterable<Long> iterable(long[] array)
	{
		return new LongIterable(array);
	}
	
	/**
	 * Creates a new Float iterable from a float array.
	 * @param array The array.
	 * @return The iterable.
	 */
	public static Iterable<Float> iterable(float[] array)
	{
		return new FloatIterable(array);
	}
	
	/**
	 * Creates a new Double iterable from a double array.
	 * @param array The array.
	 * @return The iterable.
	 */
	public static Iterable<Double> iterable(double[] array)
	{
		return new DoubleIterable(array);
	}
	
	/**
	 * Creates a new Boolean iterable from a boolean array.
	 * @param array The array.
	 * @return The iterable.
	 */
	public static Iterable<Boolean> iterable(boolean[] array)
	{
		return new BooleanIterable(array);
	}
	
	/**
	 * Creates a new Character iterable from a char array.
	 * @param array The array.
	 * @return The iterable.
	 */
	public static Iterable<Character> iterable(char[] array)
	{
		return new CharacterIterable(array);
	}
	
	/**
	 * Creates a new byte array from a Byte iterable, using
	 * {@link PrimitiveCollections#toByteArray(java.util.Collection)}.
	 * @param iterable The iterable.
	 * @return The array.
	 */
	public static byte[] toByteArray(Iterable<Byte> iterable)
	{
		return PrimitiveCollections.toByteArray(Iterables.toArrayList(iterable));
	}
	
	/**
	 * Creates a new short array from a Short iterable, using
	 * {@link PrimitiveCollections#toShortArray(java.util.Collection)}.
	 * @param iterable The iterable.
	 * @return The array.
	 */
	public static short[] toShortArray(Iterable<Short> iterable)
	{
		return PrimitiveCollections.toShortArray(Iterables.toArrayList(iterable));
	}
	
	/**
	 * Creates a new int array from an Integer iterable, using
	 * {@link PrimitiveCollections#toIntegerArray(java.util.Collection)}.
	 * @param iterable The iterable.
	 * @return The array.
	 */
	public static int[] toIntegerArray(Iterable<Integer> iterable)
	{
		return PrimitiveCollections.toIntegerArray(Iterables.toArrayList(iterable));
	}
	
	/**
	 * Creates a new long array from a Long iterable, using
	 * {@link PrimitiveCollections#toLongArray(java.util.Collection)}.
	 * @param iterable The iterable.
	 * @return The array.
	 */
	public static long[] toLongArray(Iterable<Long> iterable)
	{
		return PrimitiveCollections.toLongArray(Iterables.toArrayList(iterable));
	}
	
	/**
	 * Creates a new float array from a Float iterable, using
	 * {@link PrimitiveCollections#toFloatArray(java.util.Collection)}.
	 * @param iterable The iterable.
	 * @return The array.
	 */
	public static float[] toFloatArray(Iterable<Float> iterable)
	{
		return PrimitiveCollections.toFloatArray(Iterables.toArrayList(iterable));
	}
	
	/**
	 * Creates a new double array from a Double iterable, using
	 * {@link PrimitiveCollections#toDoubleArray(java.util.Collection)}.
	 * @param iterable The iterable.
	 * @return The array.
	 */
	public static double[] toDoubleArray(Iterable<Double> iterable)
	{
		return PrimitiveCollections.toDoubleArray(Iterables.toArrayList(iterable));
	}
	
	/**
	 * Creates a new boolean array from a Boolean iterable, using
	 * {@link PrimitiveCollections#toBooleanArray(java.util.Collection)}.
	 * @param iterable The iterable.
	 * @return The array.
	 */
	public static boolean[] toBooleanArray(Iterable<Boolean> iterable)
	{
		return PrimitiveCollections.toBooleanArray(Iterables.toArrayList(iterable));
	}
	
	/**
	 * Creates a new char array from a Character iterable, using
	 * {@link PrimitiveCollections#toCharacterArray(java.util.Collection)}.
	 * @param iterable The iterable.
	 * @return The array.
	 */
	public static char[] toCharacterArray(Iterable<Character> iterable)
	{
		return PrimitiveCollections.toCharacterArray(Iterables.toArrayList(iterable));
	}
	
	/**
	 * A Byte iterable for byte arrays.
	 * @see Iterator
	 * @author Benjamin Billet
	 */
	public static class ByteIterable implements Iterable<Byte>
	{
		private final byte[] array;
		
		public ByteIterable(byte... array)
		{
			this.array = array;
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public Iterator<Byte> iterator()
		{
			return PrimitiveIterators.iterator(array);
		}
	}
	
	/**
	 * A Short iterable for short arrays.
	 * @see Iterator
	 * @author Benjamin Billet
	 */
	public static class ShortIterable implements Iterable<Short>
	{
		private final short[] array;
		
		public ShortIterable(short... array)
		{
			this.array = array;
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public Iterator<Short> iterator()
		{
			return PrimitiveIterators.iterator(array);
		}
	}
	
	/**
	 * An Integer iterable for int arrays.
	 * @see Iterator
	 * @author Benjamin Billet
	 */
	public static class IntegerIterable implements Iterable<Integer>
	{
		private final int[] array;
		
		public IntegerIterable(int... array)
		{
			this.array = array;
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public Iterator<Integer> iterator()
		{
			return PrimitiveIterators.iterator(array);
		}
	}
	
	/**
	 * A Long iterable for long arrays.
	 * @see Iterator
	 * @author Benjamin Billet
	 */
	public static class LongIterable implements Iterable<Long>
	{
		private final long[] array;
		
		public LongIterable(long... array)
		{
			this.array = array;
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public Iterator<Long> iterator()
		{
			return PrimitiveIterators.iterator(array);
		}
	}
	
	/**
	 * A Boolean iterable for boolean arrays.
	 * @see Iterator
	 * @author Benjamin Billet
	 */
	public static class BooleanIterable implements Iterable<Boolean>
	{
		private final boolean[] array;
		
		public BooleanIterable(boolean... array)
		{
			this.array = array;
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public Iterator<Boolean> iterator()
		{
			return PrimitiveIterators.iterator(array);
		}
	}
	
	/**
	 * A Float iterable for float arrays.
	 * @see Iterator
	 * @author Benjamin Billet
	 */
	public static class FloatIterable implements Iterable<Float>
	{
		private final float[] array;
		
		public FloatIterable(float... array)
		{
			this.array = array;
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public Iterator<Float> iterator()
		{
			return PrimitiveIterators.iterator(array);
		}
	}
	
	/**
	 * A Double iterable for double arrays.
	 * @see Iterator
	 * @author Benjamin Billet
	 */
	public static class DoubleIterable implements Iterable<Double>
	{
		private final double[] array;
		
		public DoubleIterable(double... array)
		{
			this.array = array;
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public Iterator<Double> iterator()
		{
			return PrimitiveIterators.iterator(array);
		}
	}
	
	/**
	 * A Character iterable for char arrays.
	 * @see Iterator
	 * @author Benjamin Billet
	 */
	public static class CharacterIterable implements Iterable<Character>
	{
		private final char[] array;
		
		public CharacterIterable(char... array)
		{
			this.array = array;
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public Iterator<Character> iterator()
		{
			return PrimitiveIterators.iterator(array);
		}
	}
}
