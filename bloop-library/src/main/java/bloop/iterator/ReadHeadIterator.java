/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.iterator;

import java.util.Iterator;

/**
 * An iterator providing access to the current element.
 * 
 * @author Benjamin Billet
 * @param <T> The type of elements returned by this iterator.
 * @see Iterator
 * @version 0.1
 */
public class ReadHeadIterator<T> implements Iterator<T>
{
	private Iterator<T> internal;
	private T current;
	
	public ReadHeadIterator(Iterator<T> iterator)
	{
		this.internal = iterator;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext()
	{
		return internal.hasNext();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T next()
	{
		current = internal.next();
		return current;
	}
	
	/**
	 * Returns the current element in the iteration. If
	 * {@link ReadHeadIterator#next()} have never been invoked,
	 * {@code null} is returned.
	 * @return The current element, or {@code null}.
	 */
	public T current()
	{
		return current;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove()
	{
		internal.remove();
	}
}
