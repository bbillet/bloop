/*  
 * The Bloop Library.
 * Copyright (c) 2010 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.iterator;

import java.util.Enumeration;
import java.util.Iterator;


/**
 * An implementation of {@link Iterator} which adapts an existing
 * {@link Enumeration}.
 * 
 * @author Benjamin Billet
 * @version 0.1
 * 
 * @see Iterator
 * @see Enumeration
 * @param <T> The type of instances returned by the original enumeration.
 */
public class EnumerationIterator<T> implements Iterator<T>
{
	private Enumeration<T> enumeration;
	
	public EnumerationIterator(Enumeration<T> enumeration)
	{
		this.enumeration = enumeration;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext()
	{
		return enumeration.hasMoreElements();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
    public void remove()
    {
        throw new UnsupportedOperationException("remove");
    }

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T next()
	{
		return enumeration.nextElement();
	}
}