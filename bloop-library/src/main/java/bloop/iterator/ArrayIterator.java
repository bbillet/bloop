/*  
 * The Bloop Library.
 * Copyright (c) 2014 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.iterator;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * An {@link Iterator} for iterating on arrays.
 * @author BenjaminBillet
 * @see Iterator
 * @version 0.1
 * @param <T> The type of elements returned by this iterator.
 */
public class ArrayIterator<T> implements Iterator<T>
{
	private final T[] array;
	private int next = 0;
	
	/**
	 * Constructs a new Iterator that encapsulates an array.
	 * @param array The array.
	 */
	@SafeVarargs
	public ArrayIterator(T... array)
	{
		this.array = array;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext()
	{
		return next < array.length;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T next()
	{
		if(next < array.length)
			return array[next++];
		else
			throw new NoSuchElementException();
	}
	
	/**
	 * @throws UnsupportedOperationException if called.
	 */
	@Override @Deprecated
	public void remove()
	{
		throw new UnsupportedOperationException("remove");
	}
}
