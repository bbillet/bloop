/*  
 * The Bloop Library.
 * Copyright (c) 2010 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.iterator;

import java.util.Enumeration;
import java.util.Iterator;

/**
 * A set of helper functions and data structures related to enumerations.
 * 
 * @see Enumeration
 * @author Benjamin Billet
 * @version 0.1
 */
public class Enumerations
{
	/**
	 * Encapsulates an {@link Enumeration} into an {@link Iterable}. Please note
	 * that this is a simple encapsulation, you will be able to iterate over the
	 * new iterable only once.
	 * @param enumeration The enumeration.
	 * @return The new iterable.
	 * @param <T> The type of elements in the enumeration.
	 */
	public static <T> Iterable<T> asIterable(Enumeration<T> enumeration)
	{
		return new EnumerationIterable<T>(enumeration);
	}
	
	private static final class EnumerationIterable<T> implements Iterable<T>
	{
		private Enumeration<T> enumeration;

		private EnumerationIterable(Enumeration<T> enumeration)
		{
			this.enumeration = enumeration;
		}

		@Override
		public Iterator<T> iterator()
		{
			return new EnumerationIterator<>(enumeration);
		}
	}
}
