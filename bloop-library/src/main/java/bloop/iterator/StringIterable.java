/*  
 * The Bloop Library.
 * Copyright (c) 2014 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.iterator;

import java.util.Iterator;

/**
 * An iterable for {@link CharSequence} (such as a {@link java.lang.String} or a
 * {@link java.lang.StringBuilder}), which returns {@link StringIterator}
 * instances.
 * 
 * <pre>
 * {@code
 * String s = "this is a string!";
 * for(Character c : new StringIterable(s))
 * {
 * 	...
 * }
 * }
 * </pre>
 * 
 * @see CharSequence
 * @see StringIterator
 * @author Benjamin Billet
 * @version 0.1
 */
public class StringIterable implements Iterable<Character>
{
	public CharSequence sequence;
	
	public StringIterable(CharSequence sequence)
	{
		this.sequence = sequence;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<Character> iterator()
	{
		return new StringIterator(sequence);
	}
}
