/*  
 * The Bloop Library.
 * Copyright (c) 2015 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.iterator;

import java.util.Iterator;

import bloop.func.AdaptFunction;

/**
 * An implementation of {@link Iterable} which adaots the elements of an
 * existing {@code Iterable<T1>} into elements of type T2, using an
 * {@link AdaptFunction}.
 * 
 * <pre>{@code
 * Collection<Long> longs = ...;
 * ...
 * Iterator<String> strings = new IterableAdapter<>(longs, new AdaptFunction<Long, String>()
 * {
 * 	public String adapt(Long element)
 * 	{
 * 		return String.valueOf(element);
 * 	}
 * });
 * }</pre>
 * 
 * @author Benjamin Billet
 * @version 0.1
 * 
 * @see Iterable
 * @see AdaptFunction
 * @see IteratorAdapter
 * @param <T1> The type of instances managed by the original iterable.
 * @param <T2> The type of instances managed by this iterable.
 */
public class IterableAdapter<T1, T2> implements Iterable<T2>
{
	private Iterable<T1> iterable;
	private AdaptFunction<T1, T2> adaptFunction;
	
	public IterableAdapter(Iterable<T1> iterable, AdaptFunction<T1, T2> adaptFunction)
	{
		this.iterable = iterable;
		this.adaptFunction = adaptFunction;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<T2> iterator()
	{
		return new IteratorAdapter<>(iterable.iterator(), adaptFunction);
	}
	
	/**
	 * Returns the original iterable encapsulated by this iterable.
	 */
	public Iterable<T1> getIterable()
	{
		return iterable;
	}
	
	/**
	 * Mutate this iterable adapter into another type of iterable adapter.
	 * The following codes are equivalent:
	 * <pre>
	 * {@code
	 * IterableAdapter<Long, String> iterable = ...;
	 * IterableAdapter<Long, byte[]> newIterable = iterable.mutate(new AdaptFunction<Long, byte[]>()
	 * 	...
	 * });
	 * }
	 * </pre>
	 * <pre>
	 * {@code
	 * IterableAdapter<Long, String> iterable = ...;
	 * IterableAdapter<Long, byte[]> newIterable = new IterableAdapter<>(iterable.getIterable(), new AdaptFunction<Long, byte[]>()
	 * {
	 * 	...
	 * });
	 * }
	 * </pre>
	 * 
	 * @param adaptFunction The new adapt function to use.
	 * @return The new iterable adapter.
	 */
	public <T> IterableAdapter<T1, T> mutate(AdaptFunction<T1, T> adaptFunction)
	{
		return new IterableAdapter<>(iterable, adaptFunction);
	}
}