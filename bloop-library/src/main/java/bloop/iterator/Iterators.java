/*  
 * The Bloop Library.
 * Copyright (c) 2014 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.iterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * A set of helper functions and data structures related to iterators.
 * 
 * @see Iterator
 * @author Benjamin Billet
 * @version 0.1
 */
public final class Iterators
{
	private Iterators() { }
	
	private static final Iterator<Object> EMPTY_ITERATOR = new Iterator<Object>()
	{
		@Override
		public boolean hasNext()
		{
			return false;
		}

		@Override
		public Object next()
		{
			throw new NoSuchElementException();
		}
		
		@Override
		public void remove()
		{
			throw new UnsupportedOperationException("remove");
		}
	};
	
	/**
	 * Returns an empty {@link Iterator}.
	 * @return The iterator.
	 * @param <T> The type of elements in the iterator.
	 */
	@SuppressWarnings("unchecked")
	public static <T> Iterator<T> emptyIterator()
	{
		return (Iterator<T>) EMPTY_ITERATOR;
	}
	
	/**
	 * Returns an unmodifiable {@link Iterator}.
	 * @return The iterator.
	 * @param <T> The type of elements in the iterator.
	 */
	public static <T> Iterator<T> unmodifiableIterator(Iterator<T> iterator)
	{
		return new UnmodifiableIterator<>(iterator);
	}
	
	/**
	 * Builds an array list from an iterator.
	 * @param iterator The source iterator.
	 * @return The list.
	 * @param <T> The type of elements in the iterator.
	 */
	public static <T> List<T> toArrayList(Iterator<T> iterator)
	{
		List<T> list = new ArrayList<T>();
		while(iterator.hasNext())
		{
			list.add(iterator.next());
		}
		return list;
	}
	
	/**
	 * Builds a new {@link ArrayIterator} from an array.
	 * @param array The array to iterate on.
	 * @return The new iterator.
	 * @param <T> The type of elements in the array.
	 */
	@SafeVarargs
	public static <T> Iterator<T> arrayIterator(T... array)
	{
		return new ArrayIterator<T>(array);
	}
	
	/**
	 * Encapsulates an {@link Iterator} into an {@link Iterable}. Please note
	 * that this is a simple encapsulation, you will be able to iterate over the
	 * new iterable only once.
	 * @param iterator The iterator.
	 * @return The new iterable.
	 * @param <T> The type of elements in the iterator.
	 */
	public static <T> Iterable<T> asIterable(Iterator<T> iterator)
	{
		return new IteratorIterable<T>(iterator);
	}
	
	private static final class IteratorIterable<T> implements Iterable<T>
	{
		private Iterator<T> iterator;

		private IteratorIterable(Iterator<T> iterator)
		{
			this.iterator = iterator;
		}

		@Override
		public Iterator<T> iterator()
		{
			return iterator;
		}
	}
}
