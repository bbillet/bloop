/*  
 * The Bloop Library.
 * Copyright (c) 2014 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.iterator;

import java.util.Iterator;
import java.util.NoSuchElementException;

import bloop.java.util.PrimitiveCollections;

/**
 * A set of iterators for arrays of primitive types.
 * 
 * @author Benjamin Billet
 * @version 0.1
 */
public class PrimitiveIterators
{
	private PrimitiveIterators() { }
	
	/**
	 * Creates a new Byte iterator from a byte array.
	 * @param array The array.
	 * @return The iterator.
	 */
	public static Iterator<Byte> iterator(byte[] array)
	{
		return new ByteIterator(array);
	}
	
	/**
	 * Creates a new Short iterator from a short array.
	 * @param array The array.
	 * @return The iterator.
	 */
	public static Iterator<Short> iterator(short[] array)
	{
		return new ShortIterator(array);
	}
	
	/**
	 * Creates a new Integer iterator from an int array.
	 * @param array The array.
	 * @return The iterator.
	 */
	public static Iterator<Integer> iterator(int[] array)
	{
		return new IntegerIterator(array);
	}
	
	/**
	 * Creates a new Long iterator from a long array.
	 * @param array The array.
	 * @return The iterator.
	 */
	public static Iterator<Long> iterator(long[] array)
	{
		return new LongIterator(array);
	}
	
	/**
	 * Creates a new Float iterator from a float array.
	 * @param array The array.
	 * @return The iterator.
	 */
	public static Iterator<Float> iterator(float[] array)
	{
		return new FloatIterator(array);
	}
	
	/**
	 * Creates a new Double iterator from a double array.
	 * @param array The array.
	 * @return The iterator.
	 */
	public static Iterator<Double> iterator(double[] array)
	{
		return new DoubleIterator(array);
	}
	
	/**
	 * Creates a new Boolean iterator from a boolean array.
	 * @param array The array.
	 * @return The iterator.
	 */
	public static Iterator<Boolean> iterator(boolean[] array)
	{
		return new BooleanIterator(array);
	}
	
	/**
	 * Creates a new Character iterator from a char array.
	 * @param array The array.
	 * @return The iterator.
	 */
	public static Iterator<Character> iterator(char[] array)
	{
		return new CharacterIterator(array);
	}
	
	/**
	 * Creates a new byte array from a Byte iterator, using
	 * {@link PrimitiveCollections#toByteArray(java.util.Collection)}.
	 * @param iterator The iterator.
	 * @return The array.
	 */
	public static byte[] toByteArray(Iterator<Byte> iterator)
	{
		return PrimitiveCollections.toByteArray(Iterators.toArrayList(iterator));
	}
	
	/**
	 * Creates a new short array from a Short iterator, using
	 * {@link PrimitiveCollections#toShortArray(java.util.Collection)}.
	 * @param iterator The iterator.
	 * @return The array.
	 */
	public static short[] toShortArray(Iterator<Short> iterator)
	{
		return PrimitiveCollections.toShortArray(Iterators.toArrayList(iterator));
	}
	
	/**
	 * Creates a new int array from an Integer iterator, using
	 * {@link PrimitiveCollections#toIntegerArray(java.util.Collection)}.
	 * @param iterator The iterator.
	 * @return The array.
	 */
	public static int[] toIntegerArray(Iterator<Integer> iterator)
	{
		return PrimitiveCollections.toIntegerArray(Iterators.toArrayList(iterator));
	}
	
	/**
	 * Creates a new long array from a Long iterator, using
	 * {@link PrimitiveCollections#toLongArray(java.util.Collection)}.
	 * @param iterator The iterator.
	 * @return The array.
	 */
	public static long[] toLongArray(Iterator<Long> iterator)
	{
		return PrimitiveCollections.toLongArray(Iterators.toArrayList(iterator));
	}
	
	/**
	 * Creates a new float array from a Float iterator, using
	 * {@link PrimitiveCollections#toFloatArray(java.util.Collection)}.
	 * @param iterator The iterator.
	 * @return The array.
	 */
	public static float[] toFloatArray(Iterator<Float> iterator)
	{
		return PrimitiveCollections.toFloatArray(Iterators.toArrayList(iterator));
	}
	
	/**
	 * Creates a new double array from a Double iterator, using
	 * {@link PrimitiveCollections#toDoubleArray(java.util.Collection)}.
	 * @param iterator The iterator.
	 * @return The array.
	 */
	public static double[] toDoubleArray(Iterator<Double> iterator)
	{
		return PrimitiveCollections.toDoubleArray(Iterators.toArrayList(iterator));
	}
	
	/**
	 * Creates a new boolean array from a Boolean iterator, using
	 * {@link PrimitiveCollections#toBooleanArray(java.util.Collection)}.
	 * @param iterator The iterator.
	 * @return The array.
	 */
	public static boolean[] toBooleanArray(Iterator<Boolean> iterator)
	{
		return PrimitiveCollections.toBooleanArray(Iterators.toArrayList(iterator));
	}
	
	/**
	 * Creates a new char array from a Character iterator, using
	 * {@link PrimitiveCollections#toCharacterArray(java.util.Collection)}.
	 * @param iterator The iterator.
	 * @return The array.
	 */
	public static char[] toCharacterArray(Iterator<Character> iterator)
	{
		return PrimitiveCollections.toCharacterArray(Iterators.toArrayList(iterator));
	}
	
	/**
	 * A Byte iterator for byte arrays.
	 * @see Iterator
	 * @author Benjamin Billet
	 */
	public static class ByteIterator implements Iterator<Byte>
	{
		private final byte[] array;
		private int next = 0;
		
		public ByteIterator(byte... array)
		{
			this.array = array;
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean hasNext()
		{
			return next < array.length;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Byte next()
		{
			if(next < array.length)
				return array[next++];
			else
				throw new NoSuchElementException();
		}
		
		/**
		 * @throws UnsupportedOperationException if called.
		 */
		@Override @Deprecated
		public void remove()
		{
			throw new UnsupportedOperationException("remove");
		}
	}
	
	/**
	 * A Short iterator for short arrays.
	 * @see Iterator
	 * @author Benjamin Billet
	 */
	public static class ShortIterator implements Iterator<Short>
	{
		private final short[] array;
		private int next = 0;
		
		public ShortIterator(short... array)
		{
			this.array = array;
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean hasNext()
		{
			return next < array.length;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Short next()
		{
			if(next < array.length)
				return array[next++];
			else
				throw new NoSuchElementException();
		}
		
		/**
		 * @throws UnsupportedOperationException if called.
		 */
		@Override @Deprecated
		public void remove()
		{
			throw new UnsupportedOperationException("remove");
		}
	}
	
	/**
	 * An Integer iterator for int arrays.
	 * @see Iterator
	 * @author Benjamin Billet
	 */
	public static class IntegerIterator implements Iterator<Integer>
	{
		private final int[] array;
		private int next = 0;
		
		public IntegerIterator(int... array)
		{
			this.array = array;
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean hasNext()
		{
			return next < array.length;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Integer next()
		{
			if(next < array.length)
				return array[next++];
			else
				throw new NoSuchElementException();
		}
		
		/**
		 * @throws UnsupportedOperationException if called.
		 */
		@Override @Deprecated
		public void remove()
		{
			throw new UnsupportedOperationException("remove");
		}
	}
	
	/**
	 * A Long iterator for long arrays.
	 * @see Iterator
	 * @author Benjamin Billet
	 */
	public static class LongIterator implements Iterator<Long>
	{
		private final long[] array;
		private int next = 0;
		
		public LongIterator(long... array)
		{
			this.array = array;
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean hasNext()
		{
			return next < array.length;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Long next()
		{
			if(next < array.length)
				return array[next++];
			else
				throw new NoSuchElementException();
		}
		
		/**
		 * @throws UnsupportedOperationException if called.
		 */
		@Override @Deprecated
		public void remove()
		{
			throw new UnsupportedOperationException("remove");
		}
	}
	
	/**
	 * A Boolean iterator for boolean arrays.
	 * @see Iterator
	 * @author Benjamin Billet
	 */
	public static class BooleanIterator implements Iterator<Boolean>
	{
		private final boolean[] array;
		private int next = 0;
		
		public BooleanIterator(boolean... array)
		{
			this.array = array;
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean hasNext()
		{
			return next < array.length;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Boolean next()
		{
			if(next < array.length)
				return array[next++];
			else
				throw new NoSuchElementException();
		}
		
		/**
		 * @throws UnsupportedOperationException if called.
		 */
		@Override @Deprecated
		public void remove()
		{
			throw new UnsupportedOperationException("remove");
		}
	}
	
	/**
	 * A Float iterator for float arrays.
	 * @see Iterator
	 * @author Benjamin Billet
	 */
	public static class FloatIterator implements Iterator<Float>
	{
		private final float[] array;
		private int next = 0;
		
		public FloatIterator(float... array)
		{
			this.array = array;
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean hasNext()
		{
			return next < array.length;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Float next()
		{
			if(next < array.length)
				return array[next++];
			else
				throw new NoSuchElementException();
		}
		
		/**
		 * @throws UnsupportedOperationException if called.
		 */
		@Override @Deprecated
		public void remove()
		{
			throw new UnsupportedOperationException("remove");
		}
	}
	
	/**
	 * A Double iterator for double arrays.
	 * @see Iterator
	 * @author Benjamin Billet
	 */
	public static class DoubleIterator implements Iterator<Double>
	{
		private final double[] array;
		private int next = 0;
		
		public DoubleIterator(double... array)
		{
			this.array = array;
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean hasNext()
		{
			return next < array.length;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Double next()
		{
			if(next < array.length)
				return array[next++];
			else
				throw new NoSuchElementException();
		}
		
		/**
		 * @throws UnsupportedOperationException if called.
		 */
		@Override @Deprecated
		public void remove()
		{
			throw new UnsupportedOperationException("remove");
		}
	}
	
	/**
	 * A Character iterator for char arrays.
	 * @see Iterator
	 * @author Benjamin Billet
	 */
	public static class CharacterIterator implements Iterator<Character>
	{
		private final char[] array;
		private int next = 0;
		
		public CharacterIterator(char... array)
		{
			this.array = array;
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean hasNext()
		{
			return next < array.length;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Character next()
		{
			if(next < array.length)
				return array[next++];
			else
				throw new NoSuchElementException();
		}
		
		/**
		 * @throws UnsupportedOperationException if called.
		 */
		@Override @Deprecated
		public void remove()
		{
			throw new UnsupportedOperationException("remove");
		}
	}
}
