/*  
 * The Bloop Library.
 * Copyright (c) 2014 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.iterator;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * An implementation of {@link Iterable} which encapsulates a set of existing
 * iterables.
 * 
 * @author Benjamin Billet
 * @version 0.1
 * @see Iterable
 * @param <T> the type of elements returned by the iterator.
 */
public class MultiIterable<T> implements Iterable<T>
{
	private List<Iterable<T>> internal;
	
	@SafeVarargs
	public MultiIterable(Iterable<T>... iterables)
	{
		internal = Arrays.asList(iterables);
	}
	
	public MultiIterable(Collection<? extends Iterable<T>> iterables)
	{
		internal  = new ArrayList<>(iterables);
	}
	
	/**
	 * Adds a new {@link Iterable} to the set of iterables managed by this
	 * MultiIterable.<br>
	 * Note: The new iterable will be available only to the new iterators
	 * produced by calling {@link MultiIterable#iterator()}. Old iterators
	 * instances will ignore the new iterable.
	 */
	public void add(Iterable<T> iterable)
	{
		internal.add(iterable);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<T> iterator()
	{
		return new MultiIterator();
	}
	
	// this is a copy of bloop.java.util.MultiIterator, but this implementation
	// deals with Iterable instead of Iterator
	private class MultiIterator implements Iterator<T>
	{
		private Iterator<Iterable<T>> globalIterator;
		private Iterator<T> currentIterator;
		
		public MultiIterator()
		{
			globalIterator = internal.iterator();
			if(globalIterator.hasNext())
				currentIterator = globalIterator.next().iterator();
		}
		
		@Override
		public boolean hasNext()
		{
			if(currentIterator == null)
				return false;
			
			skipEmpty();
			return currentIterator.hasNext();
		}

		@Override
		public T next()
		{
			if(currentIterator == null)
				throw new NoSuchElementException();
			
			skipEmpty();
			return currentIterator.next();
		}
		
		@Override
	    public void remove()
	    {
			if(currentIterator != null)
				currentIterator.remove();
	    }
		
		private void skipEmpty()
		{
			while(currentIterator.hasNext() == false && globalIterator.hasNext())
			{
				currentIterator = globalIterator.next().iterator();
			}
		}
	}
}
