/*  
 * The Bloop Library.
 * Copyright (c) 2014 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.iterator;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * An iterator that iterates over a sequence of sub-iterators.
 * 
 * @author Benjamin Billet
 * @param <T> The type of elements returned by this iterator.
 * @see Iterator
 * @version 0.1
 */
public class MultiIterator<T> implements Iterator<T>
{
	private Iterator<Iterator<T>> globalIterator;
	private Iterator<T> currentIterator;
	
	/**
	 * Constructs a new multi iterator.
	 * @param iterators The underlying iterators.
	 */
	public MultiIterator(Collection<Iterator<T>> iterators)
	{
		this.globalIterator = iterators.iterator();
		if(globalIterator.hasNext())
			currentIterator = globalIterator.next();
	}

	/**
	 * Constructs a new multi iterator.
	 * @param iterators The underlying iterators.
	 */
	@SafeVarargs
	public MultiIterator(Iterator<T>... iterators)
	{
		this.globalIterator = Arrays.asList(iterators).iterator();
		if(globalIterator.hasNext())
			currentIterator = globalIterator.next();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext()
	{
		if(currentIterator == null)
			return false;
		
		skipEmpty();
		return currentIterator.hasNext();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T next()
	{
		if(currentIterator == null)
			throw new NoSuchElementException();
		
		skipEmpty();
		return currentIterator.next();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
    public void remove()
    {
		if(currentIterator != null)
			currentIterator.remove();
    }
	
	private void skipEmpty()
	{
		while(currentIterator.hasNext() == false && globalIterator.hasNext())
		{
			currentIterator = globalIterator.next();
		}
	}
}
