/*  
 * The Bloop Library.
 * Copyright (c) 2014 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.iterator;

import java.util.Iterator;

/**
 * An {@link Iterable} that encapsulates arrays.
 * @author BenjaminBillet
 * @see Iterable
 * @see ArrayIterator
 * @version 0.1
 * @param <T> The type of elements managed by this iterable.
 */
public class ArrayIterable<T> implements Iterable<T>
{
	private final T[] array;
	
	/**
	 * Constructs a new {@link Iterable} that encapsulates an array.
	 * @param array The array.
	 */
	@SafeVarargs
	public ArrayIterable(T... array)
	{
		this.array = array;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<T> iterator()
	{
		return new ArrayIterator<>(array);
	}
}
