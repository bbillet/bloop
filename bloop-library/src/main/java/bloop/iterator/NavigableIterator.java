/*  
 * The Bloop Library.
 * Copyright (c) 2014 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.iterator;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * An iterator interface enhanced with extended navigation methods (jump to a
 * position, reverse iteration, etc.).
 * 
 * @author Benjamin Billet
 * @param <T> The type of elements returned by this iterator.
 * @see Iterator
 * @version 0.1
 */
public interface NavigableIterator<T> extends Iterator<T>
{
	/**
	 * Returns {@code true} if this iterator has more elements when iterating in
	 * the reverse direction. (In other words, returns {@code true} if
	 * {@link #previous()} would return an element rather than throwing an
	 * exception.)
	 * @return {@code true} If the iterator is not positioned on the first
	 *         element.
	 */
	public boolean hasPrevious();

	/**
	 * Returns the previous element in the iterator and moves the cursor
	 * position backwards. This method may be called repeatedly to iterate
	 * backwards, or intermixed with calls to {@link #next()} to go back and
	 * forth. (Note that alternating calls to {@code next} and {@code previous}
	 * will return the same element repeatedly.)
	 * @return The previous element in the list.
	 * @throws NoSuchElementException If the iteration has no previous element.
	 */
	public T previous();

	/**
	 * Returns the current element on which this iterator is positioned.
	 */
	public T current();

	/**
	 * Returns the index of the current element on which this iterator is
	 *         positioned.
	 */
	public int index();

	/**
	 * Returns the number of iterable elements.
	 */
	public int size();

	/**
	 * Checks whether an index is valid regarding the iterator, i.e. if this
	 * iterator can be positioned at this index.
	 * @param index The index.
	 * @return {@code true} If index is valid.
	 */
	public boolean indexExists(int index);

	/**
	 * Jumps to a specific position.
	 * @param index The position.
	 * @throws IllegalArgumentException If index is negative, or if index is
	 *             greater than the size of the iterator.
	 */
	public void jumpTo(int index);

	/**
	 * Performs {@code n} iterations (equivalent to call {@link #next} or
	 * {@link #previous} {@code n} times).
	 * @param n The number of steps. If negative, reverse iteration is
	 *            performed.
	 * @throws IllegalArgumentException Iff {@code index() + n} is negative, or
	 *             if {@code index() + n} is greater than the size of the
	 *             iterator.
	 */
	public void jump(int n);
}
