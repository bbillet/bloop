/*  
 * The Bloop Library.
 * Copyright (c) 2014 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.iterator;

import java.util.Iterator;

/**
 * An iterator with counting capabilities.
 * 
 * @author Benjamin Billet
 * @param <T> the type of elements returned by this iterator.
 * @see Iterator
 * @version 0.1
 */
public class CountingIterator<T> implements Iterator<T>
{
	private Iterator<T> internal;
	private int count = 0;
	private int removed = 0;
	
	public CountingIterator(Iterator<T> iterator)
	{
		this.internal = iterator;
	}
	
	public CountingIterator(Iterable<T> iterable)
	{
		this.internal = iterable.iterator();
	}
	
	/** 
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext()
	{
		return internal.hasNext();
	}

	/** 
	 * {@inheritDoc}
	 */
	@Override
	public T next()
	{
		T next = internal.next();
		count++;
		return next;
	}
	
	/** 
	 * {@inheritDoc}
	 */
	@Override
	public void remove()
	{
		internal.remove();
		removed++;
	}
	
	/**
	 * Returns the number of elements counted by this iterator.
	 */
	public int getCount()
	{
		return count;
	}
	
	/**
	 * Returns the number of elements removed by this iterator.
	 */
	public int getRemovedCount()
	{
		return removed;
	}
}
