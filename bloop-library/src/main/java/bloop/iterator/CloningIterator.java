/*  
 * The Bloop Library.
 * Copyright (c) 2014 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.iterator;

import java.lang.reflect.Method;
import java.util.Iterator;

import bloop.java.lang.reflect.Methods;

/**
 * An iterator that wraps an existing iterator, and clones the elements while
 * iterating.
 * 
 * @author Benjamin Billet
 * @param <T> The type of elements returned by this iterator.
 * @see Iterator
 * @version 0.1
 */
public class CloningIterator<T extends Cloneable> implements Iterator<T>
{
	private Iterator<T> iterator;
	private Method cloneMethod;
	
	/**
	 * Constructs a new cloning iterator.
	 * @param iterator The underlying iterator.
	 * @param contentClass The type of the elements contained in the iterator.
	 * @throws CloneNotSupportedException If the type of objects are not
	 *             providing a public {@code clone()} method.
	 */
	public CloningIterator(Iterator<T> iterator, Class<T> contentClass) throws CloneNotSupportedException
	{
		this.iterator = iterator;
		this.cloneMethod = Methods.findCloneMethod(contentClass);
		if(cloneMethod == null)
			throw new CloneNotSupportedException("'clone()' method not found");
	}
	
	// used only by CloningIterable
	CloningIterator(Iterator<T> iterator, Method cloneMethod)
	{
		this.iterator = iterator;
		this.cloneMethod = cloneMethod;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext()
	{
		return iterator.hasNext();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public T next()
	{
		Cloneable object = iterator.next();
		try
		{
			return (T) cloneMethod.invoke(object);
		}
		catch (ReflectiveOperationException e)
		{
			// should never happen, because the iterator is constructed only if
			// a satisfying clone() method is provided
			throw new IllegalStateException(e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
    public void remove()
    {
		iterator.remove();
    }
}
