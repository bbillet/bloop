/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.security;

/**
 * A set of methods for comparing arrays of elements. These methods compare all
 * the element of each array, in order to mitigate timing attacks.
 * @author Benjamin Billet
 * @version 0.1
 */
public class SafeArrayEquals
{
	/**
	 * Compares two arrays of byte values.
	 * @return {@code true} if the arrays are equals, {@code false} otherwise.
	 * @throws NullPointerException If {@code array1} or {@code array2} is
	 *             {@code null}.
	 */
	public static boolean equals(byte[] array1, byte[] array2)
	{
		if(array1.length == array2.length)
		{
			byte result = 0;
			for (int i = 0; i < array1.length; i++)
			{
				result |= array1[i] ^ array2[i];
			}
			return result == 0;
		}
		else
			return false;
	}
	
	/**
	 * Compares two arrays of short values.
	 * @return {@code true} if the arrays are equals, {@code false} otherwise.
	 * @throws NullPointerException If {@code array1} or {@code array2} is
	 *             {@code null}.
	 */
	public static boolean equals(short[] array1, short[] array2)
	{
		if(array1.length == array2.length)
		{
			short result = 0;
			for (int i = 0; i < array1.length; i++)
			{
				result |= array1[i] ^ array2[i];
			}
			return result == 0;
		}
		else
			return false;
	}
	
	/**
	 * Compares two arrays of int values.
	 * @return {@code true} if the arrays are equals, {@code false} otherwise.
	 * @throws NullPointerException If {@code array1} or {@code array2} is
	 *             {@code null}.
	 */
	public static boolean equals(int[] array1, int[] array2)
	{
		if(array1.length == array2.length)
		{
			int result = 0;
			for (int i = 0; i < array1.length; i++)
			{
				result |= array1[i] ^ array2[i];
			}
			return result == 0;
		}
		else
			return false;
	}
	
	public static boolean equals(long[] array1, long[] array2)
	{
		if(array1.length == array2.length)
		{
			long result = 0;
			for (int i = 0; i < array1.length; i++)
			{
				result |= array1[i] ^ array2[i];
			}
			return result == 0;
		}
		else
			return false;
	}
	
	/**
	 * Compares two arrays of char values.
	 * @return {@code true} if the arrays are equals, {@code false} otherwise.
	 * @throws NullPointerException If {@code array1} or {@code array2} is
	 *             {@code null}.
	 */
	public static boolean equals(char[] array1, char[] array2)
	{
		if(array1.length == array2.length)
		{
			char result = 0;
			for (int i = 0; i < array1.length; i++)
			{
				result |= array1[i] ^ array2[i];
			}
			return result == 0;
		}
		else
			return false;
	}
	
	/**
	 * Compares two arrays of boolean values.
	 * @return {@code true} if the arrays are equals, {@code false} otherwise.
	 * @throws NullPointerException If {@code array1} or {@code array2} is
	 *             {@code null}.
	 */
	public static boolean equals(boolean[] array1, boolean[] array2)
	{
		if(array1.length == array2.length)
		{
			boolean equals = true;
			for (int i = 0; i < array1.length; i++)
			{
				if(array1[i] != array2[i])
					equals = false;
			}
			return equals;
		}
		else
			return false;
	}
	
	/**
	 * Compares two arrays of doubles ({@link Float#compare(float, float)} is
	 * used for the comparison).
	 * @return {@code true} if the arrays are equals, {@code false} otherwise.
	 * @throws NullPointerException If {@code array1} or {@code array2} is
	 *             {@code null}.
	 */
	public static <T> boolean equals(float[] array1, float[] array2)
	{
		if(array1.length == array2.length)
		{
			boolean equals = true;
			for (int i = 0; i < array1.length; i++)
			{
				if(Float.compare(array1[i], array2[i]) != 0)
					equals = false;
			}
			return equals;
		}
		else
			return false;
	}
	
	/**
	 * Compares two arrays of doubles ({@link Double#compare(double, double)} is
	 * used for the comparison).
	 * @return {@code true} if the arrays are equals, {@code false} otherwise.
	 * @throws NullPointerException If {@code array1} or {@code array2} is
	 *             {@code null}.
	 */
	public static <T> boolean equals(double[] array1, double[] array2)
	{
		if(array1.length == array2.length)
		{
			boolean equals = true;
			for (int i = 0; i < array1.length; i++)
			{
				if(Double.compare(array1[i], array2[i]) != 0)
					equals = false;
			}
			return equals;
		}
		else
			return false;
	}
	
	/**
	 * Compares two arrays of objects.
	 * @return {@code true} if the arrays are equals, {@code false} otherwise.
	 * @throws NullPointerException If {@code array1} or {@code array2} is
	 *             {@code null}.
	 */
	public static <T> boolean equals(T[] array1, T[] array2)
	{
		if(array1.length == array2.length)
		{
			boolean equals = true;
			for (int i = 0; i < array1.length; i++)
			{
				if(array1[i].equals(array2[i]) == false)
					equals = false;
			}
			return equals;
		}
		else
			return false;
	}
}
