/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.nio;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

/**
 * An adapter for using a sequence of NIO {@link ByteBuffer} as an
 * {@link InputStream}.<br>
 * <b>Warning</b>: the byte buffers MUST be in read mode (see
 * {@link java.nio.Buffer#flip()}). If not, the behavior is undefined.
 * @author Benjamin Billet
 * @see ByteBuffer
 * @see java.nio.Buffer#flip()
 * @see InputStream
 * @version 0.1
 */
public class ByteBufferInputStream extends InputStream
{
	private Deque<ByteBuffer> queue;
	private ByteBuffer current;
	
	/**
	 * Creates a new ByteBufferInputStream from a set of {@link ByteBuffer}.<br>
	 * <b>Warning</b>: the byte buffers MUST be in read mode. If not, the behavior is
	 * undefined.
	 * @param buffers The buffers.
	 * @see ByteBuffer
	 * @throws IllegalArgumentException If {@code buffers} is empty.
	 */
	public ByteBufferInputStream(ByteBuffer... buffers)
	{
		this(Arrays.asList(buffers));
	}
	
	/**
	 * Creates a new ByteBufferInputStream from a set of {@link ByteBuffer}.<br>
	 * <b>Warning</b>: the byte buffers MUST be in read mode. If not, the behavior is
	 * undefined.
	 * @param buffers The buffers.
	 * @see ByteBuffer
	 * @throws IllegalArgumentException If {@code buffers} is empty.
	 */
	public ByteBufferInputStream(List<ByteBuffer> buffers)
	{
		if(buffers.size() == 0)
			throw new IllegalArgumentException("no buffer");
		
		queue = new LinkedList<ByteBuffer>(buffers);
	}
	
	/**
	 * Adds a new {@link ByteBuffer} as a source for this stream.<br>
	 * <b>Warning</b>: the byte buffer MUST be in read mode. If not, the behavior is
	 * undefined.
	 * @param buffer The buffer.
	 * @see ByteBuffer
	 */
	public void addBuffer(ByteBuffer buffer)
	{
		queue.add(buffer);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read() throws IOException
	{
		if(current == null)
		{
			next();
			if(current == null)
				return -1;
		}
		
		while(current.hasRemaining() == false)
		{
			next();
			if(current == null)
				return -1;
		}
		
		return current.get();
	}
	
	private void next()
	{
		current = queue.poll();
	}
}
