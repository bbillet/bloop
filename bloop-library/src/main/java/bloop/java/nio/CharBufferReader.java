/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.nio;

import java.io.IOException;
import java.io.Reader;
import java.nio.CharBuffer;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

/**
 * An adapter for using a sequence of NIO {@link CharBuffer} as a {@link Reader}.
 * <br>
 * <b>Warning</b>: the char buffers MUST be in read mode (see
 * {@link java.nio.Buffer#flip()}). If not, the behavior is undefined.
 * @author Benjamin Billet
 * @see CharBuffer
 * @see Reader
 * @see java.nio.Buffer#flip()
 * @version 0.1
 */
public class CharBufferReader extends Reader
{
	private Deque<CharBuffer> queue;
	private CharBuffer current;
	
	/**
	 * Creates a new CharBufferReader from a set of {@link CharBuffer}.<br>
	 * <b>Warning</b>: the char buffers MUST be in read mode. If not, the behavior is
	 * undefined.
	 * @param buffers The buffers.
	 * @see CharBuffer
	 * @throws IllegalArgumentException If {@code buffers} is empty.
	 */
	public CharBufferReader(CharBuffer... buffers)
	{
		this(Arrays.asList(buffers));
	}
	
	/**
	 * Creates a new CharBufferReader from a set of {@link CharBuffer}.<br>
	 * <b>Warning</b>: the char buffers MUST be in read mode. If not, the behavior is
	 * undefined.
	 * @param buffers The buffers.
	 * @see CharBuffer
	 * @throws IllegalArgumentException If {@code buffers} is empty.
	 */
	public CharBufferReader(List<CharBuffer> buffers)
	{
		if(buffers.size() == 0)
			throw new IllegalArgumentException("no buffer");
		
		queue = new LinkedList<CharBuffer>(buffers);
	}
	
	/**
	 * Adds a new {@link CharBuffer} as a source for this stream.<br>
	 * <b>Warning</b>: the char buffer MUST be in read mode. If not, the behavior is
	 * undefined.
	 * @param buffer The buffer.
	 * @see CharBuffer
	 */
	public void addBuffer(CharBuffer buffer)
	{
		queue.add(buffer);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read() throws IOException
	{
		if(current == null)
		{
			next();
			if(current == null)
				return -1;
		}
		
		while(current.hasRemaining() == false)
		{
			next();
			if(current == null)
				return -1;
		}
		
		return current.get();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read(char[] chars, int offset, int length) throws IOException
	{
		if (chars == null)
			throw new NullPointerException();
		else if (offset < 0 || length < 0 || length > chars.length - offset)
			throw new IndexOutOfBoundsException();
		else if (length == 0)
			return 0;

        int c = read();
        if (c == -1)
            return -1;
        
        chars[offset] = (char) c;

		int read = 1;
		for (; read < length; read++)
		{
			c = read();
			if (c == -1)
				break;
			chars[offset + read] = (char) c;
		}
        return read;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException
	{
	}
	
	private void next()
	{
		current = queue.poll();
	}
}
