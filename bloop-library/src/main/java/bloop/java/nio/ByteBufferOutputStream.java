/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.nio;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

/**
 * An adapter for using a sequence of NIO {@link ByteBuffer} as an
 * {@link OutputStream}.<br>
 * <b>Warning</b>: the byte buffers MUST be empty (see {@link java.nio.Buffer#clear()}
 * ). If not, the behavior is undefined.
 * @author Benjamin Billet
 * @see ByteBuffer
 * @see OutputStream
 * @see java.nio.Buffer#clear()
 * @version 0.1
 */
public class ByteBufferOutputStream extends OutputStream
{
	private Deque<ByteBuffer> queue;
	private ByteBuffer current;
	
	/**
	 * Creates a new ByteBufferOutputStream from a set of {@link ByteBuffer}.<br>
	 * <b>Warning</b>: the byte buffers MUST be empty. If not, the behavior is
	 * undefined.
	 * @param buffers The buffers.
	 * @see ByteBuffer
	 * @throws IllegalArgumentException If {@code buffers} is empty.
	 */
	public ByteBufferOutputStream(ByteBuffer... buffers)
	{
		this(Arrays.asList(buffers));
	}
	
	/**
	 * Creates a new ByteBufferOutputStream from a set of {@link ByteBuffer}.<br>
	 * <b>Warning</b>: the byte buffers MUST be empty. If not, the behavior is
	 * undefined.
	 * @param buffers The buffers.
	 * @see ByteBuffer
	 * @throws IllegalArgumentException If {@code buffers} is empty.
	 */
	public ByteBufferOutputStream(List<ByteBuffer> buffers)
	{
		if(buffers.size() == 0)
			throw new IllegalArgumentException("no buffer");
		
		queue = new LinkedList<ByteBuffer>(buffers);
	}
	
	/**
	 * Adds a new {@link ByteBuffer} as a destination for this stream.<br>
	 * <b>Warning</b>: the byte buffer MUST be empty. If not, the behavior is
	 * undefined.
	 * @param buffer The buffer.
	 * @see ByteBuffer
	 */
	public void addBuffer(ByteBuffer buffer)
	{
		queue.add(buffer);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(int b) throws IOException
	{
		if(isFull())
			throw new IOException("buffers are full");

		if(current.position() < current.capacity() - 1)
			current.put((byte) b);	
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write (byte[] bytes, int offset, int length) throws IOException
	{
		if(isFull())
			throw new IOException("buffers are full");
		
		if(current.capacity() - current.position() < length)
		{
			int written = 0;
			do
			{
				int len = current.capacity() - current.position();
				if(written + len > length)
					len = length - written;
				
				current.put(bytes, offset + written, len);
				written += len;
				
				if(written == length)
					break;
				
				if(isFull())
					throw new IOException("buffers are full");
			}
			while(true);
		}
		else
			current.put(bytes, offset, length);
	}

	/**
	 * Returns {@code true} if this {@link OutputStream} is full, {@code false}
	 * otherwise.
	 */
	public boolean isFull()
	{
		if(current == null || current.position() == current.capacity())
		{
			next();
			if(current == null)
				return true;
		}
		
		return false;
	}
	
	private void next()
	{
		do
		{
			current = queue.poll();
		}
		while(current != null 
				&& (current.capacity() == 0 
					|| current.position() == current.capacity()));
	}
}
