/*  
 * The Bloop Library.
 * Copyright (c) 2012 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bloop.java.lang;

import bloop.java.util.Bytes;

/**
 * Helper functions for displaying the content of a byte array as bits, in a
 * human-readable format.
 * 
 * <pre>
 * 0                     1                     2                     3  
 * 0 1 2 3 4 5 6 7   8 9 0 1 2 3 4 5   6 7 8 9 0 1 2 3   4 5 6 7 8 9 0 1
 * ---------------------------------------------------------------------
 * 0 1 1 0 0 0 1 0   0 1 1 0 0 1 0 1   0 1 1 0 1 1 1 0   0 1 1 0 1 0 1 0
 * 0 1 1 0 0 0 0 1   0 1 1 0 1 1 0 1   0 1 1 0 1 0 0 1   0 1 1 0 1 1 1 0
 * 0 1 1 0 0 0 1 0   0 1 1 0 1 0 0 1   0 1 1 0 1 1 0 0   0 1 1 0 1 1 0 0
 * 0 1 1 0 0 1 0 1   0 1 1 1 0 1 0 0   0 0 1 0 1 1 1 0   0 1 1 0 0 1 1 0
 * 0 1 1 1 0 0 1 0   0 0 0 1 1 0 0 0
 * </pre>
 * 
 * @author Benjamin Billet
 * @version 0.1
 */
public final class BitPrettyPrint
{
	private BitPrettyPrint() { }
	
	private static final int BytesPerLine = 4;
	private static final int LineSize = 69;
	
	/**
	 * Nicely prints an array of bytes as a sequence of bits, into a String.
	 */
	public static String prettyPrint(byte[] bytes)
	{
		return prettyPrint(bytes, 0, bytes.length);
	}
	
	/**
	 * Nicely prints an array of bytes as a sequence of bits, into a String,
	 * into a String, skipping {@code offset} bytes and stopping after printing
	 * {@code length} bytes.
	 * @param bytes The array of bytes.
	 * @param offset The number of bytes to skip.
	 * @param length The number of bytes to print.
	 */
	public static String prettyPrint(byte[] bytes, int offset, int length)
	{
		StringBuilder builder = new StringBuilder();
		prettyPrint(builder, bytes, offset, length);
		return builder.toString();
	}
	
	/**
	 * Nicely prints an array of bytes as a sequence of bits, into a
	 * {@link StringBuilder}, skipping {@code offset} bytes and stopping after
	 * printing {@code length} bytes.
	 * @param builder The {@link StringBuilder}.
	 * @param bytes The array of bytes.
	 * @param offset The number of bytes to skip.
	 * @param length The number of bytes to print.
	 */
	public static void prettyPrint(StringBuilder builder, byte[] bytes, int offset, int length)
	{
		int lines = length / BytesPerLine;
		char[] lineBuffer = new char[LineSize];
		
		appendHeader(builder);
		
		// writes full lines
		for (int i = 0; i < lines; i++)	
		{
			builder.append(prettyLine(lineBuffer, bytes, offset + (i * BytesPerLine), BytesPerLine));
			builder.append('\n');
		}

		// last incomplete line
		int mod = length % BytesPerLine;
		if (mod != 0)
		{
			builder.append(prettyLine(lineBuffer, bytes, offset + (lines * BytesPerLine), mod));
			builder.append('\n');
		}
	}
	
	private static void appendHeader(StringBuilder builder)
	{
		builder.append("0                     1                     2                     3  ");
		builder.append('\n');
		builder.append("0 1 2 3 4 5 6 7   8 9 0 1 2 3 4 5   6 7 8 9 0 1 2 3   4 5 6 7 8 9 0 1");
		builder.append('\n');
		builder.append("---------------------------------------------------------------------");
		builder.append('\n');
	}
	
	private static char[] prettyLine(char[] lineBuffer, byte[] bytes, int offset, int length)
	{
		if(length > BytesPerLine || length <= 0 || offset + length > bytes.length)
			throw new IllegalArgumentException();
		
		int k = 0;
		for(int i = 0; i < BytesPerLine; i++)
		{
			if (i > 0)
				k += 2;
			
			for(int j = 7; j >= 0; j--)
			{
				if(i < length)
					lineBuffer[k++] = (char) (48 + Bytes.getBit(bytes[offset + i], j));
				else
					lineBuffer[k++] = ' ';
				k++;
			}
		}
		return lineBuffer;
	}
}