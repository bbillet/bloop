/*  
 * The Bloop Library.
 * Copyright (c) 2012 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bloop.java.lang;

/**
 * Helper functions for displaying the content of a byte array in a
 * human-readable format, similar to a network sniffer.
 * 
 * <pre>
 * 0000   23 00 00 00 84 2b 23 01  00 00 00 81 b9 84 9b 0a   +....+#. ........
 * 0010   01 15 00 00 00 01 00 00  11 62 65 6e 6a 61 6d 69   ........ .benjami
 * 0020   6e 62 69 6c 6c 65 74 2e  66 72 18 00 00 00 10 d2   nbillet. fr......
 * 0030   c7 23 dd c7 80 46 e7 85  33 12 00 99 bb e9 d3 0d   .#...F.. 3.......
 * 0040   00 01 5e 10 6b 01 04 00  00 00 00 00 00 00 02 00   ..^.k... ........
 * 0050   00 00 00 00 01 ce 10                               .......
 * </pre>
 * 
 * @author Benjamin Billet
 * @version 0.1
 */
public final class PrettyPrint
{
	private PrettyPrint() { }
	
	private static final int BytesPerLine = 16;
	private static final int HexLineNumberSize = 4;
	private static final int ASCIIOffset = 58;
	private static final int LineSize = 75;
	
	private static final char[] HexDigits = 
		{ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
	
	/**
	 * Nicely prints an array of bytes into a String.
	 */
	public static String prettyPrint(byte[] bytes)
	{
		return prettyPrint(bytes, 0, bytes.length);
	}
	
	/**
	 * Nicely prints an array of bytes into a String, skipping {@code offset}
	 * bytes and stopping after printing {@code length} bytes.
	 * @param bytes The array of bytes.
	 * @param offset The number of bytes to skip.
	 * @param length The number of bytes to print.
	 */
	public static String prettyPrint(byte[] bytes, int offset, int length)
	{
		StringBuilder builder = new StringBuilder();
		prettyPrint(builder, bytes, offset, length);
		return builder.toString();
	}
	
	/**
	 * Nicely prints an array of bytes into a {@link StringBuilder}, skipping
	 * {@code offset} bytes and stopping after printing {@code length} bytes.
	 * @param builder The {@link StringBuilder}.
	 * @param bytes The array of bytes.
	 * @param offset The number of bytes to skip.
	 * @param length The number of bytes to print.
	 */
	public static void prettyPrint(StringBuilder builder, byte[] bytes, int offset, int length)
	{
		int lines = length / BytesPerLine;
		char[] lineBuffer = new char[LineSize];
		
		// writes full lines
		for (int i = 0; i < lines; i++)	
		{
			builder.append(prettyLine(lineBuffer, bytes, i, offset + (i * BytesPerLine), BytesPerLine));
			builder.append('\n');
		}

		// last incomplete line
		int mod = length % BytesPerLine;
		if (mod != 0)
		{
			builder.append(prettyLine(lineBuffer, bytes, lines, offset + (lines * BytesPerLine), mod));
			builder.append('\n');
		}
	}
	
	private static char[] prettyLine(char[] lineBuffer, byte[] bytes, int lineNumber, int offset, int length)
	{
		if(length > BytesPerLine || length <= 0 || offset + length > bytes.length)
			throw new IllegalArgumentException();
		
		lineNumber = lineNumber * BytesPerLine;
		
		int j = 0;
		// write the line number
		for (int i = 1; i <= HexLineNumberSize; i++)
		{
			lineBuffer[j++] = HexDigits[(lineNumber >> 4 * (HexLineNumberSize - i)) & 0x0F];
		}
		
		j += 2; // jump separator
		
		int k = ASCIIOffset;
		for(int i = 0; i < BytesPerLine; i++)
		{
			if (i != 8)
				j++;
			else 
			{
				j += 2; // double space after 8 bytes
				k++; // space separation after 8 ASCII chars
			}
			
			if(i < length)
			{
				// write byte
				int byteVal = bytes[offset + i] & 0xFF;
				lineBuffer[j++] = HexDigits[byteVal >> 4];
				lineBuffer[j++] = HexDigits[byteVal & 0x0F];
				
				// write ASCII
				if (byteVal < ' ' || byteVal > '~')
					lineBuffer[k++] = '.';
				else
					lineBuffer[k++] = (char) byteVal;
			}
			else // fill remaining with spaces
			{
				lineBuffer[j++] = ' ';
				lineBuffer[j++] = ' ';
				lineBuffer[k++] = ' ';
			}
		}
		
		return lineBuffer;
	}
}