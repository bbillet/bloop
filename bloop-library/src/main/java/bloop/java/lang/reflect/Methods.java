/*  
 * The Bloop Library.
 * Copyright (c) 2015 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.lang.reflect;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * Helpers for the {@link Method} class, which provides information about a
 * single method on a class or interface.
 * @author Benjamin Billet
 * @version 0.1
 */
public class Methods
{
	/**
	 * Looks for a public {@link Object#clone()} method. The
	 * @param cloneableClass The class which possibly contains a
	 *        {@link Object#clone()} method.
	 * @return The {@link Method} instance, or {@code null} if a public
	 *         {@link Object#clone()} method is not defined.
	 */
	public static Method findCloneMethod(Class<? extends Cloneable> cloneableClass)
	{
		for(Method method : cloneableClass.getMethods())
		{
			if("clone".equals(method.getName())
					&& method.getParameterTypes().length == 0
					&& Modifier.isPublic(method.getModifiers()))
			{
				return method;
			}
		}
		return null;
	}
}
