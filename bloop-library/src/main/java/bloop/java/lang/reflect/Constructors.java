/*  
 * The Bloop Library.
 * Copyright (c) 2015 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.lang.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

/**
 * Helpers for the {@link Constructor} class, which provides information about a
 * a single constructor for a class.
 * @author Benjamin Billet
 * @version 0.1
 */
public class Constructors
{
	/**
	 * Looks for a default constructor (a constructor without parameters).
	 * @param aClass The class which possibly contains a default constructor.
	 * @param publicOnly If {@code true}, this method will only look for a
	 *        public default constructor.
	 * @return The {@link Constructor} instance, or {@code null} if the default
	 *         constructor is not defined.
	 */
	public static Constructor<?> findDefaultConstructor(Class<?> aClass, boolean publicOnly)
	{
		for(Constructor<?> constructor : aClass.getConstructors())
		{
			if(constructor.getParameterTypes().length == 0)
			{
				if(publicOnly)
				{
					if(Modifier.isPublic(constructor.getModifiers()))
						return constructor;
				}
				else
					return constructor;
			}
		}
		return null;
	}
}
