/*  
 * The Bloop Library.
 * Copyright (c) 2015 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

/**
 * A comparator for multi-criteria comparison. Given two elements A and B, this
 * comparator will use the first comparator in the chain. If the results are
 * equals, this comparator will try the second comparator in the chain and so
 * on.
 * <pre>{@code
 * Comparator<String> comparator = new ComparatorChain<>(
 * 		firstComparator,
 * 		secondComparator,
 * 		...);
 * }</pre>
 * This comparator can be elegantly combined with enum comparators:
 * <pre>{@code
 * Comparator<Entry<String, String>> comparator = new ComparatorChain<>(
 * 		SortParameter.KEY_ASC, 
 * 		SortParameter.VALUE_ASC);
 *  
 * private static enum SortParameter implements Comparator<Entry<String, String>> 
 * {
 * 	KEY_ASC 
 * 	{
 * 		public int compare(Entry<String, String> value1, Entry<String, String> value2)
 * 		{
 * 			return value1.getKey().compareTo(value2.getKey());
 * 		}
 * 	},
 * 	VALUE_ASC
 * 	{
 * 		public int compare(Entry<String, String> value1, Entry<String, String> value2)
 * 		{
 * 			return value1.getValue().compareTo(value2.getValue());
 * 		}
 * 	}
 * };
 * }</pre>
 * 
 * @author Benjamin Billet
 * @version 0.1
 * 
 * @param <T> The type of elements compared by this comparator.
 */
public class ComparatorChain<T> implements Comparator<T>
{
	private List<Comparator<T>> chainedComparators;
	
	/**
	 * Creates a new {@link ComparatorChain} from an array of comparators.
	 */
	@SafeVarargs
	public ComparatorChain(Comparator<T>... comparators) 
	{
		this(Arrays.asList(comparators));
	}
	
	/**
	 * Creates a new {@link ComparatorChain} from a collection of comparators.
	 */
	public ComparatorChain(Collection<? extends Comparator<T>> comparators) 
	{
		chainedComparators = new ArrayList<>(comparators);
	}
	
	/**
	 * Adds a new comparator at the end of this chain.
	 * @return <code>true</code> If the comparator was successfully added,
	 *         <code>false</code> elsewhere.
	 */
	public boolean add(Comparator<T> comparator)
	{
		return chainedComparators.add(comparator);
	}

	/**
	 * Inserts a new comparator at a given position in the chain.
	 */
	public void add(int position, Comparator<T> comparator)
	{
		chainedComparators.add(position, comparator);
	}

	/**
	 * Checks if a comparator exists in the chain.
	 */
	public boolean contains(Comparator<T> comparator)
	{
		return chainedComparators.contains(comparator);
	}

	/**
	 * Returns the comparator at a given position.
	 */
	public Comparator<T> get(int position)
	{
		return chainedComparators.get(position);
	}

	/**
	 * Returns the position of the first occurence of comparator.
	 */
	public int indexOf(Comparator<T> comparator)
	{
		return chainedComparators.indexOf(comparator);
	}

	/**
	 * Checks if this chain is empty.
	 */
	public boolean isEmpty()
	{
		return chainedComparators.isEmpty();
	}

	/**
	 * Removes a comparator at a given position.
	 */
	public Comparator<T> remove(int position)
	{
		return chainedComparators.remove(position);
	}

	/**
	 * Removes all the occurences of a comparator.
	 */
	public boolean remove(Comparator<T> comparator)
	{
		return chainedComparators.remove(comparator);
	}

	/**
	 * Returns the size of this chain.
	 */
	public int size()
	{
		return chainedComparators.size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int compare(T value1, T value2)
	{
		for (Comparator<T> comparator : chainedComparators)
		{
			int result = comparator.compare(value1, value2);
			if (result != 0)
				return result;
		}
		return 0;
	}
}
