/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.util;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Utility functions to create {@link HashMap} and {@link TreeMap} from arrays
 * and lists.
 * 
 * @see HashMap
 * @see TreeMap
 * @author Benjamin Billet
 * @version 0.1
 */
public final class Maps
{
	private Maps() { }	
	
	/**
	 * Creates a set of key/value pairs from an array of keys and an array of
	 * values, and puts them into a map.
	 * @param map The destination map.
	 * @param keys The keys.
	 * @param values The values.
	 * @throws IllegalArgumentException If {@code keys.length != values.length}.
	 * @throws NullPointerException If one key or one value is {@code null}, and
	 *             the map does not supports null keys or null values.
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> void putAll(Map<K, V> map, K[] keys, V[] values)
	{
		if(keys.length != values.length)
			throw new IllegalArgumentException("'keys' and 'values' must have the same size");
		
		for(int i = 0; i < keys.length; i++)
		{
			map.put(keys[i], values[i]);
		}
	}
	
	/**
	 * Creates a new {@link HashMap} from an array of keys and an array of
	 * values.
	 * @param keys The keys.
	 * @param values The values.
	 * @return The new map.
	 * @throws IllegalArgumentException If {@code keys.length != values.length}.
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> HashMap<K, V> createHashMap(K[] keys, V[] values)
	{
		return createHashMap(keys, values, keys.length);
	}
	
	/**
	 * Creates a new {@link HashMap} from an array of keys and an array of
	 * values.
	 * @param keys The keys.
	 * @param values The values.
	 * @param initialCapacity The initial capacity of the map.
	 * @return The new map.
	 * @throws IllegalArgumentException If {@code keys.length != values.length}.
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> HashMap<K, V> createHashMap(K[] keys, V[] values, int initialCapacity)
	{		
		HashMap<K, V> map = new HashMap<K, V>(initialCapacity);
		putAll(map, keys, values);
		return map;
	}
	
	/**
	 * Creates a new {@link TreeMap} from an array of keys and an array of
	 * values.
	 * @param keys The keys.
	 * @param values The values.
	 * @return The new map.
	 * @throws IllegalArgumentException If {@code keys.length != values.length}.
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> TreeMap<K, V> createTreeMap(K[] keys, V[] values)
	{
		return createTreeMap(keys, values, (Comparator<K>) null);
	}
	
	/**
	 * Creates a new {@link TreeMap} from an array of keys and an array of
	 * values.
	 * @param keys The keys.
	 * @param values The values.
	 * @param comparator The comparator that will be used to sort the map
	 * @return The new map.
	 * @throws IllegalArgumentException If {@code keys.length != values.length}.
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> TreeMap<K, V> createTreeMap(K[] keys, V[] values, Comparator<K> comparator)
	{		
		TreeMap<K, V> map = new TreeMap<K, V>(comparator);
		putAll(map, keys, values);
		return map;
	}
	
	/**
	 * Creates a set of key/value pairs from a list of keys and a list of
	 * values, and put them into a map.
	 * @param map The destination map.
	 * @param keys The keys.
	 * @param values The values.
	 * @throws IllegalArgumentException If {@code keys.size() != values.size()}.
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> void putAll(Map<K, V> map, List<K> keys, List<V> values)
	{
		if(keys.size() != values.size())
			throw new IllegalArgumentException("'keys' and 'values' must have the same size");
		
		Iterator<K> itKeys = keys.iterator();
		Iterator<V> itValues = values.iterator();
		while(itKeys.hasNext())
		{
			map.put(itKeys.next(), itValues.next());
		}
	}
	
	/**
	 * Creates a new {@link HashMap} from a list of keys and a list of values.
	 * @param keys The keys.
	 * @param values The values.
	 * @return The new map.
	 * @throws IllegalArgumentException If {@code keys.size() != values.size()}.
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> HashMap<K, V> createHashMap(List<K> keys, List<V> values)
	{
		return createHashMap(keys, values, keys.size());
	}
	
	/**
	 * Creates a new {@link HashMap} from a list of keys and a list of values.
	 * @param keys The keys.
	 * @param values The values.
	 * @param initialCapacity The initial capacity of the map.
	 * @return The new map.
	 * @throws IllegalArgumentException If {@code keys.size() != values.size()}.
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> HashMap<K, V> createHashMap(List<K> keys, List<V> values, int initialCapacity)
	{
		HashMap<K, V> map = new HashMap<K, V>(initialCapacity);
		putAll(map, keys, values);
		return map;
	}
	
	/**
	 * Creates a new {@link TreeMap} from a list of keys and a list of values.
	 * @param keys The keys.
	 * @param values The values.
	 * @return The new map.
	 * @throws IllegalArgumentException If {@code keys.size() != values.size()}.
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> TreeMap<K, V> createTreeMap(List<K> keys, List<V> values)
	{
		return createTreeMap(keys, values, (Comparator<K>) null);
	}
	
	/**
	 * Creates a new {@link TreeMap} from a list of keys and a list of values
	 * @param keys The keys.
	 * @param values The values.
	 * @param comparator The comparator that will be used to sort the map.
	 * @return The new map.
	 * @throws IllegalArgumentException If {@code keys.size() != values.size()}.
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> TreeMap<K, V> createTreeMap(List<K> keys, List<V> values, Comparator<K> comparator)
	{
		TreeMap<K, V> map = new TreeMap<K, V>(comparator);
		putAll(map, keys, values);
		return map;
	}
	
	
	
	
	/**
	 * Creates a set of key/value pairs from an array of raw objects, and puts
	 * them into a map.
	 * @param map The destination map.
	 * @param rawObjects The raw objects.
	 * @throws IllegalArgumentException If {@code rawObjects.length is not even}.
	 * @throws NullPointerException If one key or one value is {@code null}, and
	 *             the map does not supports null keys or null values.
	 * @throws ClassCastException If the object types are not compatible with
	 *             the key and value types of the map.
	 */
	@SuppressWarnings("unchecked")
	public static <K, V> void putAllRaw(Map<K, V> map, Object... rawObjects)
	{
		if(rawObjects.length % 2 == 1)
			throw new IllegalArgumentException("'rawObjects' must have an even size");
		
		for(int i = 0; i < rawObjects.length; i += 2)
		{
			map.put((K) rawObjects[i], (V) rawObjects[i + 1]);
		}
	}
	/**
	 * Creates a new {@link HashMap} from an array of raw objects.
	 * @param rawObjects The raw objects.
	 * @return The new map.
	 * @throws IllegalArgumentException If {@code rawObjects.length is not even}.
	 * @throws ClassCastException If the object types are not compatible with
	 *             the key and value types of the map.
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> HashMap<K, V> createHashMapRaw(Object... rawObjects)
	{
		return createHashMapRaw(rawObjects.length, rawObjects);
	}
	
	/**
	 * Creates a new {@link HashMap} from an array of raw objects.
	 * @param initialCapacity The initial capacity of the map.
	 * @param rawObjects The raw objects.
	 * @return The new map.
	 * @throws IllegalArgumentException If {@code rawObjects.length is not even}.
	 * @throws ClassCastException If the object types are not compatible with
	 *             the key and value types of the map.
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> HashMap<K, V> createHashMapRaw(int initialCapacity, Object... rawObjects)
	{
		HashMap<K, V> map = new HashMap<K, V>(initialCapacity);
		putAllRaw(map, rawObjects);
		return map;
	}
	
	/**
	 * Creates a new {@link TreeMap} from an array of raw objects.
	 * @param rawObjects The raw objects.
	 * @return The new map.
	 * @throws IllegalArgumentException If {@code rawObjects.length is not even}.
	 * @throws ClassCastException If the object types are not compatible with
	 *             the key and value types of the map.
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> TreeMap<K, V> createTreeMapRaw(Object... rawObjects)
	{
		return createTreeMapRaw((Comparator<K>) null, rawObjects);
	}
	
	/**
	 * Creates a new {@link TreeMap} from an array of raw objects.
	 * @param comparator The comparator that will be used to sort the map.
	 * @param rawObjects The raw objects.
	 * @return The new map.
	 * @throws IllegalArgumentException If {@code rawObjects.length is not even}.
	 * @throws ClassCastException If the object types are not compatible with
	 *             the key and value types of the map.
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> TreeMap<K, V> createTreeMapRaw(Comparator<K> comparator, Object... rawObjects)
	{
		TreeMap<K, V> map = new TreeMap<K, V>(comparator);
		putAllRaw(map, rawObjects);
		return map;
	}
	
	/**
	 * Creates a new {@link HashMap} that contains a single key/value pair.
	 * @param key The key.
	 * @param value The value.
	 * @return The new map.
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> HashMap<K, V> createHashMap(K key, V value)
	{
		HashMap<K, V> map = new HashMap<K, V>();
		map.put(key, value);
		return map;
	}
	
	/**
	 * Creates a new {@link HashMap} that contains a single key/value pair.
	 * @param key The key.
	 * @param value The value.
	 * @param initialCapacity The initial capacity of the map.
	 * @return The new map.
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> HashMap<K, V> createHashMap(K key, V value, int initialCapacity)
	{
		HashMap<K, V> map = new HashMap<K, V>(initialCapacity);
		map.put(key, value);
		return map;
	}
	
	/**
	 * Creates a new {@link TreeMap} that contains a single key/value pair.
	 * @param key The key.
	 * @param value The value.
	 * @return The new map.
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> TreeMap<K, V> createTreeMap(K key, V value)
	{
		TreeMap<K, V> map = new TreeMap<K, V>();
		map.put(key, value);
		return map;
	}
	
	/**
	 * Creates a new {@link TreeMap} that contains a single key/value pair.
	 * @param key The key.
	 * @param value The value.
	 * @param comparator The comparator that will be used to sort the map.
	 * @return The new map.
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> TreeMap<K, V> createTreeMap(K key, V value, Comparator<K> comparator)
	{
		TreeMap<K, V> map = new TreeMap<K, V>(comparator);
		map.put(key, value);
		return map;
	}
	
	/**
	 * Looks for a value into a map. If the value is not found (i.e.,
	 * {@link Map#get(Object)} returns {@code null}), a default value is
	 * returned.
	 * @param map The map.
	 * @param key The key.
	 * @param defaultValue The default value.
	 * @param comparator The comparator that will be used to sort the map.
	 * @return The value mapped to {@code key}, or {@code defaultValue} if there
	 *         is no mapping for {@code key}.
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> V getDefault(Map<K, V> map, K key, V defaultValue)
	{
		V value = map.get(key);
		if(value != null)
			return value;
		else
			return defaultValue;
	}
}
