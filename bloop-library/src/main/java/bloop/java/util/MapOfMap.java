/*  
 * The Bloop Library.
 * Copyright (c) 2012 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.util;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * A {@link Map} of {@link Map}, which automatically creates sub-maps when
 * elements are added. This class is abstract; the implementation of the factory
 * method {@link MapOfMap#newSubMap()} is required to instantiate sub-maps. <br>
 * The static methods {@link MapOfMap#hashMapOfHashMap()} or
 * {@link MapOfMap#treeMapOfTreeMap()} can be called for creating new
 * {@link MapOfMap} instances based on {@link HashMap} or {@link TreeMap}.
 * 
 * @author Benjamin Billet
 * @version 0.1
 * 
 * @param <K1> The type of keys maintained by the main map.
 * @param <K2> The type of keys maintained by the sub-maps.
 * @param <V> The type of mapped values.
 */
public abstract class MapOfMap<K1, K2, V> implements Map<K1, Map<K2, V>>
{
	private Map<K1, Map<K2, V>> internal;	

	/**
	 * Creates a new {@link MapOfMap}, using the provided map to store the
	 * sub-maps.
	 * @param mainMap The map where the sub-maps will be stored.
	 */
	public MapOfMap(Map<K1, Map<K2, V>> mainMap)
	{
		this.internal = mainMap;
	}
	
	/**
	 * Creates a new sub-map.
	 */
	protected abstract Map<K2, V> newSubMap();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size()
	{
		return internal.size();
	}

	/**
	 * Similar to {@link MapOfMap#size()}, but for a sub-map.
	 * @param key The key of the sub-map.
	 * @return The size of the sub-map.
	 */
	public int size(K1 key)
	{
		Map<K2, V> map = internal.get(key);
		if(map != null)
			return map.size();
		else
			return 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEmpty()
	{
		return internal.isEmpty();
	}
	
	/**
	 * Similar to {@link MapOfMap#isEmpty()}, but for a sub-map.
	 * @param key The key of the sub-map.
	 */
	public boolean isEmpty(K1 key)
	{
		return size(key) == 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsKey(Object key)
	{
		return internal.containsKey(key);
	}
	
	/**
	 * Similar to {@link MapOfMap#containsKey(Object)}, but for a sub-map.
	 * @param key1 The key of the sub-map.
	 * @param key2 The key to check into the sub-map.
	 */
	public boolean containsKey(K1 key1, K2 key2)
	{
		Map<K2, V> map = internal.get(key1);
		if(map != null)
			return map.containsKey(key2);
		else
			return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsValue(Object value)
	{
		return internal.containsValue(value);
	}
	
	/**
	 * Similar to {@link MapOfMap#containsValue(Object)}, but for a sub-map.
	 * @param key The key of the sub-map.
	 * @param value The value to check into the sub-map.
	 */
	public boolean containsValue(K1 key, V value)
	{
		Map<K2, V> map = internal.get(key);
		if(map != null)
			return map.containsValue(value);
		else
			return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<K2, V> get(Object key)
	{
		return internal.get(key);
	}
	
	/**
	 * Similar to {@link MapOfMap#get(Object)}, but for a sub-map.
	 * @param key1 The key of the sub-map.
	 * @param key2 The key to look for in the sub-map.
	 */
	public V get(K1 key1, K2 key2)
	{
		Map<K2, V> map = internal.get(key1);
		if(map != null)
			return map.get(key2);
		else
			return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<K2, V> put(K1 key, Map<K2, V> value)
	{
		return internal.put(key, value);
	}
	
	/**
	 * Similar to {@link MapOfMap#put(Object, Map)}, but for a sub-map.
	 * @param key1 The key of the sub-map.
	 * @param key2 The key to insert into the sub-map.
	 * @param value The value to insert into the sub-map.
	 */
	public V put(K1 key1, K2 key2, V value)
	{
		Map<K2, V> map = internal.get(key1);
		if(map == null)
		{
			map = newSubMap();
			internal.put(key1, map);
		}
			
		return map.put(key2, value);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<K2, V> remove(Object key)
	{
		return internal.remove(key);
	}
	
	/**
	 * Similar to {@link MapOfMap#remove(Object)}, but for a sub-map.
	 * @param key1 The key of the sub-map.
	 * @param key2 The key to remove from the sub-map.
	 */
	public V remove(K1 key1, K2 key2)
	{
		Map<K2, V> map = internal.get(key1);
		if(map != null)
			return map.remove(key2);
		else
			return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void putAll(Map<? extends K1, ? extends Map<K2, V>> map)
	{
		internal.putAll(map);
	}
	
	/**
	 * Similar to {@link MapOfMap#putAll(Map)}, but for a sub-map.
	 * @param key The key of the sub-map.
	 * @param map The content to insert in the sub-map.
	 */
	public void putAll(K1 key, Map<? extends K2, ? extends V> map)
	{
		Map<K2, V> subMap = internal.get(key);
		if(subMap != null)
			subMap.putAll(map);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear()
	{
		internal.clear();
	}
	
	/**
	 * Similar to {@link MapOfMap#clear()}, but for a sub-map.
	 * @param key The key of the sub-map.
	 */
	public void clear(K1 key)
	{
		Map<K2, V> map = internal.get(key);
		if(map != null)
			map.clear();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<K1> keySet()
	{
		return internal.keySet();
	}
	
	/**
	 * Similar to {@link MapOfMap#keySet()}, but for a sub-map.
	 * @param key The key of the sub-map.
	 */
	public Set<K2> keySet(K1 key)
	{
		Map<K2, V> map = internal.get(key);
		if(map != null)
			return map.keySet();
		else
			return Collections.emptySet();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<Map<K2, V>> values()
	{
		return internal.values();
	}
	
	/**
	 * Similar to {@link MapOfMap#values()}, but for a sub-map.
	 * @param key The key of the sub-map.
	 */
	public Collection<V> values(K1 key)
	{
		Map<K2, V> map = internal.get(key);
		if(map != null)
			return map.values();
		else
			return Collections.emptyList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<Entry<K1, Map<K2, V>>> entrySet()
	{
		return internal.entrySet();
	}
	
	/**
	 * Similar to {@link MapOfMap#entrySet()}, but for a sub-map.
	 * @param key The key of the sub-map.
	 */
	public Set<Entry<K2, V>> entrySet(K1 key)
	{
		Map<K2, V> map = internal.get(key);
		if(map != null)
			return map.entrySet();
		else
			return Collections.emptySet();
	}

	
	/**
	 * Creates a new {@link MapOfMap}, using {@link HashMap}.
	 */
	public static <K1, K2, V> MapOfMap<K1, K2, V> hashMapOfHashMap()
	{
		return new MapOfHashMap<K1, K2, V>(new HashMap<K1, Map<K2, V>>());
	}
	
	/**
	 * Creates a new {@link MapOfMap}, using {@link HashMap}.
	 * @param initialCapacity The initial capacity, which will be applied to the
	 *            main map and the sub-maps.
	 * @throws IllegalArgumentException If the initial capacity is negative.
	 */
	public static <K1, K2, V> MapOfMap<K1, K2, V> hashMapOfHashMap(int initialCapacity)
	{
		return new MapOfHashMap<K1, K2, V>(new HashMap<K1, Map<K2, V>>(initialCapacity), initialCapacity);
	}
	
	/**
	 * Creates a new {@link MapOfMap}, using {@link HashMap}.
	 * @param initialCapacity The initial capacity, which will be applied to the
	 *            main map and the sub-maps.
	 * @param loadFactor The load factor, which will be applied to the main map
	 *            and the sub-maps.
	 * @throws IllegalArgumentException If the initial capacity is negative or
	 *             the load factor is non-positive.
	 */
	public static <K1, K2, V> MapOfMap<K1, K2, V> hashMapOfHashMap(int initialCapacity, float loadFactor)
	{
		return new MapOfHashMap<K1, K2, V>(new HashMap<K1, Map<K2, V>>(initialCapacity, loadFactor), initialCapacity, loadFactor);
	}
	
	/**
	 * Creates a new {@link MapOfMap}, using {@link TreeMap}.
	 */
	public static <K1, K2, V> MapOfMap<K1, K2, V> treeMapOfTreeMap()
	{
		return new MapOfTreeMap<>(new TreeMap<K1, Map<K2, V>>());
	}
	
	/**
	 * Creates a new {@link MapOfMap}, using {@link TreeMap}.
	 * @param mainMapComparator The comparator that will be used to order the
	 *            main map. If {@code null}, the {@linkplain Comparable natural
	 *            ordering} of the keys will be used.
	 * @param subMapComparator The comparator that will be used to order the
	 *            sub-maps. If {@code null}, the {@linkplain Comparable natural
	 *            ordering} of the keys will be used.
	 */
	public static <K1, K2, V> MapOfMap<K1, K2, V> treeMapOfTreeMap(Comparator<K1> mainMapComparator, Comparator<K2> subMapComparator)
	{
		return new MapOfTreeMap<>(new TreeMap<K1, Map<K2, V>>(mainMapComparator), subMapComparator);
	}
	
	private static class MapOfTreeMap<K1, K2, V> extends MapOfMap<K1, K2, V>
	{
		private Comparator<K2> comparator;
		
		public MapOfTreeMap(Map<K1, Map<K2, V>> mainMap)
		{
			super(mainMap);
		}
		
		public MapOfTreeMap(Map<K1, Map<K2, V>> mainMap, Comparator<K2> comparator)
		{
			super(mainMap);
			this.comparator = comparator;
		}

		@Override
		protected Map<K2, V> newSubMap()
		{
			return new TreeMap<>(comparator);
		}
	}
	
	private static class MapOfHashMap<K1, K2, V> extends MapOfMap<K1, K2, V>
	{
		private int initialCapacity = -1;
		private float loadFactor = -1;
		
		public MapOfHashMap(Map<K1, Map<K2, V>> mainMap)
		{
			super(mainMap);
		}
		
		public MapOfHashMap(Map<K1, Map<K2, V>> mainMap, int initialCapacity)
		{
			this(mainMap);
			this.initialCapacity = initialCapacity;
		}
		
		public MapOfHashMap(Map<K1, Map<K2, V>> mainMap, int initialCapacity, float loadFactor)
		{
			this(mainMap, initialCapacity);
			this.loadFactor = loadFactor;
		}

		@Override
		protected Map<K2, V> newSubMap()
		{
			if(loadFactor != -1f)
				return new HashMap<>(initialCapacity, loadFactor);
			else if(initialCapacity != -1)
				return new HashMap<>(initialCapacity);
			else
				return new HashMap<>();
		}
	}
}
