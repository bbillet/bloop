/*  
 * The Bloop Library.
 * Copyright (c) 2014 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.util;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * A dual map is a one-to-one {@link Map} where each key and each value are
 * unique. In practice a dual map embeds two {@link Map} objects (one for
 * mapping the keys to the values and one for mapping the values to the keys),
 * with the benefit of an identical complexity for keys and values operations
 * (depending on The type of map encapsulated into the dual map).<br>
 * This class presents a set of utility functions for building {@link DualMap}
 * instances, e.g.: {@code Map<String, Something> map = DualMaps.dualHashMap()}
 * 
 * @see DualMap
 * @see Map
 * @author Benjamin Billet
 * @version 0.1
 */
public final class DualMaps
{
	private DualMaps() { }
	
	/**
	 * Creates a new {@code DualMap} using two {@link HashMap} objects.
	 * @return The new dual map.
	 * @see HashMap
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> DualMap<K, V> dualHashMap()
	{
		return new DualMap<K, V>(new HashMap<K, V>(), new HashMap<V, K>());
	}

	/**
	 * Creates a new {@code DualMap} using two {@link HashMap} objects.
	 * @param initialCapacity The initial capacity of the internal hashmaps.
	 * @return The new dual map.
	 * @see HashMap
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> DualMap<K, V> dualHashMap(int initialCapacity)
	{
		return new DualMap<K, V>(
				new HashMap<K, V>(initialCapacity),
				new HashMap<V, K>(initialCapacity));
	}
	

	/**
	 * Creates a new {@code DualMap} using two {@link HashMap} objects.
	 * @param initialCapacity The initial capacity of the internal hashmaps.
	 * @param loadFactor The load factor of the internal hashmaps.
	 * @return The new dual map.
	 * @see HashMap
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> DualMap<K, V> dualHashMap(int initialCapacity, float loadFactor)
	{
		return new DualMap<K, V>(
				new HashMap<K, V>(initialCapacity, loadFactor), 
				new HashMap<V, K>(initialCapacity, loadFactor));
	}
	
	/**
	 * Creates a new {@code DualMap} using two {@link TreeMap} objects.
	 * @return The new dual map.
	 * @see TreeMap
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> DualMap<K, V> dualTreeMap()
	{
		return new DualMap<K, V>(new TreeMap<K, V>(), new TreeMap<V, K>());
	}
	
	/**
	 * Creates a new {@code DualMap} using two {@link TreeMap} objects.
	 * @param comparator A comparator to sort the keys of the {@code DualMap}.
	 * @return The new dual map.
	 * @see TreeMap
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> DualMap<K, V> dualTreeMap(Comparator<? super K> comparator)
	{
		return new DualMap<K, V>(new TreeMap<K, V>(comparator), new TreeMap<V, K>());
	}
	
	/**
	 * Creates a new {@code DualMap} using two {@link LinkedHashMap} objects.
	 * @return The new dual map.
	 * @see LinkedHashMap
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> DualMap<K, V> dualLinkedHashMap()
	{
		return new DualMap<K, V>(new LinkedHashMap<K, V>(), new LinkedHashMap<V, K>());
	}
	
	/**
	 * Creates a new {@code DualMap} using two {@link LinkedHashMap} objects.
	 * @param initialCapacity The initial capacity of the internal hashmaps.
	 * @return The new dual map.
	 * @see LinkedHashMap
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> DualMap<K, V> dualLinkedHashMap(int initialCapacity)
	{
		return new DualMap<K, V>(
				new LinkedHashMap<K, V>(initialCapacity),
				new LinkedHashMap<V, K>(initialCapacity));
	}
	
	/**
	 * Creates a new {@code DualMap} using two {@link LinkedHashMap} objects.
	 * @param initialCapacity The initial capacity of the internal hashmaps.
	 * @param loadFactor The load factor of the internal hashmaps.
	 * @return The new dual map.
	 * @see LinkedHashMap
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> DualMap<K, V> dualLinkedHashMap(int initialCapacity, float loadFactor)
	{
		return new DualMap<K, V>(
				new LinkedHashMap<K, V>(initialCapacity, loadFactor), 
				new LinkedHashMap<V, K>(initialCapacity, loadFactor));
	}
	
	/**
	 * Creates a new {@code DualMap} using two {@link IdentityHashMap} objects.
	 * @return The new dual map.
	 * @see IdentityHashMap
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> DualMap<K, V> dualIdentityHashMap()
	{
		return new DualMap<K, V>(new IdentityHashMap<K, V>(), new IdentityHashMap<V, K>());
	}
	
	/**
	 * Creates a new {@code DualMap} using two {@link IdentityHashMap} objects.
	 * @param expectedMaxSize The expected maximum size of the internal hashmaps.
	 * @return The new dual map.
	 * @see IdentityHashMap
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> DualMap<K, V> dualIdentityHashMap(int expectedMaxSize)
	{
		return new DualMap<K, V>(
				new IdentityHashMap<K, V>(expectedMaxSize),
				new IdentityHashMap<V, K>(expectedMaxSize));
	}
	
	/**
	 * Creates a new {@code DualMap} that encapsulates two maps. <br>
	 * The provided maps SHOULD be empty. However, if the map are not empty,
	 * {@code valueKeyMap} is cleared and each {@literal <Key, Value>} pair of
	 * {@code keyValueMap} is reversed and copied into {@code valueKeyMap} as
	 * {@literal <Value, Key>}.
	 * @param keyValueMap The {@literal Key->Value} map
	 * @param valueKeyMap The {@literal Value->Key} map
	 * @return The new dual map.
	 * @param <K> The type of the keys.
	 * @param <V> The type of the values.
	 */
	public static <K, V> DualMap<K, V> dualMap(Map<K, V> keyValueMap, Map<V, K> valueKeyMap)
	{
		DualMap<K, V> map = new DualMap<>(keyValueMap, valueKeyMap);
		valueKeyMap.clear();
		if (keyValueMap.isEmpty() == false)
			map.putAll(keyValueMap);
		return map;
	}
	
	/**
	 * The {@code DualMap} container that encapsulates two maps.<br>
	 * Use the building functions provided by {@link DualMaps} to instantiate
	 * new dual maps. E.g.:
	 * {@code Map<String, Something> map = DualMaps.dualHashMap()}
	 * 
	 * @param <K> The type of keys maintained by this map.
	 * @param <V> The type of mapped values.
	 * @see Map
	 */
	public static class DualMap<K, V> implements Map<K, V>
	{
		private final Map<K, V> map1;
		private final Map<V, K> map2;
		
		private Set<K> keySet;
		private Set<V> valueSet;
		private Set<Map.Entry<K, V>> entrySet;
		private Set<Map.Entry<V, K>> reverseEntrySet;

		/**
		 * Creates a new {@code DualMap} based on two {@link HashMap}.
		 */
		public DualMap()
		{
			this.map1 = new HashMap<K, V>();
			this.map2 = new HashMap<V, K>();
		}
		
		private DualMap(Map<K, V> map1, Map<V, K> map2)
		{
			this.map1 = map1;
			this.map2 = map2;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int size()
		{
			return map1.size();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean isEmpty()
		{
			return map1.isEmpty();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean containsKey(Object key)
		{
			return map1.containsKey(key);
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean containsValue(Object value)
		{
			return map2.containsKey(value);
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public V get(Object key)
		{
			return map1.get(key);
		}

		/**
		 * Returns the value to which the specified key is mapped (there can be
		 * at most one such mapping).
		 * @param value The value whose associated key is to be returned.
		 * @return The key to which the specified value is mapped.
		 * @throws ClassCastException If the value is of an inappropriate type
		 *             for this map.
		 * @throws NullPointerException If the specified value is {@code null}
		 *             and this map does not permit {@code null} values.
		 */
		public K reverseGet(Object value)
		{
			return map2.get(value);
		}

		/**
		 * Associates a key and a value. If the key is involved in an existing
		 * association, the old association is removed. If the value is involved
		 * in an existing association, the old association is removed.
		 * @throws NullPointerException If key or value are {@code null}.
		 */
		@Override
		public V put(K key, V value)
		{
			if(key == null || value == null)
				throw new NullPointerException();
			
			if(map1.containsKey(key))
				remove(key);
			if(map2.containsKey(value))
				reverseRemove(value);
			
			map2.put(value, key);
			return map1.put(key, value);
		}

		@Override
		public V remove(Object key)
		{
			V value = map1.remove(key);
			if(value != null)
				map2.remove(value);
			return value;
		}

		/**
		 * Removes the mapping for a value from this map if it is present.
		 * Returns the key to which this map previously associated the key, or
		 * {@code null} if the map contained no mapping for the value. The map
		 * will not contain a mapping for the specified value once the call
		 * returns.
		 * @param value Value whose mapping is to be removed from the map
		 * @return The previous key associated with {@code value}, or
		 *         {@code null} if there was no mapping for {@code value}.
		 * @throws UnsupportedOperationException If the remove operation is not
		 *             supported by this map.
		 * @throws ClassCastException If the value is of an inappropriate type
		 *             for this map.
		 * @throws NullPointerException If the specified value is {@code null}
		 *             and this map does not permit {@code null} values.
		 */
		public K reverseRemove(Object value)
		{
			K key = map2.remove(value);
			if(key != null)
				map1.remove(key);
			return key;
		}

		@Override
		public void putAll(Map<? extends K, ? extends V> map)
		{
			for (Map.Entry<? extends K, ? extends V> entry : map.entrySet())
			{
				put(entry.getKey(), entry.getValue());
			}
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void clear()
		{
			map1.clear();
			map2.clear();
		}

		/**
		 * Returns a {@link Set} view of the keys contained in this map. Note
		 * that the set is unmodifiable.
		 * @return A set view of the keys contained in this map.
		 * @see Map#keySet()
		 */
		@Override
		public Set<K> keySet()
		{
			if(keySet == null)
				keySet = Collections.unmodifiableSet(map1.keySet());
			return keySet;
		}

		/**
		 * Returns a {@link Set} view of the values contained in this map. Note
		 * that the set is unmodifiable.
		 * @return A set view of the keys contained in this map.
		 */
		public Set<V> valueSet()
		{
			if(valueSet == null)
				valueSet = Collections.unmodifiableSet(map2.keySet());
	        return valueSet;
		}

		/**
		 * Returns a {@link Collection} view of the values contained in this
		 * map. Note that the collection is unmodifiable.
		 * @return A collection view of the values contained in this map.
		 * @see DualMap#valueSet()
		 */
		@Override
		public Collection<V> values()
		{
			return valueSet();
		}

		/**
		 * Returns a {@link Set} view of the {@literal Key->Value} mappings
		 * contained in this map. Note that the entry set is unmodifiable.
		 * @return A set view of the mappings contained in this map.
		 */
		@Override
		public Set<Map.Entry<K, V>> entrySet()
		{
			if(entrySet == null)
				entrySet = Collections.unmodifiableMap(map1).entrySet();
			return entrySet;
		}

		/**
		 * Returns a {@link Set} view of the {@literal Value->Key} mappings
		 * contained in this map. Note that the entry set is unmodifiable.
		 * @return A set view of the mappings contained in this map.
		 */
		public Set<Map.Entry<V, K>> reverseEntrySet()
		{
			if(reverseEntrySet == null)
				reverseEntrySet = Collections.unmodifiableMap(map2).entrySet();
			return reverseEntrySet;
		}

		/**
		 * Returns a DualMap where the {@literal Key->Value} mappings are
		 * reversed. Note that the new dual map is constructed by reference, so
		 * changes to the reverse map are reflected in this map, and vice-versa.
		 * @return A reverse dual map.
		 */
		public DualMap<V, K> reverseMap()
		{
			return new DualMap<V, K>(map2, map1);
		}
		
		@Override
	    public String toString() 
		{
	        return map1.toString();
		}
	}
}
