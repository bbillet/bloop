/*  
 * The Bloop Library.
 * Copyright (c) 2012 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.util;

/**
 * Various helpers for string manipulation (concatenate, compare, case
 * manipulation, etc.).
 * 
 * @author Benjamin Billet
 * @version 0.1
 */
public final class Strings
{
	private Strings() { }
	
	/**
	 * Concatenates an array of string. Null strings are converted into "null".
	 * @param strings The array of string (can be of length 0).
	 * @return The concatenated string.
	 */
	public static String concat(String... strings)
	{
		StringBuilder sb = new StringBuilder(length(strings));
		for(int i = 0; i < strings.length; i++)
		{
			sb.append(strings[i]);
		}
		return sb.toString();
	}
	
	/**
	 * Concatenates an array of string with a delimiter. The concatenated string
	 * has the following form: s<sub>0</sub> + delimiter + ... + delimiter +
	 * s<sub>n-1</sub> where s<sub>i</sub> is the i-th element of
	 * {@code strings}. If there is only one string s<sub>0</sub> in
	 * {@code strings}, the result is equals to s<sub>0</sub> without any
	 * delimiter. Null strings are converted into "null".
	 * @param delimiter The delimiter.
	 * @param strings The array of string (can be of length 0).
	 * @return The concatenated string.
	 */
	public static String delimit(String delimiter, String... strings)
	{
		int length = length(strings);
		if(strings.length > 0)
			length += delimiter.length() * (strings.length - 1);
		
		StringBuilder sb = new StringBuilder(length);
		if(strings.length > 1)
		{
			sb.append(strings[0]);
			for(int i = 1; i < strings.length; i++)
			{
				sb.append(delimiter);
				sb.append(strings[i]);
			}
		}
		else if(strings.length == 1)
			sb.append(strings[0]);

		return sb.toString();
	}
	
	/**
	 * Appends an array of string to an existing {@link StringBuilder}.
	 * @param sb The string builder.
	 * @param strings The strings.
	 */
	public static void append(StringBuilder sb, String... strings)
	{
		sb.ensureCapacity(sb.length() + length(strings));
		for(String s : strings)
		{
			sb.append(s);
		}
	}
	
	/**
	 * Counts the total length of an array of string. Null strings are counted
	 * like "null" (4 characters).
	 * @param strings The strings.
	 * @return The total length.
	 */
	public static int length(String... strings)
	{
		int length = 0;
		for (String string : strings)
		{
			if (string == null)
				length += 4;
			else
				length += string.length();
		}
		return length;
	}
	
	/**
	 * Checks if a string is null or empty ({@code length() == 0}).
	 * @param string The string.
	 * @return {@code true} if the string is null or empty, {@code false} otherwise.
	 */
	public static boolean isNullOrEmpty(String string)
	{
		return (string == null || string.length() == 0);
	}

	/**
	 * Checks if a string is null or filled with whitespaces (' ', \\t, \\r, \\n).
	 * @param string The string.
	 * @return {@code true} if the string is null or filled with whitespaces, {@code false} otherwise.
	 */
	public static boolean isNullOrWhitespace(String string)
	{
		return (string == null || string.trim().length() == 0);
	}

	/**
	 * Checks if a string is not null and not empty ({@code length() > 0}).
	 * @param string The string.
	 * @return {@code true} if the string is not null and not empty, {@code false} otherwise.
	 */
	public static boolean isNotNullOrEmpty(String string)
	{
		return (string != null && string.length() > 0);
	}

	/**
	 * Checks if a string is not null and not filled with whitespaces (' ', \\t,
	 * \\r, \\n).
	 * @param string The string.
	 * @return {@code true} if the string is not null and not filled with
	 *         whitespaces, {@code false} otherwise.
	 */
	public static boolean isNotNullOrWhitespace(String string)
	{
		return (string != null && string.trim().length() > 0);
	}

	/**
	 * Checks if a string is filled with blanks (' ', \\t, \\r, \\n).
	 * @param string The string.
	 * @return {@code true} if the string is filled with blanks, {@code false}
	 *         otherwise.
	 */
	public static boolean isBlank(String string)
	{
		if(string == null)
			return false;
		return isNullOrWhitespace(string);
	}
	
	/**
	 * Checks if a string is not filled with blanks (' ', \\t, \\r, \\n).
	 * @param string The string.
	 * @return {@code true} if the string is not filled with blanks,
	 *         {@code false} otherwise.
	 */
	public static boolean isNotBlank(String string)
	{
		if(string == null)
			return true;
		return isNotNullOrWhitespace(string);
	}
	
	/**
	 * Checks if a character is contained in an array of character.
	 * @param c The character to check.
	 * @param chars The array of characters.
	 * @return {@code true} if the character is contained in the array,
	 *         {@code false} otherwise.
	 */
	public static boolean matchChar(char c, char... chars)
	{
		for(int i = 0; i < chars.length; i++)
		{
			if(c == chars[i])
				return true;
		}
		return false;
	}
	
	private static String trim(String string, boolean left, boolean right, char... trimChars)
	{
		if(isNullOrEmpty(string) || trimChars == null || trimChars.length == 0)
			return string;
		
		int max = string.length() - 1;
		int start = 0;
		int end = max;
		
		if(left)
		{
			for(start = 0; start < string.length(); start++)
			{
				if(matchChar(string.charAt(start), trimChars) == false)
					break;
			}
			if(start == string.length())
				return "";
		}
		
		if(right)
		{
			for(end = max ; end >= 0; end--)
			{
				if(matchChar(string.charAt(end), trimChars) == false)
					break;
			}
			if(end == 0)
				return "";
		}
		
		if(start == 0 && end == max)
			return string;
		else
			return string.substring(start, end + 1);
	}

	/**
	 * Returns a string whose value is this string, with any leading specified
	 * characters removed.
	 * @param string The string to clean.
	 * @param trimChars An array of chars to be removed from the beginning of
	 *            the string.
	 * @return A string whose value is this string, with any leading
	 *         {@code 'trimChars'} characters removed.
	 */
	public static String leftTrim(String string, char... trimChars)
	{
		return trim(string, true, false, trimChars);
	}
	
	/**
	 * Returns a string whose value is this string, with any trailing specified
	 * characters removed.
	 * @param string The string to clean.
	 * @param trimChars An array of chars to be removed from the end of the
	 *            string.
	 * @return A string whose value is this string, with any trailing
	 *         {@code 'trimChars'} characters removed.
	 */
	public static String rightTrim(String string, char... trimChars)
	{
		return trim(string, false, true, trimChars);
	}
	
	 /**
	 * Returns a string whose value is this string, with any leading and
	 * trailing specified characters removed.
	 * @param string The string to clean.
	 * @param trimChars An array of chars to be removed from the beginning and
	 *            the end of the string.
	 * @return A string whose value is this string, with any leading and
	 *         trailing {@code 'trimChars'} characters removed.
	 */
	public static String trim(String string, char... trimChars)
	{
		return trim(string, true, true, trimChars);
	}

	/**
	 * Checks if a string starts with the specified prefix, ignoring case.
	 * @param string The string to check.
	 * @param prefix The prefix.
	 * @return {@code true} if {@code prefix} is a prefix of the string;
	 *         {@code false} otherwise.
	 * @see String#startsWith(String)
	 * @see String#regionMatches(int, String, int, int)
	 */
	public static boolean startsWithIgnoreCase(String string, String prefix)
	{
		if(prefix == null || string == null) 
			return false;
		
		if(prefix.length() > string.length())
			return false;
		
		return string.regionMatches(true, 0, prefix, 0, prefix.length());
	}
	
	/**
	 * Checks if a string ends with the specified suffix, ignoring case.
	 * @param string The string to check.
	 * @param suffix The suffix.
	 * @return {@code true} if {@code suffix} is a suffix of the string;
	 *         {@code false} otherwise.
	 * @see String#endsWith(String)
	 * @see String#regionMatches(int, String, int, int)
	 */
	public static boolean endsWithIgnoreCase(String string, String suffix)
	{
		if(suffix == null || string == null) 
			return false;
		
		int offset = string.length() - suffix.length();
		if(offset < 0)
			return false;
		
		return string.regionMatches(true, offset, suffix, 0, suffix.length());
	}
	
	/**
	 * Adds {@code padChar} at the beginning of a string until its size reaches
	 * the {@code expectedSize}.
	 * @param string The string.
	 * @param padChar The padding char.
	 * @param expectedSize The expected size.
	 * @return The padded string, or the original string if its size is already
	 *         greater or equals to {@code expectedSize}.
	 */
	public static String leftPad(String string, char padChar, int expectedSize)
	{
		if(string.length() >= expectedSize)
			return string;
		
		char[] chars = new char[expectedSize];
		int current = expectedSize - string.length();
		for(int i = 0; i < current; i++)
		{
			chars[i] = padChar;
		}
		
		string.getChars(0, string.length(), chars, current);
		return new String(chars);
	}
	
	/**
	 * Adds {@code padChar} at the end of a string until its size reaches the
	 * {@code expectedSize}.
	 * @param string The string.
	 * @param padChar The padding char.
	 * @param expectedSize The expected size.
	 * @return The padded string, or the original string if its size is already
	 *         greater or equals to {@code expectedSize}.
	 */
	public static String rightPad(String string, char padChar, int expectedSize)
	{
		if(string.length() >= expectedSize)
			return string;
		
		char[] chars = new char[expectedSize];
		string.getChars(0, string.length(), chars, 0);

		for(int i = string.length(); i < expectedSize; i++)
		{
			chars[i] = padChar;
		}
		return new String(chars);
	}
	
	/**
	 * Converts the first character of a string to upper case.
	 * @param string The string to convert.
	 * @param lower if {@code true}, the remaining part of the string is
	 *            converted to lower case.
	 * @return The capitalized string
	 */
	public static String firstUpper(String string, boolean lower)
	{
		if(string.length() == 0)
			return string;
		else if(string.length() == 1)
			return Character.toString(Character.toUpperCase(string.charAt(0)));
		
		if(lower)
			string = string.toLowerCase();
		
		char[] chars = new char[string.length()];
		chars[0] = Character.toUpperCase(string.charAt(0));
		
		string.getChars(1, string.length(), chars, 1);
		return new String(chars);
	}
	
	/**
	 * Converts the first character of a string to upper case. 
	 * This is an alias for 'firstUpper(string, true)'.
	 * @param string The string to convert.
	 * @return The capitalized string
	 * @see Strings#firstUpper(String, boolean)
	 */
	public static String capitalize(String string)
	{
		return firstUpper(string, true);
	}
	
	/**
	 * Converts an object to a {@link String}, by calling
	 * {@link String#toString()} or by returning a string containing
	 * {@code "null"} if the object is {@code null}.
	 * @param o The object.
	 * @return The string returned by {@link String#toString()} or
	 *         {@code "null"}.
	 */
	public static String toString(Object o)
	{
		if(o != null)
			return o.toString();
		else
			return "null";
	}
	
	/**
	 * Counts the number of times a given string {@code 'toCount'} appears
	 * inside another string {@code 'string'}.
	 * @param string The main string.
	 * @param toCount The string to find.
	 * @return The number of occurences.
	 */
	public static int countOccurences(String string, String toCount) 
	{
		if(string == null || string.length() == 0 || toCount == null || toCount.length() == 0)
			return 0;

		int count = 0;
		int index = 0;
		while ((index = string.indexOf(toCount, index)) != -1)
		{
			count++;
			index += toCount.length();
		}
		return count;
	}
	
	/**
	 * Looks for a string between two other strings.<br>
	 * <pre>{@code
	 * stringBetween("abcdef", "a", "f");	// returns "bcde"
	 * stringBetween("abcdef", "a", null);	// returns "bcdef"
	 * stringBetween("abcdef", "a", "");	// returns "bcdef"
	 * stringBetween("abcdef", "", "f");	// returns "abcde"
	 * stringBetween("abcdef", null, "f");	// returns "abcde"
	 * stringBetween("abcdef", "a", "x");	// returns null
	 * stringBetween("abcdef", "", "x");	// returns null
	 * }</pre>
	 * @param string The main string.
	 * @param before The string before.
	 * @param after The string after.
	 * @return The string between {@code 'before'} and {@code 'after'}, or
	 * 			{@code null}, if before or after were not found.
	 */
	public static String stringBetween(String string, String before, String after)
	{
		int start = 0;
		if(isNullOrEmpty(before) == false)
		{
			start = string.indexOf(before);
			if(start != -1)
				start = start + before.length();
			else
				return null;
		}
		
		int end = string.length();
		if(isNullOrEmpty(after) == false)
		{
			end = string.lastIndexOf(after);
			if(end == -1)
				return null;
		}
		
		return string.substring(start, end);
	}
}
