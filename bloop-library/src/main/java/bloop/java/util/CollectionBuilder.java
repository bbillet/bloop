/*  
 * The Bloop Library.
 * Copyright (c) 2014 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Builder pattern for building {@link Collection} objects.
 * <pre>{@code
 * Set<String> set = new CollectionBuilder<>(new HashSet<String>())
 * 		.value("xxx")
 * 		.values("yyy", "zzz")
 * 		.values(anotherStringCollection)
 * 		.build();
 * }</pre>
 *
 * @author Benjamin Billet
 * @version 0.1
 * 
 * @param <E> The type of elements in this collection
 */
public class CollectionBuilder<E>
{
	private Collection<E> collection;

	/**
	 * Creates a new {@link CollectionBuilder} for a new {@link ArrayList}.
	 */
	public CollectionBuilder()
	{
		this.collection = new ArrayList<>();
	}
	
	/**
	 * Creates a new {@link CollectionBuilder} for an existing
	 * {@link Collection}.
	 */
	public CollectionBuilder(Collection<E> collection)
	{
		this.collection = collection;
	}
	
	/**
	 * Adds a new value to the collection.
	 * @return This builder.
	 */
	public CollectionBuilder<E> value(E value)
	{
		collection.add(value);
		return this;
	}
	
	/**
	 * Adds several values to the collection.
	 * @return This builder.
	 */
	@SuppressWarnings("unchecked")
	public CollectionBuilder<E> values(E... values)
	{
		for(E value : values)
		{
			collection.add(value);
		}
		return this;
	}
	
	/**
	 * Adds the content of an existing collection to this collection.
	 * @return This builder.
	 */
	public CollectionBuilder<E> values(Collection<E> collection)
	{
		collection.addAll(collection);
		return this;
	}
	
	/**
	 * Adds the content of several collections to this collection.
	 * @return This builder.
	 */
	@SuppressWarnings("unchecked")
	public CollectionBuilder<E> values(Collection<E>... collections)
	{
		for(Collection<E> collection : collections)
		{
			collection.addAll(collection);
		}
		return this;
	}
	
	/**
	 * Returns the collection managed by this builder.
	 */
	@SuppressWarnings("unchecked")
	public <T extends Collection<E>> T build()
	{
		return (T) collection;
	}
	
	/**
	 * Returns an unmodifiable version of the collection managed by this
	 * builder.
	 */
	@SuppressWarnings("unchecked")
	public <T extends Collection<E>> T buildUnmodifiable()
	{
		return (T) Collections.unmodifiableCollection(collection);
	}
}
