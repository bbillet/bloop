/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.util;

import java.util.Calendar;

/**
 * A set of helpers for time operations.
 * 
 * @author Benjamin Billet
 * @version 0.1
 */
public final class Time
{
	private Time() { }
	
	/**
	 * Waits for a given amount of milliseconds. If a {@link InterruptedException}
	 * occurs, the function returns {@code true} immediately.
	 * @param milliseconds The time to wait.
	 * @return {@code true} if the pause was interrupted, {@code false} otherwise.
	 * @see Thread#sleep(long)
	 */
	public static boolean pause(int milliseconds)
	{
		try
		{
			Thread.sleep(milliseconds);
		}
		catch (InterruptedException e) 
		{
			return true;
		}
		return false;
	}
	
	/**
	 * Computes the time delta between the current time and a given timestamp.
	 * @param time The timestamp
	 * @return {@code System.currentTimeMillis() - time}
	 */
	public static long delta(long time)
	{
		return System.currentTimeMillis() - time;
	}

	/**
	 * Short alias for {@link System#currentTimeMillis()}.
	 * @return The difference, measured in milliseconds, between the current
	 *         time and midnight, January 1, 1970 UTC.
	 */
	public static long now()
	{
		return System.currentTimeMillis();
	}

	/**
	 * Returns the current time formatted as hours:minutes:seconds, without
	 * using a {@link java.text.DateFormat}.
	 */
	public static String nowString()
	{
		Calendar c = Calendar.getInstance();
		return c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE) + ":" + c.get(Calendar.SECOND);
	}
}
