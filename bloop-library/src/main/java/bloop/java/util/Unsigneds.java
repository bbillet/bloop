/*  
 * The Bloop Library.
 * Copyright (c) 2012 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.util;

import java.math.BigInteger;

/**
 * Helper functions for unsigned values.
 * @author Benjamin Billet
 * @version 0.1
 */
public final class Unsigneds
{
	private Unsigneds() { }
	
	/**
	 * Maximum value for an unsigned byte.
	 */
	public static final short UnsignedByteMaxValue = 255;
	/**
	 * Maximum value for an unsigned short.
	 */
	public static final int UnsignedShortMaxValue = 65535;
	/**
	 * Maximum value for an unsigned int.
	 */
	public static final long UnsignedIntMaxValue = 4294967295l;
	
	
	/**
	 * Converts a 8-length byte array to an unsigned long stored into a
	 * {@link BigInteger}.
	 * @param bytes The byte array.
	 * @return The unsigned long, as a {@link BigInteger}.
	 * @see BigInteger#BigInteger(byte[])
	 */
	public static BigInteger toUnsignedLong(byte[] bytes)
	{
		return new BigInteger(1, bytes);
	}
	
	/**
	 * Converts a 4-length byte array to an unsigned int stored into a long.
	 * @param bytes The byte array.
	 * @return The unsigned int, as a long.
	 */
	public static long toUnsignedInt(byte[] bytes)
	{
		return newUnsignedInt(bytes[0], bytes[1], bytes[2], bytes[3]);
	}
	
	/**
	 * Converts a 2-length byte array to an unsigned short stored into an int.
	 * @param bytes The byte array.
	 * @return The unsigned short, as an int.
	 */
	public static int toUnsignedShort(byte[] bytes)
	{
		return newUnsignedShort(bytes[0], bytes[1]);
	}
	
	/**
	 * Converts 8 bytes to an unsigned long stored into a {@link BigInteger}.
	 * @param b1 The byte 1 (most significant).
	 * @param b2 The byte 2.
	 * @param b3 The byte 3.
	 * @param b4 The byte 4.
	 * @param b5 The byte 5.
	 * @param b6 The byte 6.
	 * @param b7 The byte 7.
	 * @param b8 The byte 8 (least significant).
	 * @return The unsigned long, as a {@link BigInteger}.
	 * @see BigInteger#BigInteger(byte[])
	 */
	public static BigInteger newUnsignedLong(byte b1, byte b2, byte b3, byte b4, byte b5, byte b6, byte b7, byte b8)
	{
		return toUnsignedLong(new byte[] { b1, b2, b3, b4, b5, b6, b7, b8 });
	}

	/**
	 * Converts 4 bytes to an unsigned int stored into a long.
	 * @param b1 The byte 1 (most significant).
	 * @param b2 The byte 2.
	 * @param b3 The byte 3.
	 * @param b4 The byte 4 (least significant).
	 * @return The unsigned int, as a long.
	 */
	public static long newUnsignedInt(byte b1, byte b2, byte b3, byte b4)
	{
		return ((long) (b1 & 0xFF)) << 24 
				| ((long) (b2 & 0xFF)) << 16 
				| ((long) (b3 & 0xFF)) << 8 
				| ((long) (b4 & 0xFF));
	}
	
	/**
	 * Converts 2 bytes to an unsigned short stored into an int.
	 * @param b1 The byte 1 (most significant).
	 * @param b2 The byte 2 (least significant).
	 * @return The unsigned short, as an int.
	 */
	public static int newUnsignedShort(byte b1, byte b2)
	{
		return ((b1 & 0xFF) << 8 | (b2 & 0xFF));
	}
}
