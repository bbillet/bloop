/*  
 * The Bloop Library.
 * Copyright (c) 2014 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.util;

/**
 * Helper functions for parsing {@link String} into primitive values.
 *
 * @author Benjamin Billet
 * @version 0.1
 */
public class PrimitiveStrings
{
	private PrimitiveStrings() { }
	
	/**
	 * Parses a byte. If the {@code string} is null or if the parsing fails, a
	 * default value is returned.
	 * @param string The string to parse.
	 * @param defaultValue The default value.
	 * @return The byte, or {@code defaultValue}.
	 * @see Byte#parseByte(String)
	 */
	public byte parseByte(String string, byte defaultValue)
	{
		try
		{
			if(string != null)
				return Byte.parseByte(string);
		}
		catch(NumberFormatException e) { }
		return defaultValue;
	}
	
	/**
	 * Parses a byte. If the {@code string} is null or if the parsing fails, a
	 * default value is returned.
	 * @param string The string to parse.
	 * @param radix The radix to be used
	 * @param defaultValue The default value.
	 * @return The byte, or {@code defaultValue}.
	 * @see Byte#parseByte(String, int)
	 */
	public byte parseByte(String string, int radix, byte defaultValue)
	{
		try
		{
			if(string != null)
				return Byte.parseByte(string, radix);
		}
		catch(NumberFormatException e) { }
		return defaultValue;
	}
	
	/**
	 * Parses a short. If the {@code string} is null or if the parsing fails, a
	 * default value is returned.
	 * @param string The string to parse.
	 * @param defaultValue The default value.
	 * @return The short, or {@code defaultValue}.
	 * @see Short#parseShort(String)
	 */
	public short parseShort(String string, short defaultValue)
	{
		try
		{
			if(string != null)
				return Short.parseShort(string);
		}
		catch(NumberFormatException e) { }
		return defaultValue;
	}
	
	/**
	 * Parses a short. If the {@code string} is null or if the parsing fails, a
	 * default value is returned.
	 * @param string The string to parse.
	 * @param radix The radix to be used
	 * @param defaultValue The default value.
	 * @return The short, or {@code defaultValue}.
	 * @see Short#parseShort(String, int)
	 */
	public short parseShort(String string, int radix, short defaultValue)
	{
		try
		{
			if(string != null)
				return Short.parseShort(string, radix);
		}
		catch(NumberFormatException e) { }
		return defaultValue;
	}
	
	/**
	 * Parses an integer. If the {@code string} is null or if the parsing fails,
	 * a default value is returned.
	 * @param string The string to parse.
	 * @param defaultValue The default value.
	 * @return The int, or {@code defaultValue}.
	 * @see Integer#parseInt(String)
	 */
	public int parseInt(String string, int defaultValue)
	{
		try
		{
			if(string != null)
				return Integer.parseInt(string);
		}
		catch(NumberFormatException e) { }
		return defaultValue;
	}
	
	/**
	 * Parses an integer. If the {@code string} is null or if the parsing fails,
	 * a default value is returned.
	 * @param string The string to parse.
	 * @param radix The radix to be used
	 * @param defaultValue The default value.
	 * @return The int, or {@code defaultValue}.
	 * @see Integer#parseInt(String, int)
	 */
	public int parseInt(String string, int radix, int defaultValue)
	{
		try
		{
			if(string != null)
				return Integer.parseInt(string, radix);
		}
		catch(NumberFormatException e) { }
		return defaultValue;
	}
	
	/**
	 * Parses a long. If the {@code string} is null or if the parsing fails, a
	 * default value is returned.
	 * @param string The string to parse.
	 * @param defaultValue The default value.
	 * @return The long, or {@code defaultValue}.
	 * @see Long#parseLong(String)
	 */
	public long parseLong(String string, long defaultValue)
	{
		try
		{
			if(string != null)
				return Long.parseLong(string);
		}
		catch(NumberFormatException e) { }
		return defaultValue;
	}
	
	/**
	 * Parses a long. If the {@code string} is null or if the parsing fails, a
	 * default value is returned.
	 * @param string The string to parse.
	 * @param radix The radix to be used
	 * @param defaultValue The default value.
	 * @return The long, or {@code defaultValue}.
	 * @see Long#parseLong(String, int)
	 */
	public long parseLong(String string, int radix, long defaultValue)
	{
		try
		{
			if(string != null)
				return Long.parseLong(string, radix);
		}
		catch(NumberFormatException e) { }
		return defaultValue;
	}
	
	/**
	 * Parses a float. If the {@code string} is null or if the parsing fails, a
	 * default value is returned.
	 * @param string The string to parse.
	 * @param defaultValue The default value.
	 * @return The float, or {@code defaultValue}.
	 * @see Float#parseFloat(String)
	 */
	public float parseFloat(String string, float defaultValue)
	{
		try
		{
			if(string != null)
				return Float.parseFloat(string);
		}
		catch(NumberFormatException e) { }
		return defaultValue;
	}
	
	/**
	 * Parses a double. If the {@code string} is null or if the parsing fails, a
	 * default value is returned.
	 * @param string The string to parse.
	 * @param defaultValue The default value.
	 * @return The double, or {@code defaultValue}.
	 * @see Double#parseDouble(String)
	 */
	public double parseDouble(String string, double defaultValue)
	{
		try
		{
			if(string != null)
				return Double.parseDouble(string);
		}
		catch(NumberFormatException e) { }
		return defaultValue;
	}
	
	/**
	 * Parses a byte. If the {@code string} is null or if the parsing fails, a
	 * default value is returned.
	 * @param string The string to parse.
	 * @param defaultValue The default value.
	 * @return The byte, or {@code defaultValue}.
	 * @see Byte#valueOf(String)
	 */
	public Byte valueOf(String string, Byte defaultValue)
	{
		try
		{
			if(string != null)
				return Byte.valueOf(string);
		}
		catch(NumberFormatException e) { }
		return defaultValue;
	}
	
	/**
	 * Parses a byte. If the {@code string} is null or if the parsing fails, a
	 * default value is returned.
	 * @param string The string to parse.
	 * @param radix The radix to be used
	 * @param defaultValue The default value.
	 * @return The byte, or {@code defaultValue}.
	 * @see Byte#valueOf(String, int)
	 */
	public Byte valueOf(String string, int radix, Byte defaultValue)
	{
		try
		{
			if(string != null)
				return Byte.valueOf(string, radix);
		}
		catch(NumberFormatException e) { }
		return defaultValue;
	}
	
	/**
	 * Parses a short. If the {@code string} is null or if the parsing fails, a
	 * default value is returned.
	 * @param string The string to parse.
	 * @param defaultValue The default value.
	 * @return The short, or {@code defaultValue}.
	 * @see Short#valueOf(String)
	 */
	public Short valueOf(String string, Short defaultValue)
	{
		try
		{
			if(string != null)
				return Short.valueOf(string);
		}
		catch(NumberFormatException e) { }
		return defaultValue;
	}
	
	/**
	 * Parses a short. If the {@code string} is null or if the parsing fails, a
	 * default value is returned.
	 * @param string The string to parse.
	 * @param radix The radix to be used
	 * @param defaultValue The default value.
	 * @return The short, or {@code defaultValue}.
	 * @see Short#valueOf(String, int)
	 */
	public Short valueOf(String string, int radix, Short defaultValue)
	{
		try
		{
			if(string != null)
				return Short.valueOf(string, radix);
		}
		catch(NumberFormatException e) { }
		return defaultValue;
	}
	
	/**
	 * Parses an integer. If the {@code string} is null or if the parsing fails,
	 * a default value is returned.
	 * @param string The string to parse.
	 * @param defaultValue The default value.
	 * @return The int, or {@code defaultValue}.
	 * @see Integer#valueOf(String)
	 */
	public Integer valueOf(String string, Integer defaultValue)
	{
		try
		{
			if(string != null)
				return Integer.valueOf(string);
		}
		catch(NumberFormatException e) { }
		return defaultValue;
	}
	
	/**
	 * Parses an integer. If the {@code string} is null or if the parsing fails,
	 * a default value is returned.
	 * @param string The string to parse.
	 * @param radix The radix to be used
	 * @param defaultValue The default value.
	 * @return The int, or {@code defaultValue}.
	 * @see Integer#valueOf(String, int)
	 */
	public Integer valueOf(String string, int radix, Integer defaultValue)
	{
		try
		{
			if(string != null)
				return Integer.valueOf(string, radix);
		}
		catch(NumberFormatException e) { }
		return defaultValue;
	}
	
	/**
	 * Parses a long. If the {@code string} is null or if the parsing fails, a
	 * default value is returned.
	 * @param string The string to parse.
	 * @param defaultValue The default value.
	 * @return The long, or {@code defaultValue}.
	 * @see Long#valueOf(String)
	 */
	public Long valueOf(String string, Long defaultValue)
	{
		try
		{
			if(string != null)
				return Long.valueOf(string);
		}
		catch(NumberFormatException e) { }
		return defaultValue;
	}
	
	/**
	 * Parses a long. If the {@code string} is null or if the parsing fails, a
	 * default value is returned.
	 * @param string The string to parse.
	 * @param radix The radix to be used
	 * @param defaultValue The default value.
	 * @return The long, or {@code defaultValue}.
	 * @see Long#valueOf(String, int)
	 */
	public Long valueOf(String string, int radix, Long defaultValue)
	{
		try
		{
			if(string != null)
				return Long.valueOf(string, radix);
		}
		catch(NumberFormatException e) { }
		return defaultValue;
	}
	
	/**
	 * Parses a float. If the {@code string} is null or if the parsing fails, a
	 * default value is returned.
	 * @param string The string to parse.
	 * @param defaultValue The default value.
	 * @return The float, or {@code defaultValue}.
	 * @see Float#valueOf(String)
	 */
	public Float valueOf(String string, Float defaultValue)
	{
		try
		{
			if(string != null)
				return Float.valueOf(string);
		}
		catch(NumberFormatException e) { }
		return defaultValue;
	}
	
	/**
	 * Parses a double. If the {@code string} is null or if the parsing fails, a
	 * default value is returned.
	 * @param string The string to parse.
	 * @param defaultValue The default value.
	 * @return The double, or {@code defaultValue}.
	 * @see Double#valueOf(String)
	 */
	public Double valueOf(String string, Double defaultValue)
	{
		try
		{
			if(string != null)
				return Double.valueOf(string);
		}
		catch(NumberFormatException e) { }
		return defaultValue;
	}
}
