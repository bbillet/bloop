/*  
 * The Bloop Library.
 * Copyright (c) 2012 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.util;

import java.util.BitSet;

/**
 * Byte manipulation functions.
 *
 * @author Benjamin Billet
 * @version 0.1
 */
public final class Bytes
{
	private Bytes() { }
	
	/**
	 * Converts a long to a 8-length byte array.
	 */
	public static byte[] longToBytes(long value)
	{
		return new byte[]
		{
			(byte) (value >> 56), 
			(byte) (value >> 48),
			(byte) (value >> 40), 
			(byte) (value >> 32), 
			(byte) (value >> 24), 
			(byte) (value >> 16),
			(byte) (value >> 8), 
			(byte) value 
		};
	}
	
	/**
	 * Converts an int to a 4-length byte array.
	 */
	public static byte[] intToBytes(int value)
	{
		return new byte[]
		{ 
			(byte) (value >> 24), 
			(byte) (value >> 16),
			(byte) (value >> 8), 
			(byte) value 
		};
	}
	
	/**
	 * Converts a short to a 2-length byte array.
	 */
	public static byte[] shortToBytes(short value)
	{
		return new byte[]
		{ 
			(byte) (value >> 8), 
			(byte) value 
		};
	}
	
	/**
	 * Converts a 8-length byte array to a long.
	 */
	public static long toLong(byte[] bytes)
	{
		return newLong(bytes[0], bytes[1], bytes[2], bytes[3], bytes[4], bytes[5], bytes[6], bytes[7]);
	}
	
	/**
	 * Converts a 4-length byte array to an int.
	 */
	public static int toInt(byte[] bytes)
	{
		return newInt(bytes[0], bytes[1], bytes[2], bytes[3]);
	}
	
	/**
	 * Converts a 2-length byte array to a short.
	 */
	public static short toShort(byte[] bytes)
	{
		return newShort(bytes[0], bytes[1]);
	}

	/**
	 * Converts 8 bytes to a long.
	 * @param b1 The byte 1 (most significant).
	 * @param b2 The byte 2.
	 * @param b3 The byte 3.
	 * @param b4 The byte 4.
	 * @param b5 The byte 5.
	 * @param b6 The byte 6.
	 * @param b7 The byte 7.
	 * @param b8 The byte 8 (least significant).
	 */
	public static long newLong(byte b1, byte b2, byte b3, byte b4, byte b5, byte b6, byte b7, byte b8)
	{
		return ((long) b1 & 0xFF) << 56 
				| ((long) b2 & 0xFF) << 48 
				| ((long) b3 & 0xFF) << 40
				| ((long) b4 & 0xFF) << 32 
				| ((long) b5 & 0xFF) << 24 
				| ((long) b6 & 0xFF) << 16
				| ((long) b7 & 0xFF) << 8 
				| ((long) b8 & 0xFF);
	}
	
	/**
	 * Converts 4 bytes to an int.
	 * @param b1 The byte 1 (most significant).
	 * @param b2 The byte 2.
	 * @param b3 The byte 3.
	 * @param b4 The byte 4 (least significant).
	 * @return the int.
	 */
	public static int newInt(byte b1, byte b2, byte b3, byte b4)
	{
		return (b1 & 0xFF) << 24 
				| (b2 & 0xFF) << 16 
				| (b3 & 0xFF) << 8 
				| (b4 & 0xFF);
	}
	
	/**
	 * Converts 2 bytes to a short.
	 * @param b1 The byte 1 (most significant).
	 * @param b2 The byte 2 (least significant).
	 * @return the short.
	 */
	public static short newShort(byte b1, byte b2)
	{
		return (short)((b1 & 0xFF) << 8 | (b2 & 0xFF));
	}
	
	/**
	 * Sets a bit in a byte.
	 * @param b The original byte.
	 * @param bitIndex The index of the bit, between 0 (LSB) and 7 (MSB).
	 * @param value {@code true} to set the bit, or {@code false} to clear the
	 *            byte.
	 * @return The new byte.
	 */
	public static byte setBit(byte b, int bitIndex, boolean value)
	{
		if(bitIndex < Byte.SIZE)
		{
			if(value == true)
				b |= (1 << bitIndex);
			else
				b &= ~(1 << bitIndex);
		}
		
		return b;
	}
	
	/**
	 * Gets a single bit in a byte.
	 * @param b The original byte.
	 * @param bitIndex The index of the bit to get.
	 * @return 0 or 1, depending on the value of the bit.
	 */
	public static int getBit(byte b, int bitIndex)
	{
		if(bitIndex < Byte.SIZE)
			return (b >>> bitIndex) & 1;
		
		return 0;
	}
	
	/**
	 * Checks if a bit is set in a byte.
	 * @param b The original byte.
	 * @param bitIndex The index of the bit to check.
	 * @return {@code true} if the bit is set, {@code false} otherwise.
	 */
	public static boolean isBitSet(byte b, int bitIndex)
	{
		return getBit(b, bitIndex) == 1;
	}
	
	/**
	 * Initializes a {@link BitSet} from a value: {@code length} bits of
	 * {@code value} (bits 0 to {@code length} where 0 is the LSB) will be
	 * written in the {@code bitset}, starting at {@code offset}.
	 * @param value The value.
	 * @param bits The bitset.
	 * @param offset The bit offset.
	 * @param length The number of bits to put in the bitset.
	 * @see BitSet
	 */
	public static void toBitset(long value, BitSet bits, int offset, int length)
	{
		for(int i = 0; i < length; i++)
		{
			if(((value >>> i) & 1) == 1)
				bits.set(offset + i, true);
			else
				bits.set(offset + i, false);
		}
	}
	
	/**
	 * Converts the 8 first bit of a {@link BitSet} into a byte.
	 * @param bits the bitset.
	 * @return The byte.
	 * @see BitSet
	 */
	public static byte bitsetToByte(BitSet bits)
	{
		byte result = 0;
		if(bits.get(0) == true)
			result = 1;
		
		for(int i = 1; i < 8; i++)
		{
			if(bits.get(i) == true)
				result |= (1 << i);
		}
		
		return result;
	}

	/**
	 * Converts an array of ints to an array of bytes, by simply casting each
	 * int to byte.
	 */
	public static byte[] castToBytes(int... array)
	{
		byte[] bytes = new byte[array.length];
		for(int i = 0; i < array.length; i++)
		{
			bytes[i] = (byte) array[i];
		}
		return bytes;
	}
}
