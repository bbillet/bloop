/*  
 * The Bloop Library.
 * Copyright (c) 2012 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * An integer that will automatically roll to a min value after reaching a max
 * value. For example, the following {@link RollingInteger} will roll from 1 to
 * 3 and is initialized to 1.
 * 
 * <pre>{@code
 * RollingInteger roller = new RollingInteger(1, 1, 3);
 * 
 * roller.increment(); // value: 2
 * roller.increment(); // value: 3
 * roller.increment(); // value: 1
 * roller.increment(); // value: 2
 * roller.decrement(); // value: 1
 * roller.decrement(); // value: 3
 * roller.decrement(); // value: 2
 * }</pre>
 * 
 * A {@link RollingInteger} can register a set of {@link OnRoll} listeners,
 * which will be notified when the value of the integer rolls from min to max or
 * from max to min. See {@link RollingInteger#registerListener(OnRoll)} for details.
 * <br>
 * <b>Warning</b>: This class is not thread-safe.
 * 
 * @author Benjamin Billet
 * @version 0.1
 * @see OnRoll
 */
public class RollingInteger
{
	private int value;
	private int min;
	private int max;	
	private List<OnRoll<RollingInteger>> listeners;

	/**
	 * Creates a new {@link RollingInteger}.
	 * @param initial The current value.
	 * @param min The minimum value.
	 * @param max The maximum value.
	 * @param listeners A set of {@link OnRoll} listeners.
	 * @see OnRoll
	 * @throws IllegalArgumentException If 'min' is greater or equals to 'max',
	 *             or if 'initial' is not between 'min' and 'max' (inclusive).
	 */
	@SafeVarargs
	public RollingInteger(int initial, int min, int max, OnRoll<RollingInteger>... listeners)
	{
		this(initial, min, max, Arrays.asList(listeners));
	}
	
	/**
	 * Creates a new {@link RollingInteger}.
	 * @param initial The current value.
	 * @param min The minimum value.
	 * @param max The maximum value.
	 * @param listeners A set of {@link OnRoll} listeners.
	 * @see OnRoll
	 * @throws IllegalArgumentException If 'min' is greater or equals to 'max',
	 *             or if 'initial' is not between 'min' and 'max' (inclusive).
	 */
	public RollingInteger(int initial, int min, int max, Collection<OnRoll<RollingInteger>> listeners)
	{
		if(min < 0 || max < 0 || max <= min || initial < min || initial > max)
			throw new IllegalArgumentException("'min' must be lesser than 'max', and 'initial' must be between 'min' and 'max' (inclusive)");
		
		this.listeners = new ArrayList<>(listeners);
		this.value = initial;
		this.min = min;
		this.max = max;
	}
	
	/**
	 * Creates a new {@link RollingInteger}, which rolls from 0 to
	 * {@link Integer#MAX_VALUE} and is initialized to 0.
	 */
	public RollingInteger()
	{
		this(0, 0, Integer.MAX_VALUE);
	}
	
	/**
	 * Increments the value of the integer. If the increment value is greater
	 * than the maximum, the integer is rolled to the minimum.
	 */
	public void increment()
	{
		if(value == max)
		{
			value = min;
			notifyListeners();
		}
		else
			value++;
	}
	
	/**
	 * Decrements the value of the integer. If the decremented value is lesser
	 * than the minimum, the integer is rolled to the maximum.
	 */
	public void decrement()
	{
		if(value == min)
		{
			value = max;
			notifyListeners();
		}
		else
			value--;
	}
	
	/**
	 * Returns the current value of the integer.
	 */
	public int getValue()
	{
		return value;
	}
	
	/**
	 * Resets the value of the integer to the minimum value.
	 */
	public void reset()
	{
		value = min;
	}
	
	/**
	 * Registers a new {@link OnRoll} listener.
	 * @param listener The listener.
	 * @see OnRoll
	 */
	public void registerListener(OnRoll<RollingInteger> listener)
	{
		if(listener == null)
			throw new NullPointerException();
		
		listeners.add(listener);
	}
	
	private void notifyListeners()
	{
		for(OnRoll<RollingInteger> listener : listeners)
		{
			listener.onRoll(this);
		}
	}
	
	/**
	 * A listener for being notified when a rolling number has rolled.
	 * @author Benjamin Billet
	 * @version 0.1
	 * @param <T> The type of rolling number associated to the listener.
	 */
	public static interface OnRoll<T>
	{
		public void onRoll(T rollingNumber);
	}
}
