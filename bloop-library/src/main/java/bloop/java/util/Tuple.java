/*  
 * The Bloop Library.
 * Copyright (c) 2012 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * A simple tuple class.
 * 
 * @author Benjamin Billet
 * @param <T> The type of the values contained in the tuple.
 * @version 0.1
 */
public class Tuple<T> implements Iterable<T>
{
	private List<T> values;
	
	/**
	 * Creates a new tuple filled with an array of values (values are copied by
	 * reference). The size of the new tuple equals {@code values.length}.
	 * @param values The values
	 */
	@SafeVarargs
	public Tuple(T... values)
	{
		this.values = new ArrayList<T>(Arrays.asList(values));
	}
	
	/**
	 * Creates a new tuple from an existing tuple (values are copied by
	 * reference).
	 * @param tuple The original tuple.
	 */
	public Tuple(Tuple<T> tuple)
	{
		this.values = new ArrayList<T>(tuple.values);
	}
	
	/**
	 * Creates a new tuple of a given size.
	 * @param size The size.
	 */
	public Tuple(int size)
	{
		values = new ArrayList<T>(size);
	}

	/**
	 * Sets the i-th element of the tuple.
	 * @param i The index.
	 * @param value The value.
	 */
	public void set(int i, T value)
	{
		values.set(i, value);
	}
	
	/**
	 * Returns the i-th element of the tuple.
	 * @param i The index.
	 */
	public T get(int i)
	{
		return values.get(i);
	}
	
	/**
	 * Returns the size of the tuple.
	 */
	public int size()
	{
		return values.size();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<T> iterator()
	{
		return values.iterator();
	}
	
	/**
	 * Two tuples are equals if (i) they have the same size and, for each i, the
	 * i-th elements of the two tuples are equals.
	 * @param obj The reference object with which to compare.
	 * @return {@code true}, if this tuples equals {@code obj}.
	 * @see Object#equals(Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if(obj == this)
			return true;
		if(obj instanceof Tuple)
		{
			Tuple<?> tuple = (Tuple<?>) obj;
			if(tuple.values.size() == values.size())
			{
				Iterator<?> it1 = iterator();
				Iterator<?> it2 = tuple.iterator();
				while(it1.hasNext())
				{
					if(it1.next().equals(it2.next()) == false)
						return false;
				}
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Creates a new unmodifiable tuple from an existing one. The new tuple will
	 * throw {@link UnsupportedOperationException} when
	 * {@link Tuple#set(int, Object)} is invoked.
	 * @param tuple The original tuple.
	 * @return The unmodifiable tuple.
	 * @param <T> The type of values stored in the tuple.
	 */
	public static <T> Tuple<T> unmodifiableTuple(Tuple<T> tuple)
	{
		return new UnmodifiableTuple<T>(tuple);
	}
	
	private static class UnmodifiableTuple<T> extends Tuple<T>
	{
		private UnmodifiableTuple(Tuple<T> tuple)
		{
			super(tuple);
		}
		
		@Override
		public void set(int i, T element)
		{
			throw new UnsupportedOperationException("unmodifiable tuple");
		}
	}
}
