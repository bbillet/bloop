/*  
 * The Bloop Library.
 * Copyright (c) 2015 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.util;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * A Map implementation, with case-insensitive {@link String} keys.
 * @author Benjamin Billet
 * @version 0.1
 * @param <V> The type of mapped values.
 */
public class CaseInsensitiveMap<V> implements Map<String, V>
{
	private Map<String, V> internal;
	
	/**
	 * Creates a new {@link CaseInsensitiveMap}, backed by an existing map.
	 * @param map The existing map.
	 */
	public CaseInsensitiveMap(Map<String, V> internal)
	{
		this.internal = internal;
	}
	
	/**
	 * Creates a new {@link CaseInsensitiveMap}, backed by a {@link HashMap}.
	 */
	public CaseInsensitiveMap()
	{
		this.internal = new HashMap<>();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size()
	{
		return internal.size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEmpty()
	{
		return internal.isEmpty();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsKey(Object key)
	{
		return internal.containsKey(toLowerCase(key));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsValue(Object value)
	{
		return internal.containsValue(value);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public V get(Object key)
	{
		return internal.get(toLowerCase(key));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public V put(String key, V value)
	{
		return internal.put(toLowerCase(key), value);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public V remove(Object key)
	{
		return internal.remove(toLowerCase(key));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void putAll(Map<? extends String, ? extends V> map)
	{
		for(Entry<? extends String, ? extends V> e : map.entrySet())
		{
			internal.put(toLowerCase(e.getKey()), e.getValue());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear()
	{
		internal.clear();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> keySet()
	{
		return internal.keySet();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<V> values()
	{
		return internal.values();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<Entry<String, V>> entrySet()
	{
		return internal.entrySet();
	}
	
	private String toLowerCase(Object o)
	{
		String s = (String) o;
		if(s != null)
			return s.toLowerCase();
		else
			return s;
	}
	
	/**
	 * Creates a new {@link CaseInsensitiveMap}, using {@link HashMap}.
	 */
	public static <V> Map<String, V> caseInsensitiveHashMap()
	{
		return new CaseInsensitiveMap<>(new HashMap<String, V>());
	}
	
	/**
	 * Creates a new {@link CaseInsensitiveMap}, using {@link HashMap}.
	 * @param initialCapacity The initial capacity
	 * @throws IllegalArgumentException If the initial capacity is negative
	 */
	public static <V> Map<String, V> caseInsensitiveHashMap(int initialCapacity)
	{
		return new CaseInsensitiveMap<>(new HashMap<String, V>(initialCapacity));
	}
	
	/**
	 * Creates a new {@link CaseInsensitiveMap}, using {@link HashMap}.
	 * @param initialCapacity The initial capacity
	 * @param loadFactor The load factor
	 * @throws IllegalArgumentException If the initial capacity is negative or
	 *             the load factor is nonpositive
	 */
	public static <V> Map<String, V> caseInsensitiveHashMap(int initialCapacity, float loadFactor)
	{
		return new CaseInsensitiveMap<>(new HashMap<String, V>(initialCapacity, loadFactor));
	}
	
	/**
	 * Creates a new {@link CaseInsensitiveMap}, using {@link TreeMap}.
	 */
	public static <V> Map<String, V> caseInsensitiveTreeMap()
	{
		return new CaseInsensitiveMap<>(new TreeMap<String, V>());
	}
	
	/**
	 * Creates a new {@link CaseInsensitiveMap}, using {@link TreeMap}.
	 * @param comparator The comparator that will be used to order this map. If
	 *            {@code null}, the {@linkplain Comparable natural ordering} of
	 *            the keys will be used.
	 */
	public static <V> Map<String, V> caseInsensitiveTreeMap(Comparator<? super String> comparator)
	{
		return new CaseInsensitiveMap<>(new TreeMap<String, V>(comparator));
	}
}
