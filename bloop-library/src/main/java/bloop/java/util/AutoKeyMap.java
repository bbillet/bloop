/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
/*  
 * The Bloop Library.
 * Copyright (c) 2012 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
import java.util.Set;
import java.util.TreeMap;

/**
 * A decorator for {@link Map}, which use a {@link KeyGetter} to extract
 * directly the key of an element when inserted into the map.<br>
 * For example, consider the following code: 
 * 
 * <pre>{@code
 * Map<String, Something> map = new HashMap<>();
 * Something something1 = new Something();
 * map.put(something1.getKey(), something1);
 * Something something2 = new Something();
 * map.put(something2.getKey(), something2);  
 * }</pre>
 * 
 * Using {@link AutoKeyMap}, the key extraction can be specified once for all: 
 * 
 * <pre>{@code
 * KeyGetter<String, Something> getter = new KeyGetter<String, Something>()
 * {
 * 	public String getKey(Something value)
 * 	{
 * 		return something.getKey();
 * 	}
 * };
 * Map<String, Something> map = new AutoKeyMap<>(new HashMap<String, Object>(), getter);
 * map.put(new Something());
 * map.put(new Something());
 * }</pre>
 * 
 * The main benefit of this approach consists into externalizing the key extraction logic, which can
 * be reused elsewhere.
 * 
 * @author Benjamin Billet
 * @version 0.1
 * 
 * @param <K> The type of keys maintained by this map.
 * @param <V> The type of mapped values.
 */
public class AutoKeyMap<K, V> implements Map<K, V>
{
	private Map<K, V> internal;
	private KeyGetter<K, V> keyGetter;
	
	/**
	 * Creates a new {@link AutoKeyMap}, backed by an existing map.
	 * @param map The existing map.
	 * @param keyGetter A {@link KeyGetter} instance.
	 */
	public AutoKeyMap(Map<K, V> map, KeyGetter<K, V> keyGetter)
	{
		this.internal = map;
		this.keyGetter = keyGetter;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size()
	{
		return internal.size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEmpty()
	{
		return internal.isEmpty();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsKey(Object key)
	{
		return internal.containsKey(key);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsValue(Object value)
	{
		return internal.containsValue(value);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public V get(Object key)
	{
		return internal.get(key);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public V put(K key, V value)
	{
		return internal.put(key, value);
	}
	
	/**
	 * Similar to {@link AutoKeyMap#put(Object, Object)}, but the key will be
	 * extracted directly, using the {@link KeyGetter}.
	 */
	public V put(V value)
	{
		return internal.put(keyGetter.getKey(value), value);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public V remove(Object key)
	{
		return internal.remove(key);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void putAll(Map<? extends K, ? extends V> m)
	{
		internal.putAll(m);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear()
	{
		internal.clear();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<K> keySet()
	{
		return internal.keySet();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<V> values()
	{
		return internal.values();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<Entry<K, V>> entrySet()
	{
		return internal.entrySet();
	}

	/**
	 * Returns the {@link KeyGetter} used by this map.
	 */
	public KeyGetter<K, V> getKeyGetter()
	{
		return keyGetter;
	}

	/**
	 * A functor which, given an object, returns its key.
	 * @author Benjamin Billet
	 * @version 0.1
	 */
	public static interface KeyGetter<K, V>
	{
		/**
		 * Returns the key inferred from a value.
		 */
		public K getKey(V value);
	}
	
	/**
	 * Creates a new {@link AutoKeyMap}, backed with an {@link HashMap}.
	 * @param keyGetter A {@link KeyGetter} instance.
	 */
	public static <K, V> AutoKeyMap<K, V> autoKeyHashMap(KeyGetter<K, V> keyGetter)
	{
		return new AutoKeyMap<>(new HashMap<K, V>(), keyGetter);
	}
	
	/**
	 * Creates a new {@link AutoKeyMap}, backed with a {@link TreeMap}.
	 * @param keyGetter A {@link KeyGetter} instance.
	 */
	public static <K, V> AutoKeyMap<K, V> autoKeyTreeMap(KeyGetter<K, V> keyGetter)
	{
		return new AutoKeyMap<>(new TreeMap<K, V>(), keyGetter);
	}
}
