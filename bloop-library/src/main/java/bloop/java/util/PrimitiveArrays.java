/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.util;

/**
 * A set of helper functions for dealing with arrays of primitive types.
 * 
 * @author Benjamin Billet
 * @version 0.1
 */
public final class PrimitiveArrays
{
	private PrimitiveArrays() { }
	
	/**
	 * Creates a new array of boxed bytes from an array of primitive bytes.
	 * @param array The byte array.
	 * @return The Byte array.
	 */
	public static Byte[] toArray(byte[] array)
	{
		Byte[] newArray = new Byte[array.length];
		for(int i = 0; i < array.length; i++)
		{
			newArray[i] = array[i];
		}
		return newArray;
	}
	
	/**
	 * Creates a new array of boxed shorts from an array of primitive shorts.
	 * @param array The short array.
	 * @return The Short array.
	 */
	public static Short[] toArray(short[] array)
	{
		Short[] newArray = new Short[array.length];
		for(int i = 0; i < array.length; i++)
		{
			newArray[i] = array[i];
		}
		return newArray;
	}
	
	/**
	 * Creates a new array of boxed ints from an array of primitive ints.
	 * @param array The int array.
	 * @return The Integer array.
	 */
	public static Integer[] toArray(int[] array)
	{
		Integer[] newArray = new Integer[array.length];
		for(int i = 0; i < array.length; i++)
		{
			newArray[i] = array[i];
		}
		return newArray;
	}
	
	/**
	 * Creates a new array of boxed longs from an array of primitive longs.
	 * @param array The long array.
	 * @return The Long array.
	 */
	public static Long[] toArray(long[] array)
	{
		Long[] newArray = new Long[array.length];
		for(int i = 0; i < array.length; i++)
		{
			newArray[i] = array[i];
		}
		return newArray;
	}
	
	/**
	 * Creates a new array of boxed floats from an array of primitive floats.
	 * @param array The float array.
	 * @return The Float array.
	 */
	public static Float[] toArray(float[] array)
	{
		Float[] newArray = new Float[array.length];
		for(int i = 0; i < array.length; i++)
		{
			newArray[i] = array[i];
		}
		return newArray;
	}
	
	/**
	 * Creates a new array of boxed doubles from an array of primitive doubles.
	 * @param array The double array.
	 * @return The Double array.
	 */
	public static Double[] toArray(double[] array)
	{
		Double[] newArray = new Double[array.length];
		for(int i = 0; i < array.length; i++)
		{
			newArray[i] = array[i];
		}
		return newArray;
	}
	
	/**
	 * Creates a new array of boxed booleans from an array of primitive
	 * booleans.
	 * @param array The boolean array.
	 * @return The Boolean array.
	 */
	public static Boolean[] toArray(boolean[] array)
	{
		Boolean[] newArray = new Boolean[array.length];
		for(int i = 0; i < array.length; i++)
		{
			newArray[i] = array[i];
		}
		return newArray;
	}
	
	/**
	 * Creates a new array of boxed chars from an array of primitive chars.
	 * @param array The char array.
	 * @return The Character array.
	 */
	public static Character[] toArray(char[] array)
	{
		Character[] newArray = new Character[array.length];
		for(int i = 0; i < array.length; i++)
		{
			newArray[i] = array[i];
		}
		return newArray;
	}
	
	/**
	 * Creates a new array of primitive bytes from an array of boxed bytes.
	 * @param array The Byte array.
	 * @return The byte array.
	 */
	public static byte[] toArray(Byte[] array)
	{
		byte[] newArray = new byte[array.length];
		for(int i = 0; i < array.length; i++)
		{
			newArray[i] = array[i];
		}
		return newArray;
	}
	
	/**
	 * Creates a new array of primitive shorts from an array of boxed shorts.
	 * @param array The Short array.
	 * @return The short array.
	 */
	public static short[] toArray(Short[] array)
	{
		short[] newArray = new short[array.length];
		for(int i = 0; i < array.length; i++)
		{
			newArray[i] = array[i];
		}
		return newArray;
	}
	
	/**
	 * Creates a new array of primitive ints from an array of boxed ints.
	 * @param array The Integer array.
	 * @return The int array.
	 */
	public static int[] toArray(Integer[] array)
	{
		int[] newArray = new int[array.length];
		for(int i = 0; i < array.length; i++)
		{
			newArray[i] = array[i];
		}
		return newArray;
	}
	
	/**
	 * Creates a new array of primitive longs from an array of boxed longs.
	 * @param array The Long array.
	 * @return The long array.
	 */
	public static long[] toArray(Long[] array)
	{
		long[] newArray = new long[array.length];
		for(int i = 0; i < array.length; i++)
		{
			newArray[i] = array[i];
		}
		return newArray;
	}
	
	/**
	 * Creates a new array of primitive floats from an array of boxed floats.
	 * @param array The Float array.
	 * @return The float array.
	 */
	public static float[] toArray(Float[] array)
	{
		float[] newArray = new float[array.length];
		for(int i = 0; i < array.length; i++)
		{
			newArray[i] = array[i];
		}
		return newArray;
	}
	
	/**
	 * Creates a new array of primitive doubles from an array of boxed doubles.
	 * @param array The Double array.
	 * @return The double array.
	 */
	public static double[] toArray(Double[] array)
	{
		double[] newArray = new double[array.length];
		for(int i = 0; i < array.length; i++)
		{
			newArray[i] = array[i];
		}
		return newArray;
	}
	
	/**
	 * Creates a new array of primitive booleans from an array of boxed
	 * booleans.
	 * @param array The Boolean array.
	 * @return The boolean array.
	 */
	public static boolean[] toArray(Boolean[] array)
	{
		boolean[] newArray = new boolean[array.length];
		for(int i = 0; i < array.length; i++)
		{
			newArray[i] = array[i];
		}
		return newArray;
	}
	
	/**
	 * Creates a new array of primitive chars from an array of boxed chars.
	 * @param array The Character array.
	 * @return The char array.
	 */
	public static char[] toArray(Character[] array)
	{
		char[] newArray = new char[array.length];
		for(int i = 0; i < array.length; i++)
		{
			newArray[i] = array[i];
		}
		return newArray;
	}
}
