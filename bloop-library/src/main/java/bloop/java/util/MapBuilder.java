/*  
 * The Bloop Library.
 * Copyright (c) 2014 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.util;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

/**
 * Builder pattern for building {@link Map} objects. 
 * <pre>{@code
 * Map<String, String> map = new MapBuilder<>(new HashMap<String, String>())
 * 		.entry("key1", "value1)
 * 		.entries(new String[] { "key2", "key3" }, new String[] { "value2", "value3" })
 * 		.entries(anotherMap)
 * 		.build();
 * }</pre>
 * 
 * @author Benjamin Billet
 * @version 0.1
 * 
 * @param <K> The type of keys for the maps produced by this builder.
 * @param <V> The type of values for the maps produced by this builder.
 */
public final class MapBuilder<K, V>
{
	private Map<K, V> map;
	
	/**
	 * Creates a new {@link MapBuilder} for a new {@link HashMap}.
	 */
	public MapBuilder() 
	{ 
		this.map = new HashMap<K, V>();
	}
	
	/**
	 * Creates a new {@link MapBuilder} for an existing {@link Map}.
	 */
	public MapBuilder(Map<K, V> map) 
	{ 
		this.map = map;
	}
	
	/**
	 * Puts a new entry into the map.
	 * 
	 * @param key
	 *            The entry key.
	 * @param value
	 *            The entry value.
	 * @return This builder.
	 * @see Map#put(Object, Object)
	 */
	public MapBuilder<K, V> entry(K key, V value)
	{
		map.put(key, value);
		return this;
	}
	
	/**
	 * Puts a new entry into the map.
	 * 
	 * @param entry
	 *            An {@link Entry} object.
	 * @return This builder.
	 * @see Map#put(Object, Object)
	 */
	public MapBuilder<K, V> entry(Entry<K, V> entry)
	{
		return entry(entry.getKey(), entry.getValue());
	}
	
	/**
	 * Puts several entries into the map. Each item i of 'keys' will be
	 * associated to the corresponding item i of 'values'.
	 * 
	 * @param keys
	 *            The entry keys.
	 * @param values
	 *            The values.
	 * @return This builder.
	 */
	public MapBuilder<K, V> entries(K[] keys, V[] values)
	{
		Maps.putAll(map, keys, values);
		return this;
	}
	
	/**
	 * Puts several entries into the map. Each item i of 'keys' will be
	 * associated to the corresponding item i of 'values'.
	 * 
	 * @param keys
	 *            The entry keys.
	 * @param values
	 *            The values.
	 * @return This builder.
	 */
	public MapBuilder<K, V> entries(List<K> keys, List<V> values)
	{
		Maps.putAll(map, keys, values);
		return this;
	}
	
	/**
	 * Puts the content of a map into this map.
	 * @return This builder.
	 * @see Map#putAll(Map)
	 */
	public MapBuilder<K, V> entries(Map<K, V> map)
	{
		map.putAll(map);
		return this;
	}
	
	/**
	 * Puts several entries into this map.
	 * @param entries A collection of {@link Entry} objects.
	 * @return This builder.
	 */
	public MapBuilder<K, V> entries(Collection<Entry<K, V>> entries)
	{
		for(Entry<K, V> entry : entries)
		{
			map.put(entry.getKey(), entry.getValue());
		}
		return this;
	}
	
	/**
	 * Puts several entries into this map.
	 * @param entries A variadic array of {@link Entry} objects.
	 * @return This builder.
	 */
	@SuppressWarnings("unchecked")
	public MapBuilder<K, V> entries(Entry<K, V>... entries)
	{
		for(Entry<K, V> entry : entries)
		{
			map.put(entry.getKey(), entry.getValue());
		}
		return this;
	}
	
	/**
	 * Returns the map managed by this builder.
	 */
	public Map<K, V> build()
	{
		return map;
	}
	
	/**
	 * Returns an unmodifiable version of the map managed by this builder.
	 */
	public Map<K, V> buildUnmodifiable()
	{
		return Collections.unmodifiableMap(map);
	}
	
	/**
	 * Creates a new {@link MapBuilder} for an {@link HashMap}.
	 */
	public static <K, V> MapBuilder<K, V> hashMapBuilder()
	{
		return new MapBuilder<>(new HashMap<K, V>());
	}
	
	/**
	 * Creates a new {@link MapBuilder} for a {@link TreeMap}.
	 */
	public static <K, V> MapBuilder<K, V> treeMapBuilder()
	{
		return new MapBuilder<>(new TreeMap<K, V>());
	}
}
