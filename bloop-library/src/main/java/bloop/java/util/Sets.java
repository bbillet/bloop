/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.util;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Common operations for sets.<br>
 * <b>Warning</b>: These functions will not create a copy of the given sets and may
 * modify their content. You are responsible of creating copies of your sets, in
 * order to avoid modifications. Obviously, unmodifiable sets will lead most of
 * these functions to throw {@link UnsupportedOperationException}.
 * 
 * @author Benjamin Billet
 * @version 0.1
 */
public class Sets 
{
	/**
	 * Computes the union of two sets. <br>
	 * <b>Warning</b>: This function will not create a copy of the given sets and will
	 * modify the content of {@code set1}. You are responsible of creating
	 * copies of your sets, in order to avoid modifications. Obviously,
	 * unmodifiable sets will lead this function to throw
	 * {@link UnsupportedOperationException}.
	 * 
	 * @param set1 The first set.
	 * @param set2 The second set.
	 * @return The result of the union will be stored in {@code set1},
	 *         which is returned.
	 */
	public static <T> Set<T> union(Set<T> set1, Set<T> set2)
	{
		if(set1.size() == 0)
			return set2;
		else if(set2.size() == 0)
			return set1;
		
		set1.addAll(set2);
		return set1;
	}
	
	/**
	 * Computes the intersection of two sets. <br>
	 * <b>Warning</b>: This function will not create a copy of the given sets and will
	 * modify the content of {@code set1}. You are responsible of creating
	 * copies of your sets, in order to avoid modifications. Obviously,
	 * unmodifiable sets will lead this function to throw
	 * {@link UnsupportedOperationException}.
	 * 
	 * @param set1 The first set.
	 * @param set2 The second set.
	 * @return The result of the intersection will be stored in {@code set1},
	 *         which is returned.
	 */
	public static <T> Set<T> intersection(Set<T> set1, Set<T> set2)
	{
		if(set1.size() == 0)
			return set1;
		else if(set2.size() == 0)
			return set2;
		
		Iterator<T> it = set1.iterator();
		while(it.hasNext())
		{
			if(set2.contains(it.next()) == false)
				it.remove();
		}
		return set1;
	}
	
	/**
	 * Computes the difference of two sets. <br>
	 * <b>Warning</b>: This function will not create a copy of the given sets and will
	 * modify the content of {@code set1}. You are responsible of creating
	 * copies of your sets, in order to avoid modifications. Obviously,
	 * unmodifiable sets will lead this function to throw
	 * {@link UnsupportedOperationException}.
	 * 
	 * @param set1 The first set.
	 * @param set2 The second set.
	 * @return The result of the difference will be stored in {@code set1},
	 *         which is returned.
	 */
	public static <T> Set<T> difference(Set<T> set1, Set<T> set2)
	{
		if(set1.size() == 0 || set2.size() == 0)
			return set1;
		
		for(T value : set2)
		{
			set1.remove(value);
		}
		return set1;
	}
	
	/**
	 * Computes the reverse difference of two sets. <br>
	 * <b>Warning</b>: This function will not create a copy of the given sets and will
	 * modify the content of {@code set2}. You are responsible of creating
	 * copies of your sets, in order to avoid modifications. Obviously,
	 * unmodifiable sets will lead this function to throw
	 * {@link UnsupportedOperationException}.
	 * 
	 * @param set1 The first set.
	 * @param set2 The second set.
	 * @return The result of the difference will be stored in {@code set2},
	 *         which is returned.
	 */
	public static <T> Set<T> reverseDifference(Set<T> set1, Set<T> set2)
	{
		if(set1.size() == 0 || set2.size() == 0)
			return set2;
		
		for(T value : set1)
		{
			set2.remove(value);
		}
		return set2;
	}
	
	/**
	 * Computes the symmetric difference of two sets. A new {@link HashSet} will
	 * be created and filled with the result of the symmetric difference.
	 * 
	 * @param set1 The first set.
	 * @param set2 The second set.
	 * @return The result of the symmetric difference.
	 */
	public static <T> Set<T> symmetricDifference(Set<T> set1, Set<T> set2)
	{
		if(set1.size() == 0 || set2.size() == 0)
			return set1;
		
		Set<T> set = new HashSet<>();

		for(T value : set1)
		{
			if(set2.contains(value) == false)
				set.add(value);
		}
		for(T value : set2)
		{
			if(set1.contains(value) == false)
				set.add(value);
		}
		return set;
	}
}
