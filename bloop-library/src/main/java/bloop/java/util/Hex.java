/*  
 * The Bloop Library.
 * Copyright (c) 2014 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.util;

import java.nio.charset.Charset;

/**
 * Conversion functions for hexadecimal strings.<br>
 * For example, the byte array {@code [50, 110, 0, -10]} would be converted to
 * the hexadecimal string {@code 326E00F6}.
 * 
 * @author Benjamin Billet
 * @version 0.1
 */
public final class Hex
{
	private Hex() { }

	private static final char[] HexDigits = 
		{ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

    /**
	 * Decodes an array of hexadecimal characters into an array of bytes. Each
	 * consecutive pair of characters c1, c2 is used to build a byte where c1
	 * represents the 4 most significant bits and c2 represents the least
	 * significant bits.<br>
	 * Consequently, the decoded byte array is half the length of the input
	 * character array.<br>
	 * Example: {@code ['1', 'a', 'f', '2'] => [26, 242]}
	 * 
	 * @param input An array of hexadecimal characters
	 * @return The decoded byte array
	 * @throws IllegalArgumentException If {@code data.length} is odd or if
	 *             {@code input} contains an illegal character.
	 */
	public static byte[] decodeHex(char[] input)
	{
		int len = input.length;
		if (len % 2 == 1)
			throw new IllegalArgumentException("odd number of chars");

		byte[] out = new byte[len >> 1]; // len / 2

		// 1 byte b = 2 hex characters h1, h2
		for (int i = 0; i < len; i += 2)
		{
			// h1: the 4 most significant bits of b
			int val = toHex(input[i], i) << 4;
			// h2: the 4 least significant bits of b
			val = val | toHex(input[i + 1], i + 1);
			// out[i / 2] = b
			out[i >> 1] = (byte) (val & 0xFF);
		}
		
		return out;
	}

	/**
	 * Encodes an array of bytes into an array of hexadecimal characters
	 * corresponding to the values of each input byte. Each byte is converted
	 * into a pair of hexadecimal characters c1, c2 corresponding respectively
	 * to the 4 most significant bits and the 4 least significant bits of the
	 * byte.<br>
	 * Consequently, the encoded char array is twice the length of the input
	 * byte array. <br>
	 * Example: {@code [26, 242] => ['1', 'a', 'f', '2']}
	 * 
	 * @param input An array of bytes
	 * @return The encoded char array
	 */
	public static char[] encodeHex(byte[] input)
	{
		int len = input.length;
		char[] out = new char[len << 1]; // len * 2
		
		// 1 byte b = 2 hex characters h1, h2
		for (int i = 0; i < len; i++)
		{
			// h1: the 4 most significant bits of b
			out[i * 2] = HexDigits[(0xF0 & input[i]) >>> 4];
			// h2: the 4 least significant bits of b
			out[i * 2 + 1] = HexDigits[0x0F & input[i]];
		}
		return out;
	}

    /**
	 * Decodes a string of hexadecimal characters into an array of bytes. Each
	 * consecutive pair of characters c1, c2 is used to build a byte where c1
	 * represents the 4 most significant bits and c2 represents the least
	 * significant bits.<br>
	 * Consequently, the decoded byte array is half the length of the input
	 * character array.
	 * 
	 * @param input A string hexadecimal characters.
	 * @return The decoded byte array.
	 * @throws IllegalArgumentException If {@code input.size()} is odd or if
	 *             {@code input} contains an illegal character.
	 */
	public static byte[] decodeHexString(String input)
	{
		return decodeHex(input.toCharArray());
	}
	
    /**
	 * Encodes an array of bytes into a string of hexadecimal characters
	 * corresponding to the values of each input byte. Each byte is converted
	 * into a pair of hexadecimal characters c1, c2 corresponding respectively
	 * to the 4 most and the 4 least significant bits of the byte.<br>
	 * Consequently, the encoded char array is twice the length of the input
	 * byte array.<br>
	 * Example: {@code "1af2" => [26, 242]}
	 * 
	 * @param input An array of bytes.
	 * @return The decoded string.
	 */
	public static String encodeHexString(byte[] input)
	{
		return new String(encodeHex(input));
	}

	/**
	 * Decodes an array of hexadecimal character bytes into an array of bytes.
	 * Each consecutive pair of character bytes b1, b2 is used to build a byte
	 * where b1 represents the 4 most significant bits and b2 represents the
	 * least significant bits.<br>
	 * Consequently, the decoded byte array is half the length of the input
	 * character array.<br>
	 * Example (for ASCII-compatible charset):
	 * {@code [49, 97, 102, 50] ("1af2") => [26, 242]}
	 * 
	 * @param input An array of hexadecimal character bytes.
	 * @param charset The {@link Charset} used to decode {@code input} into characters.
	 * @return The decoded byte array.
	 * @throws IllegalArgumentException If {@code input.size()} is odd or if
	 *             {@code input} contains an illegal character.
	 */
	public byte[] decode(byte[] input, Charset charset)
	{
		return decodeHex(new String(input, charset).toCharArray());
	}

	/**
	 * Encodes an array of bytes into an array of hexadecimal character bytes
	 * corresponding to the values of each input byte. Each byte is converted
	 * into a pair of hexadecimal characters c1, c2 corresponding respectively
	 * to the 4 most and the 4 least significant bits of the byte.<br>
	 * Consequently, the encoded char array is twice the length of the input
	 * byte array.<br>
	 * Example (for ASCII-compatible charset):
	 * {@code [26, 242] => [49, 97, 102, 50] ("1af2")}
	 * 
	 * @param input An array of bytes.
	 * @param charset The charset used to encode the resulting hexadecimal
	 *            characters into bytes.
	 * @return The hexadecimal character bytes.
	 */
	public byte[] encode(byte[] input, Charset charset)
	{
		return encodeHexString(input).getBytes(charset);
	}
	
	private static int toHex(char c, int index) 
	{
		int digit = Character.digit(c, 16);
		if (digit == -1)
			throw new IllegalArgumentException("illegal hex digit " + c + " at index " + index);
		return digit;
	}
}
