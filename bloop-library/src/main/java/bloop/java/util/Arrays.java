/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.util;

import java.lang.reflect.Array;

/**
 * Array management functions (merge, length, etc.).
 * 
 * @author Benjamin Billet
 * @version 0.1
 */
public final class Arrays
{
	private Arrays() { }
	
	/**
	 * Merges a sequence of arrays into a single one.
	 */
	public static byte[] merge(byte[]... arrays)
	{
		if(arrays.length == 0)
			return new byte[0];
		
		byte[] newArray = new byte[length(arrays)];

		int currentLen = 0;
		for(int i = 0; i < arrays.length; i++)
		{
			System.arraycopy(arrays[i], 0, newArray, currentLen, arrays[i].length);
			currentLen += arrays[i].length;
		}
		return newArray;
	}
	
	/**
	 * Returns the total length of a sequence of arrays.
	 */
	public static int length(byte[]... arrays)
	{
		int length = 0;
		for(int i = 0; i < arrays.length; i++)
		{
			length += arrays[i].length;
		}
		return length;
	}
	
	/**
	 * Merges a sequence of arrays into a single one.
	 */
	public static short[] merge(short[]... arrays)
	{
		if(arrays.length == 0)
			return new short[0];
		
		short[] newArray = new short[length(arrays)];
		
		int currentLen = 0;
		for(int i = 0; i < arrays.length; i++)
		{
			System.arraycopy(arrays[i], 0, newArray, currentLen, arrays[i].length);
			currentLen += arrays[i].length;
		}
		return newArray;
	}
	
	/**
	 * Returns the total length of a sequence of arrays, by summing up the
	 * length of each array.
	 */
	public static int length(short[]... arrays)
	{
		int length = 0;
		for(int i = 0; i < arrays.length; i++)
		{
			length += arrays[i].length;
		}
		return length;
	}
	
	/**
	 * Merges a sequence of arrays into a single one.
	 */
	public static int[] merge(int[]... arrays)
	{
		if(arrays.length == 0)
			return new int[0];
		
		int[] newArray = new int[length(arrays)];
		
		int currentLen = 0;
		for(int i = 0; i < arrays.length; i++)
		{
			System.arraycopy(arrays[i], 0, newArray, currentLen, arrays[i].length);
			currentLen += arrays[i].length;
		}
		return newArray;
	}
	
	/**
	 * Returns the total length of a sequence of arrays, by summing up the
	 * length of each array.
	 */
	public static int length(int[]... arrays)
	{
		int length = 0;
		for(int i = 0; i < arrays.length; i++)
		{
			length += arrays[i].length;
		}
		return length;
	}
	
	/**
	 * Merges a sequence of arrays into a single one.
	 */
	public static long[] merge(long[]... arrays)
	{
		if(arrays.length == 0)
			return new long[0];
		
		long[] newArray = new long[length(arrays)];
		
		int currentLen = 0;
		for(int i = 0; i < arrays.length; i++)
		{
			System.arraycopy(arrays[i], 0, newArray, currentLen, arrays[i].length);
			currentLen += arrays[i].length;
		}
		return newArray;
	}
	
	/**
	 * Returns the total length of a sequence of arrays, by summing up the
	 * length of each array.
	 */
	public static int length(long[]... arrays)
	{
		int length = 0;
		for(int i = 0; i < arrays.length; i++)
		{
			length += arrays[i].length;
		}
		return length;
	}
	
	/**
	 * Merges a sequence of arrays into a single one.
	 */
	public static float[] merge(float[]... arrays)
	{
		if(arrays.length == 0)
			return new float[0];
		
		float[] newArray = new float[length(arrays)];
		
		int currentLen = 0;
		for(int i = 0; i < arrays.length; i++)
		{
			System.arraycopy(arrays[i], 0, newArray, currentLen, arrays[i].length);
			currentLen += arrays[i].length;
		}
		return newArray;
	}
	
	/**
	 * Returns the total length of a sequence of arrays, by summing up the
	 * length of each array.
	 */
	public static int length(float[]... arrays)
	{
		int length = 0;
		for(int i = 0; i < arrays.length; i++)
		{
			length += arrays[i].length;
		}
		return length;
	}
	
	/**
	 * Merges a sequence of arrays into a single one.
	 */
	public static double[] merge(double[]... arrays)
	{
		if(arrays.length == 0)
			return new double[0];
		
		double[] newArray = new double[length(arrays)];
		
		int currentLen = 0;
		for(int i = 0; i < arrays.length; i++)
		{
			System.arraycopy(arrays[i], 0, newArray, currentLen, arrays[i].length);
			currentLen += arrays[i].length;
		}
		return newArray;
	}
	
	/**
	 * Returns the total length of a sequence of arrays, by summing up the
	 * length of each array.
	 */
	public static int length(double[]... arrays)
	{
		int length = 0;
		for(int i = 0; i < arrays.length; i++)
		{
			length += arrays[i].length;
		}
		return length;
	}
	
	/**
	 * Merges a sequence of arrays into a single one.
	 */
	public static boolean[] merge(boolean[]... arrays)
	{
		if(arrays.length == 0)
			return new boolean[0];
		
		boolean[] newArray = new boolean[length(arrays)];
		
		int currentLen = 0;
		for(int i = 0; i < arrays.length; i++)
		{
			System.arraycopy(arrays[i], 0, newArray, currentLen, arrays[i].length);
			currentLen += arrays[i].length;
		}
		return newArray;
	}
	
	/**
	 * Returns the total length of a sequence of arrays, by summing up the
	 * length of each array.
	 */
	public static int length(boolean[]... arrays)
	{
		int length = 0;
		for(int i = 0; i < arrays.length; i++)
		{
			length += arrays[i].length;
		}
		return length;
	}
	
	/**
	 * Merges a sequence of arrays into a single one.
	 */
	public static char[] merge(char[]... arrays)
	{
		if(arrays.length == 0)
			return new char[0];
		
		char[] newArray = new char[length(arrays)];
		
		int currentLen = 0;
		for(int i = 0; i < arrays.length; i++)
		{
			System.arraycopy(arrays[i], 0, newArray, currentLen, arrays[i].length);
			currentLen += arrays[i].length;
		}
		return newArray;
	}
	
	/**
	 * Returns the total length of a sequence of arrays, by summing up the
	 * length of each array.
	 */
	public static int length(char[]... arrays)
	{
		int length = 0;
		for(int i = 0; i < arrays.length; i++)
		{
			length += arrays[i].length;
		}
		return length;
	}
	
	/**
	 * Merges a sequence of arrays into a single one.
	 * @param <T> the type of elements contained in the arrays.
	 */
	@SafeVarargs
	@SuppressWarnings("unchecked")
	public static <T> T[] merge(T[]... arrays)
	{
		// 'arrays' is an array of arrays of T
		Class<?> itemType = arrays.getClass().getComponentType().getComponentType();
		
		if(arrays.length == 0)
			return (T[]) Array.newInstance(itemType, 0);
		
		T[] newArray = (T[]) Array.newInstance(itemType, length(arrays));
		
		int currentLen = 0;
		for(int i = 0; i < arrays.length; i++)
		{
			System.arraycopy(arrays[i], 0, newArray, currentLen, arrays[i].length);
			currentLen += arrays[i].length;
		}
		return newArray;
	}
	
	/**
	 * Returns the total length of a sequence of arrays, by summing up the
	 * length of each array.
	 * @param <T> the type of elements contained in the arrays.
	 */
	@SafeVarargs
	public static <T> int length(T[]... arrays)
	{
		int length = 0;
		for(int i = 0; i < arrays.length; i++)
		{
			length += arrays[i].length;
		}
		return length;
	}
}
