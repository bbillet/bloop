/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.util.concurrent;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Helper functions for {@link Executor} and {@link ExecutorService}.
 * @author Benjamin Billet
 * @version 0.1
 */
public final class Executors
{
	private Executors() { }
	
    /**
	 * A fixed thread pool which uses a number of thread equals to the number of
	 * CPU cores.
	 * @return The thread pool.
	 * @see java.util.concurrent.Executors#newFixedThreadPool()
	 */
	public static ExecutorService newCoreThreadPool()
	{
		int cores = Runtime.getRuntime().availableProcessors();
		return new ThreadPoolExecutor(cores, cores, 
				0L, TimeUnit.MILLISECONDS,
				new LinkedBlockingQueue<Runnable>());
	}

	/**
	 * A fixed thread pool which uses a number of thread equals to the number of
	 * CPU cores.
	 * @param threadFactory A factory for creating new threads.
	 * @return The thread pool.
	 * @see java.util.concurrent.Executors#newFixedThreadPool()
	 */
	public static ExecutorService newCoreThreadPool(ThreadFactory threadFactory)
	{
		int cores = Runtime.getRuntime().availableProcessors();
		return new ThreadPoolExecutor(cores, cores, 
				0L, TimeUnit.MILLISECONDS,
				new LinkedBlockingQueue<Runnable>(), 
				threadFactory);
	}
}
