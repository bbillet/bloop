/*  
 * The Bloop Library.
 * Copyright (c) 2014 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * A set of helper functions for dealing with collections of primitive types.
 * 
 * @author Benjamin Billet
 * @version 0.1
 */
public final class PrimitiveCollections
{
	private PrimitiveCollections() { }

	/**
	 * Creates a new array of primitive bytes from a collection of boxed bytes.
	 * @param collection the Byte collection.
	 * @return The byte array.
	 */
	public static byte[] toByteArray(Collection<Byte> collection)
	{
		Iterator<Byte> it = collection.iterator();
		byte[] array = new byte[collection.size()];
		for(int i = 0; i < array.length; i++)
		{
			array[i] = it.next();
		}
		return array;
	}
	
	/**
	 * Creates a new array of primitive shorts from a collection of boxed
	 * shorts.
	 * @param collection the Short collection.
	 * @return The short array.
	 */
	public static short[] toShortArray(Collection<Short> collection)
	{
		Iterator<Short> it = collection.iterator();
		short[] array = new short[collection.size()];
		for(int i = 0; i < array.length; i++)
		{
			array[i] = it.next();
		}
		return array;
	}
	
	/**
	 * Creates a new array of primitive ints from a collection of boxed ints.
	 * @param collection the Integer collection.
	 * @return The int array.
	 */
	public static int[] toIntegerArray(Collection<Integer> collection)
	{
		Iterator<Integer> it = collection.iterator();
		int[] array = new int[collection.size()];
		for(int i = 0; i < array.length; i++)
		{
			array[i] = it.next();
		}
		return array;
	}
	
	/**
	 * Creates a new array of primitive longs from a collection of boxed longs.
	 * @param collection the Long collection.
	 * @return The long array.
	 */
	public static long[] toLongArray(Collection<Long> collection)
	{
		Iterator<Long> it = collection.iterator();
		long[] array = new long[collection.size()];
		for(int i = 0; i < array.length; i++)
		{
			array[i] = it.next();
		}
		return array;
	}
	
	/**
	 * Creates a new array of primitive floats from a collection of boxed
	 * floats.
	 * @param collection the Float collection.
	 * @return The float array.
	 */
	public static float[] toFloatArray(Collection<Float> collection)
	{
		Iterator<Float> it = collection.iterator();
		float[] array = new float[collection.size()];
		for(int i = 0; i < array.length; i++)
		{
			array[i] = it.next();
		}
		return array;
	}
	
	/**
	 * Creates a new array of primitive doubles from a collection of boxed
	 * doubles.
	 * @param collection the Double collection.
	 * @return The double array.
	 */
	public static double[] toDoubleArray(Collection<Double> collection)
	{
		Iterator<Double> it = collection.iterator();
		double[] array = new double[collection.size()];
		for(int i = 0; i < array.length; i++)
		{
			array[i] = it.next();
		}
		return array;
	}
	
	/**
	 * Creates a new array of primitive booleans from a collection of boxed
	 * booleans.
	 * @param collection the Boolean collection.
	 * @return The boolean array.
	 */
	public static boolean[] toBooleanArray(Collection<Boolean> collection)
	{
		Iterator<Boolean> it = collection.iterator();
		boolean[] array = new boolean[collection.size()];
		for(int i = 0; i < array.length; i++)
		{
			array[i] = it.next();
		}
		return array;
	}
	
	/**
	 * Creates a new array of primitive chars from a collection of boxed chars.
	 * @param collection the Character collection.
	 * @return The char array.
	 */
	public static char[] toCharacterArray(Collection<Character> collection)
	{
		Iterator<Character> it = collection.iterator();
		char[] array = new char[collection.size()];
		for(int i = 0; i < array.length; i++)
		{
			array[i] = it.next();
		}
		return array;
	}
	
	/**
	 * Creates a new {@link ArrayList} of boxed bytes from an array of primitive
	 * bytes.
	 * @param array The byte array.
	 * @return The Byte list.
	 */
	public static ArrayList<Byte> toArrayList(byte[] array)
	{
		ArrayList<Byte> list = new ArrayList<Byte>(array.length);
		for(int i = 0; i < array.length; i++)
		{
			list.add(array[i]);
		}
		return list;
	}
	
	/**
	 * Creates a new {@link ArrayList} of boxed shorts from an array of
	 * primitive shorts.
	 * @param array The short array.
	 * @return The Short list.
	 */
	public static ArrayList<Short> toArrayList(short[] array)
	{
		ArrayList<Short> list = new ArrayList<Short>(array.length);
		for(int i = 0; i < array.length; i++)
		{
			list.add(array[i]);
		}
		return list;
	}
	
	/**
	 * Creates a new {@link ArrayList} of boxed ints from an array of primitive
	 * ints.
	 * @param array The int array.
	 * @return The Integer list.
	 */
	public static ArrayList<Integer> toArrayList(int[] array)
	{
		ArrayList<Integer> list = new ArrayList<Integer>(array.length);
		for(int i = 0; i < array.length; i++)
		{
			list.add(array[i]);
		}
		return list;
	}
	
	/**
	 * Creates a new {@link ArrayList} of boxed longs from an array of primitive
	 * longs.
	 * @param array The long array.
	 * @return The Long list.
	 */
	public static ArrayList<Long> toArrayList(long[] array)
	{
		ArrayList<Long> list = new ArrayList<Long>(array.length);
		for(int i = 0; i < array.length; i++)
		{
			list.add(array[i]);
		}
		return list;
	}
	
	/**
	 * Creates a new {@link ArrayList} of boxed floats from an array of
	 * primitive floats.
	 * @param array The float array.
	 * @return The Float list.
	 */
	public static ArrayList<Float> toArrayList(float[] array)
	{
		ArrayList<Float> list = new ArrayList<Float>(array.length);
		for(int i = 0; i < array.length; i++)
		{
			list.add(array[i]);
		}
		return list;
	}
	
	/**
	 * Creates a new {@link ArrayList} of boxed doubles from an array of
	 * primitive doubles.
	 * @param array The double array.
	 * @return The Double list.
	 */
	public static ArrayList<Double> toArrayList(double[] array)
	{
		ArrayList<Double> list = new ArrayList<Double>(array.length);
		for(int i = 0; i < array.length; i++)
		{
			list.add(array[i]);
		}
		return list;
	}
	
	/**
	 * Creates a new {@link ArrayList} of boxed booleans from an array of
	 * primitive booleans.
	 * @param array The boolean array.
	 * @return The Boolean list.
	 */
	public static ArrayList<Boolean> toArrayList(boolean[] array)
	{
		ArrayList<Boolean> list = new ArrayList<Boolean>(array.length);
		for(int i = 0; i < array.length; i++)
		{
			list.add(array[i]);
		}
		return list;
	}
	
	/**
	 * Creates a new {@link ArrayList} of boxed chars from an array of primitive
	 * chars.
	 * @param array The char array.
	 * @return The Character list.
	 */
	public static ArrayList<Character> toArrayList(char[] array)
	{
		ArrayList<Character> list = new ArrayList<Character>(array.length);
		for(int i = 0; i < array.length; i++)
		{
			list.add(array[i]);
		}
		return list;
	}	
}
