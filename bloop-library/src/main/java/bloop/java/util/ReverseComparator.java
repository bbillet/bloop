/*  
 * The Bloop Library.
 * Copyright (c) 2015 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.util;

import java.util.Comparator;

/**
 * A comparator which reverse the results of an existing comparator.
 * <pre>{@code
 * Comparator<String> aComparator = ...;
 * Comparator<String> aReverseComparator = ReverseComparator.reverse(aComparator);
 * }</pre>
 *
 * @author Benjamin Billet
 * @version 0.1
 * 
 * @param <T> The type of elements compared by this comparator.
 */
public class ReverseComparator<T> implements Comparator<T>
{
	private Comparator<T> original;
	
	/**
	 * Creates a new {@link ReverseComparator} for an existing comparator.
	 */
	public ReverseComparator(Comparator<T> original) 
	{
		this.original = original;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int compare(T value1, T value2)
	{
		return -1 * original.compare(value1, value2);
	}
	
	/**
	 * Returns a new {@link ReverseComparator} for an existing comparator.
	 */
	public static <T> Comparator<T> reverse(Comparator<T> original)
	{
		return new ReverseComparator<>(original);
	}
}
