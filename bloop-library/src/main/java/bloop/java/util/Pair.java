/*  
 * The Bloop Library.
 * Copyright (c) 2012 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.util;

/**
 * A simple (x, y) pair class.
 * 
 * @author Benjamin Billet
 * @param <T1> The type of the first element.
 * @param <T2> The type of the second element.
 * @version 0.1
 */
public class Pair<T1, T2>
{
	private T1 firstValue;
	private T2 secondValue;
	
	/**
	 * Creates a new empty pair.
	 */
	public Pair()
	{
	}
	
	/**
	 * Creates a new pair.
	 * @param firstValue The first value.
	 * @param secondValue The seconde value.
	 */
	public Pair(T1 firstValue, T2 secondValue)
	{
		this.firstValue = firstValue;
		this.secondValue = secondValue;
	}
	
	/**
	 * Creates a new pair from an existing pair. The values are copied by
	 * reference.
	 * @param pair The source pair.
	 */
	public Pair(Pair<T1, T2> pair)
	{
		this.firstValue = pair.firstValue;
		this.secondValue = pair.secondValue;
	}

	/**
	 * Returns the first value of this pair.
	 */
	public T1 getFirstValue()
	{
		return firstValue;
	}

	/**
	 * Sets the first value of this pair.
	 * @param firstValue The value.
	 */
	public void setFirstValue(T1 firstValue)
	{
		this.firstValue = firstValue;
	}

	/**
	 * Returns the second value of this pair.
	 */
	public T2 getSecondValue()
	{
		return secondValue;
	}

	/**
	 * Sets the second value of this pair.
	 * @param secondValue The value.
	 */
	public void setSecondValue(T2 secondValue)
	{
		this.secondValue = secondValue;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj)
	{
		if(obj == this)
			return true;
		if(obj instanceof Pair)
		{
			Pair<?, ?> pair = (Pair<?, ?>) obj;
			if(pair.firstValue.equals(firstValue) && pair.secondValue.equals(secondValue))
				return true;
		}
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firstValue == null) ? 0 : firstValue.hashCode());
		result = prime * result + ((secondValue == null) ? 0 : secondValue.hashCode());
		return result;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString()
	{
		return "<" + firstValue + ", " + secondValue + ">";
	}
	
	/**
	 * Creates a new pair where the two elements are of the same type. Has the
	 * same effect as creating a {@code Pair<T, T>}.
	 * @param firstValue The first value.
	 * @param secondValue The second value.
	 * @return The uniform pair.
	 * @param <T> The type of the first and the second values.
	 */
	public static <T> Pair<T, T> uniformPair(T firstValue, T secondValue)
	{
		return new Pair<T, T>(firstValue, secondValue);
	}
	
	/**
	 * Creates a new unmodifiable pair from an existing one. The new pair will
	 * throw {@link UnsupportedOperationException} when
	 * {@link Pair#setFirstValue(Object)} or {@link Pair#setSecondValue(Object)}
	 * are invoked.
	 * @param pair The original pair.
	 * @return The unmodifiable pair.
	 * @param <T1> The type of the first value.
	 * @param <T2> The type of the second value.
	 */
	public static <T1, T2> Pair<T1, T2> unmodifiablePair(Pair<T1, T2> pair)
	{
		return new UnmodifiablePair<T1, T2>(pair);
	}
	
	private static class UnmodifiablePair<T1, T2> extends Pair<T1, T2>
	{
		private UnmodifiablePair(Pair<T1, T2> pair)
		{
			super(pair);
		}
		
		@Override
		public void setFirstValue(T1 value1)
		{
			throw new UnsupportedOperationException("unmodifiable pair");
		}

		@Override
		public void setSecondValue(T2 value2)
		{
			throw new UnsupportedOperationException("unmodifiable pair");
		}
	}
}
