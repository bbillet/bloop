/*  
 * The Bloop Library.
 * Copyright (c) 2015 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.math;

/**
 * Methods for computing with IEEE754 floating-point numbers.
 * @author Benjamin Billet
 * @version 0.1
 */
public class FloatingMath
{
	/**
	 * The smallest float value x such that 1+x does not equal 1, assuming 32
	 * bits floats (two-complement).
	 */
	public static final float FLT_EPSILON = 1.19209290e-07f;

	/**
	 * The smallest double value x such that 1+x does not equal 1, assuming 64
	 * bits doubles (two-complement).
	 */
	public static final double DBL_EPSILON = 2.2204460492503131e-16d;
}
