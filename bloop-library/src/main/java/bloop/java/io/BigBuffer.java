/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.io;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

/**
 * An {@link OutputStream} that can store a huge amount of data by switching
 * automatically from memory buffering to file buffering. <br>
 * The lifecycle of the buffer can be described as following:
 * <ol>
 * <li>First, data written into this buffer is stored in memory using a
 * {@link ByteArrayOutputStream}.
 * <li>When the {@link ByteArrayOutputStream} reaches a given size in bytes
 * (user-defined threshold), a temporary file is created and filled with the
 * content of the {@link ByteArrayOutputStream}.
 * <li>From this point, every byte written into this buffer is stored in the
 * temporary file.
 * </ol>
 * <p>
 * The content of this buffer can be retrieved using the {@link InputStream}
 * returned by {@link BigBuffer#getInputStream()}. Depending on the current
 * state of this buffer (memory or file) a {@link ByteArrayInputStream} or a
 * {@link FileInputStream} is returned.<br>
 * <b>Warning</b>: This class is not thread-safe. Concurrent reads and writes
 * may lead to undefined behavior.
 * 
 * @author Benjamin Billet
 * @version 0.1
 */
public class BigBuffer extends OutputStream
{
	private final int threshold;
	private File outputFile;

	private DirectAccessByteArrayOutputStream memoryOutput = null;
	private OutputStream currentOutput = null;
	
	/**
	 * Creates a new {@code BigBuffer} with a size {@code threshold} in bytes.
	 * @param threshold When this buffer reaches this size, it automatically
	 *        switches to file buffering.
	 */
	public BigBuffer(int threshold)
	{
		this.threshold = threshold;		
		this.memoryOutput = new DirectAccessByteArrayOutputStream();
		this.currentOutput = this.memoryOutput;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(int b) throws IOException
	{
		update(1);
		currentOutput.write(b);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(byte[] bytes) throws IOException
	{
		write(bytes, 0, bytes.length);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(byte[] bytes, int offset, int length) throws IOException
	{
		update(length);
		currentOutput.write(bytes, offset, length);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException
	{
		clear();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException
	{
		currentOutput.flush();
	}

	/**
	 * Clears this buffer. If currently buffering into a file, the file is
	 * deleted.
	 * @throws IOException If an I/O error occurs.
	 */
	public void clear() throws IOException
	{
		try
		{
			currentOutput.close();
		}
		finally
		{
			if(memoryOutput == null)
				memoryOutput = new DirectAccessByteArrayOutputStream();
			else
				memoryOutput.reset();
			
			currentOutput = memoryOutput;
			if(outputFile != null && outputFile.exists())
			{
				File tmp = outputFile;
		        if (tmp.delete() == false)
		            throw new IOException("could not delete the temporary file: " + outputFile);
		        outputFile = null;
			}
		}
	}
	
	/**
	 * Returns an {@link InputStream} for reading the content of this buffer.
	 * Concurrently reading the returned {@link InputStream} and writing into
	 * this buffer may lead to undefined behavior.
	 * @return An input stream for reading bytes from this buffer.
	 * @throws IOException If an I/O error occurs.
	 */
	public InputStream getInputStream() throws IOException
	{
		if (outputFile != null)
			return new FileInputStream(outputFile);
		else
			return new ByteArrayInputStream(memoryOutput.getBuffer(), 0, memoryOutput.getCount());
	}
	
	/**
	 * Returns the threshold (in bytes). When this buffer reaches this size, it
	 * automatically switches to file buffering.
	 * @return The threshold (in bytes).
	 */
	public int getThreshold()
	{
		return threshold;
	}

	/**
	 * Returns the temporary file used for buffering, or {@code null} if this
	 * buffer is buffering into memory.
	 * @return The temporary file, or {@code null}.
	 */
	public File getOutputFile()
	{
		return outputFile;
	}

	private void update(int length) throws IOException
	{
		if (outputFile == null && memoryOutput.getCount() + length > threshold)
		{
			outputFile = File.createTempFile(UUID.randomUUID().toString(), null);
			outputFile.deleteOnExit();
			
			currentOutput = new BufferedOutputStream(new FileOutputStream(outputFile));
			memoryOutput.writeTo(currentOutput);
			currentOutput.flush();
			
			memoryOutput = null;
		}
	}
	
	private static final class DirectAccessByteArrayOutputStream extends ByteArrayOutputStream
	{
		private byte[] getBuffer()
		{
			return buf;
		}

		private int getCount()
		{
			return count;
		}
	}
}
