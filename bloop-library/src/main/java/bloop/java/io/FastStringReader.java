/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.io;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

/**
 * A simple character stream based on a String. This class has exactly the same
 * behavior as {@link StringReader} but is not thread-safe (faster).
 * 
 * @author Benjamin Billet
 * @see Reader
 * @see StringReader
 * @version 0.1
 */
public class FastStringReader extends Reader
{
	private String internal;
	private int internalLength;
	private int next = 0;
	private int mark = 0;

	/**
	 * Constructs a new {@code FastStringReader}.
	 * @param internal The string to be used as character stream.
	 */
	public FastStringReader(String internal)
	{
		this.internal = internal;
		this.internalLength = internal.length();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read() throws IOException
	{
		if (next >= internalLength)
			return -1;
		
		return internal.charAt(next++);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read(char chars[], int offset, int length) throws IOException
	{
		if (length < 0
				|| offset < 0 
				|| offset > chars.length 
				|| offset + length > chars.length
				|| offset + length < 0)
		{
			throw new IndexOutOfBoundsException();
		}
		else if (length == 0)
			return 0;
		else if (next >= internalLength)
			return -1;
		
		int nb = internalLength - next;
		if(length < nb)
			nb = length;
		
		internal.getChars(next, next + nb, chars, offset);
		next += nb;
		return nb;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long skip(long ns) throws IOException
	{
		if (next >= internalLength)
			return 0;
		
		long nb = internalLength - next;
		if(ns < nb)
			nb = ns;
		if(next < nb)
			nb = -next;

		next += nb;
		return nb;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean ready() throws IOException
	{
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean markSupported()
	{
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mark(int readAheadLimit) throws IOException
	{
		if (readAheadLimit < 0)
			throw new IllegalArgumentException("read-ahead limit < 0");
		
		mark = next;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() throws IOException
	{
		next = mark;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close()
	{
	}
}
