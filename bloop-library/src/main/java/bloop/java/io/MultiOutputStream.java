/*  
 * The Bloop Library.
 * Copyright (c) 2014 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bloop.java.io;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * An {@link OutputStream} that writes data into several {@link OutputStream}
 * objects. Basically, each byte written into this stream will be written in
 * each output stream managed by this stream.
 * 
 * @author Benjamin Billet
 * @version 0.1
 */
public class MultiOutputStream extends OutputStream
{
	private List<OutputStream> outputs;

	/**
	 * Creates a new {@code MultiOutputStream} from a set of
	 * {@link OutputStream}.
	 * @param outputs The streams.
	 * @throws IllegalArgumentException If {@code outputs} is empty.
	 */
	public MultiOutputStream(List<OutputStream> outputs)
	{
		if (outputs.size() == 0)
			throw new IllegalArgumentException();
		this.outputs = new ArrayList<OutputStream>(outputs);
	}

	/**
	 * Creates a new {@code MultiOutputStream} from a set of
	 * {@link OutputStream}.
	 * @param outputs The streams.
	 * @throws IllegalArgumentException If {@code outputs} is empty.
	 */
	public MultiOutputStream(OutputStream... outputs)
	{
		this(Arrays.asList(outputs));
	}

	/**
	 * Adds a new {@link OutputStream}.
	 */
	public void add(OutputStream output)
	{
		outputs.add(output);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(int b) throws IOException
	{
		for (OutputStream output : outputs)
		{
			output.write(b);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(byte[] bytes) throws IOException
	{
		for (OutputStream output : outputs)
		{
			output.write(bytes, 0, bytes.length);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(byte[] bytes, int offset, int length) throws IOException
	{
		for (OutputStream output : outputs)
		{
			output.write(bytes, offset, length);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException
	{
		for (OutputStream output : outputs)
		{
			output.flush();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException
	{
		for (OutputStream output : outputs)
		{
			output.close();
		}
	}
}
