/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bloop.java.io;

import java.io.PrintStream;
import java.util.Locale;

/**
 * A {@link TabulatedPrintStream} that enable automatic tabulation after calling
 * one of the {@code println(*)} functions.
 * 
 * @author Benjamin Billet
 * @version 0.1
 */
public class TabulatedPrintStream extends PrintStream
{
	private PrintStream stream;
	private int tabLevel = 0;
	private boolean mustTab = false;

	/**
	 * Creates a new {@link TabulatedPrintStream} that encapsulates an existing
	 * {@link PrintStream}.
	 * @param stream The stream to auto-tabulate.
	 * @param initialTabLevel The initial tabulation level (0 or greater).
	 */
	public TabulatedPrintStream(PrintStream stream, int initialTabLevel)
	{
		super(stream);
		this.stream = stream;
		setTabLevel(initialTabLevel);
	}
	
	/**
	 * Increments the tabulation level.
	 */
	public void incrementTabLevel()
	{
		tabLevel++;
	}
	
	/**
	 * Decrements the tabulation level.
	 */
	public void decrementTabLevel()
	{
		if(tabLevel > 0)
			tabLevel--;
	}
	
	/**
	 * Sets the tabulation level.
	 */
	public void setTabLevel(int tabLevel)
	{
		if(tabLevel >= 0)
			this.tabLevel = tabLevel;
		else
			this.tabLevel = 0;
	}
	
	/**
	 * Gets the current tabulation level.
	 */
	public int getTablLevel()
	{
		return tabLevel;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(int b)
	{
		if(mustTab)
		{
			tab();
			mustTab = false;
		}
		stream.write(b);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(byte[] buf, int off, int len)
	{
		if(mustTab)
		{
			tab();
			mustTab = false;
		}
		stream.write(buf, off, len);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void print(String s)
	{
		if(mustTab)
		{
			tab();
			mustTab = false;
		}
		stream.print(s);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void print(boolean b)
	{
		print(String.valueOf(b));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void print(char c)
	{
		print(String.valueOf(c));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void print(int i)
	{
		print(String.valueOf(i));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void print(long l)
	{
		print(String.valueOf(l));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void print(float f)
	{
		print(String.valueOf(f));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void print(double d)
	{
		print(String.valueOf(d));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void print(char[] s)
	{
		print(new String(s));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void print(Object obj)
	{
		print(String.valueOf(obj));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void println()
	{
		stream.println();
		mustTab = true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void println(String s)
	{
		if(mustTab)
		{
			tab();
			mustTab = false;
		}
		stream.println(s);
		mustTab = true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void println(boolean b)
	{
		println(String.valueOf(b));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void println(char c)
	{
		println(String.valueOf(c));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void println(int i)
	{
		println(String.valueOf(i));
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void println(long l)
	{
		println(String.valueOf(l));
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void println(float f)
	{
		println(String.valueOf(f));
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void println(double d)
	{
		println(String.valueOf(d));
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void println(char[] s)
	{
		println(new String(s));
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void println(Object obj)
	{
		println(String.valueOf(obj));
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public PrintStream printf(String format, Object... args)
	{
		return format(format, args);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PrintStream printf(Locale l, String format, Object... args)
	{
		return format(l, format, args);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PrintStream format(String format, Object... args)
	{
		if(mustTab)
		{
			tab();
			mustTab = false;
		}
		return stream.format(format, args);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PrintStream format(Locale l, String format, Object... args)
	{
		if(mustTab)
		{
			tab();
			mustTab = false;
		}
		return stream.format(l, format, args);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PrintStream append(CharSequence csq)
	{
		if(mustTab)
		{
			tab();
			mustTab = false;
		}
		return stream.append(csq);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PrintStream append(CharSequence csq, int start, int end)
	{
		if(mustTab)
		{
			tab();
			mustTab = false;
		}
		return stream.append(csq, start, end);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PrintStream append(char c)
	{
		if(mustTab)
		{
			tab();
			mustTab = false;
		}
		return stream.append(c);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush()
	{
		stream.flush();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close()
	{
		stream.close();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean checkError()
	{
		return stream.checkError();
	}

	@Override
	protected void setError()
	{
		// unused
	}

	@Override
	protected void clearError()
	{
		// unused
	}
	
	private void tab()
	{
		for(int i = 0; i < tabLevel; i++)
		{
			stream.print("\t");
		}
	}
}
