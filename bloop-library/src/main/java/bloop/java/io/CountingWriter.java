/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.io;

import java.io.FilterWriter;
import java.io.IOException;
import java.io.Writer;

/**
 * A {@link Writer} with char counting capabilities.
 * 
 * @author Benjamin Billet
 * @version 0.1
 */
public final class CountingWriter extends FilterWriter
{
	private long count;

	/**
	 * Creates a new {@code CountingWriter} that wraps an existing
	 * {@link Writer}.
	 */
	public CountingWriter(Writer writer)
	{
		super(writer);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(char[] chars, int offset, int length) throws IOException
	{
		out.write(chars, offset, length);
		count += length;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(String string, int offset, int length) throws IOException
	{
		out.write(string, offset, length);
		count += length;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(int b) throws IOException
	{
		out.write(b);
		count++;
	}
	
	/**
	 * Returns the number of characters written.
	 */
	public long getCount()
	{
		return count;
	}
}
