/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.io;

import java.io.IOException;
import java.io.Writer;

/**
 * A simple character stream that writes into a string builder, which can then
 * be used to construct a string. This class has exactly the same behavior as
 * {@link java.io.StringWriter} but is based on a {@link StringBuilder} instead
 * of a {@link java.lang.StringBuffer} (which is slower, due to
 * synchronization).
 * 
 * @author Benjamin Billet
 * @see Writer
 * @see java.io.StringWriter
 * @version 0.1
 */
public class FastStringWriter extends Writer
{
	private final StringBuilder builder;

	/**
	 * Creates a new {@code FastStringWriter} using the default initial
	 * {@link StringBuilder} size.
	 */
	public FastStringWriter()
	{
		super();
		builder = new StringBuilder();
	}

	/**
	 * Creates a new {@code FastStringWriter} with an initial capacity.
	 * @param capacity The number of characters that will fit into this buffer
	 *            before it is automatically expanded.
	 * @throws NegativeArraySizeException If the {@code capacity} argument is
	 *             less than 0.
	 */
	public FastStringWriter(int capacity)
	{
		super();
		builder = new StringBuilder(capacity);
	}
	
	/**
	 * Constructs a new string writer that wraps an existing
	 * {@link StringBuilder}.
	 */
	public FastStringWriter(StringBuilder builder)
	{
		super();
		this.builder = builder;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
    public void write(int c) throws IOException
    {
		builder.append((char) c);
    }
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(char[] cbuf) throws IOException
	{
		builder.append(cbuf);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(char[] cbuf, int off, int len) throws IOException
	{
		builder.append(cbuf, off, len);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(String str) throws IOException
	{
		builder.append(str);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(String str, int off, int len) throws IOException
	{
		builder.append(str, off, len);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException
	{
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException
	{
	}
	
	/**
	 * Returns a string filled with the content of this buffer.
	 */
	@Override
	public String toString()
	{
		return builder.toString();
	}
}
