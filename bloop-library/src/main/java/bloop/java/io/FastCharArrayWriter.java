/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.io;

import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;

/**
 * A simple {@link Writer} that writes into a character array that automatically
 * grows when full. This class has exactly the same behavior as
 * {@link java.io.CharArrayWriter} but is not thread-safe (faster).
 * 
 * @author Benjamin Billet
 * @see Writer
 * @see java.io.CharArrayWriter
 * @version 0.1
 */
public class FastCharArrayWriter extends Writer
{
	private char[] internal;
	private int count;

	/**
	 * Creates a new {@code CharArrayWriter}.
	 */
	public FastCharArrayWriter()
	{
		this(32);
	}

	/**
	 * Creates a new {@code CharArrayWriter} with the specified initial size.
	 * @param capacity The number of characters that will fit into this stream
	 *            before it is automatically expanded.
	 * @exception IllegalArgumentException If initialSize is negative.
	 */
	public FastCharArrayWriter(int capacity)
	{
		if (capacity < 0)
			throw new IllegalArgumentException("negative capacity");
		internal = new char[capacity];
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(int c)
	{
		int newCount = count + 1;
		reallocateIfNeeded(newCount);
		internal[count] = (char) c;
		count = newCount;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(char chars[], int offset, int length)
	{
		if (offset < 0
				|| offset > chars.length
				|| length < 0
				|| offset + length > chars.length
				|| offset + length < 0)
		{
			throw new IndexOutOfBoundsException();
		}
		else if (length == 0)
			return;

		int newCount = count + length;
		reallocateIfNeeded(newCount);
		System.arraycopy(chars, offset, internal, count, length);
		count = newCount;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(String string, int offset, int length)
	{
		int newCount = count + length;
		reallocateIfNeeded(newCount);
		string.getChars(offset, offset + length, internal, count);
		count = newCount;
	}

	/**
	 * Writes the contents of this buffer into another {@link Writer}.
	 * 
	 * @throws IOException If an I/O error occurs.
	 */
	public void writeTo(Writer output) throws IOException
	{
		output.write(internal, 0, count);
	}

	/**
	 * Resets this buffer so that you can use it again without throwing away the
	 * already allocated buffer.
	 */
	public void reset()
	{
		count = 0;
	}

	/**
	 * Returns a copy of this buffer into a char array.
	 */
	public char[] toCharArray()
	{
		return Arrays.copyOf(internal, count);
	}

	/**
	 * Returns the current size of the buffer.
	 */
	public int size()
	{
		return count;
	}

	/**
	 * Returns a string filled with the chars contained in this buffer.
	 */
	@Override
	public String toString()
	{
		return new String(internal, 0, count);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush()
	{
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close()
	{
	}
	
	private void reallocateIfNeeded(int desiredSize)
	{
		if (desiredSize > internal.length)
			internal = Arrays.copyOf(internal, Math.max(internal.length << 1, desiredSize));
	}
}
