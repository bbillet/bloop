/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bloop.java.io;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Arrays;

/**
 * Various helpers for reading from {@link InputStream} (read exactly a given
 * number of bytes, read until a pattern, read an entire stream etc.) and
 * writing to {@link OutputStream}.
 * 
 * @author Benjamin Billet
 * @version 0.1
 */
public final class IOHelper
{
	private IOHelper() { }

	public final static byte CR = 0x0d;
	public final static byte LF = 0x0a;
	public final static byte[] CRLF = new byte[] { CR, LF };

	public final static byte[] SYSTEM_LINE_SEPARATOR = System.lineSeparator().getBytes();

	/**
	 * Reads {@code buffer.length} bytes. The number of bytes read can be lesser
	 * than expected, iff the end of the stream is reached.
	 * @param input The input stream.
	 * @param output The buffer where the read bytes will be stored.
	 * @return The number of bytes read.
	 * @throws IOException If an I/O error occurs.
	 */
	public static int readExact(InputStream input, byte[] output)
			throws IOException
	{
		int len = input.read(output);
		if (len == -1)
			return -1;
		else if (len == output.length)
			return len;

		int nb = len;
		while (nb < output.length)
		{
			len = input.read(output, nb, output.length - nb);
			if (len == -1)
				break;
			nb = nb + len;
		}

		return nb;
	}

	/**
	 * Reads {@code nb} bytes from the stream and ignores them.
	 * @param input The input stream.
	 * @param nb The number of bytes that must be ignored.
	 * @return The number of bytes actually ignored (lesser than {@code nb} if
	 *         the end of stream was reached).
	 * @throws IOException If an I/O error occurs.
	 */
	public static int ignore(InputStream input, int nb) throws IOException
	{
		for (int i = 0; i < nb; i++)
		{
			if (input.read() == -1)
				return i;
		}
		return nb;
	}

	/**
	 * Reads bytes until a CRLF and converts the result into a string.
	 * @param input The input stream.
	 * @param charset The {@link Charset} for the output string.
	 * @return The line without the CRLF, or {@code null} if nothing can be read
	 *         (end of stream).
	 * @throws IOException If an I/O error occurs.
	 */
	public static String readCRLFLine(InputStream input, Charset charset) throws IOException
	{
		return readUntilPattern(input, CRLF, charset);
	}

	/**
	 * Reads bytes until a CRLF. 
	 * @param input The input stream.
	 * @return The line without the CRLF, or {@code null} if nothing can be read
	 *         (end of stream).
	 * @throws IOException If an I/O error occurs.
	 */
	public static byte[] readCRLFLine(InputStream input) throws IOException
	{
		return readUntilPattern(input, CRLF);
	}

	/**
	 * Reads bytes until a line jump, according to
	 * {@code System.lineSeparator()}, and converts the result into a string.
	 * @param input The input stream.
	 * @param charset The {@link Charset} for the output string.
	 * @return The line without the line separator, or {@code null} if nothing
	 *         can be read (end of stream).
	 * @throws IOException If an I/O error occurs.
	 */
	public static String readLine(InputStream input, Charset charset) throws IOException
	{
		return readUntilPattern(input, SYSTEM_LINE_SEPARATOR, charset);
	}

	/**
	 * Reads bytes until a line jump, according to
	 * {@code System.lineSeparator()}.
	 * @param input The input stream.
	 * @return The line without the line separator, or {@code null} if nothing
	 *         can be read (end of stream).
	 * @throws IOException If an I/O error occurs.
	 */
	public static byte[] readLine(InputStream input) throws IOException
	{
		return readUntilPattern(input, SYSTEM_LINE_SEPARATOR);
	}

	/**
	 * Reads bytes until a given pattern is matched.
	 * @param input The input stream.
	 * @param pattern The pattern to look for.
	 * @return The bytes read minus the pattern, or {@code null} if nothing can
	 *         be read (end of stream).
	 * @throws IOException If an I/O error occurs.
	 */
	public static byte[] readUntilPattern(InputStream input, byte[] pattern) throws IOException
	{
		if (pattern == null || pattern.length == 0)
			throw new IllegalArgumentException("pattern is empty");

		int matched = 0;
		boolean[] matchingState = new boolean[pattern.length];

		int b = input.read();
		if (b == -1)
			return null;

		ByteArrayOutputStream output = new ByteArrayOutputStream();
		while (true)
		{
			if (b == pattern[matched])
			{
				matchingState[matched] = true;
				matched++;
				if (matched == pattern.length && isMatched(matchingState))
					break;
			}
			else
			{
				if (matched > 0)
				{
					output.write(pattern, 0, matched);
					matched = 0;
					Arrays.fill(matchingState, true);
				}
				output.write((byte) b);
			}

			b = input.read();
			if (b == -1)
				break;
		}
		return output.toByteArray();
	}

	private static boolean isMatched(boolean[] bools)
	{
		boolean result = bools[0];
		for (int i = 1; i < bools.length; i++)
		{
			result = result && bools[i];
		}
		return result;
	}

	/**
	 * Reads bytes until a given pattern is matched, and converts the result
	 * into a string.
	 * @param input The input stream.
	 * @param pattern The pattern to look for.
	 * @param charset The {@link Charset} for the output string.
	 * @return The string read minus the pattern, or {@code null} if nothing can
	 *         be read (end of stream).
	 * @throws IOException If an I/O error occurs.
	 */
	public static String readUntilPattern(InputStream input, byte[] pattern, Charset charset) throws IOException
	{
		byte[] data = readUntilPattern(input, pattern);
		if (data != null)
			return new String(data, charset);
		else
			return null;
	}

	/**
	 * Writes a string into an output stream.
	 * @param output The output stream.
	 * @param string The string.
	 * @param charset The charset of the string.
	 * @throws IOException If an I/O error occurs.
	 */
	public static void write(OutputStream output, String string, Charset charset)
			throws IOException
	{
		output.write(string.getBytes(charset));
	}

	/**
	 * Reads until the end of the stream.
	 * @param input The input stream.
	 * @param bufferSize The size of the read buffer.
	 * @return The bytes read.
	 * @throws IOException If an I/O error occurs.
	 */
	public static byte[] readAllBytes(InputStream input, int bufferSize)
			throws IOException
	{
		return readAllBytesAsStream(input, bufferSize).toByteArray();
	}

	/**
	 * Reads until the end of the stream.
	 * @param input The input stream.
	 * @param bufferSize The size of the read buffer.
	 * @return A {@link ByteArrayOutputStream} that contains the data read from
	 *         the stream.
	 * @throws IOException If an I/O error occurs.
	 */
	public static ByteArrayOutputStream readAllBytesAsStream(InputStream input, int bufferSize) throws IOException
	{
		byte[] buffer = new byte[bufferSize];
		ByteArrayOutputStream output = new ByteArrayOutputStream();

		int len = -1;
		while ((len = input.read(buffer)) != -1)
		{
			if (len > 0)
				output.write(buffer, 0, len);
		}

		return output;
	}

	/**
	 * Reads until the end of the stream and returns a string.
	 * @param input The input stream.
	 * @param bufferSize The size of the read buffer.
	 * @param charset The charset of the string to be built.
	 * @return The data read from the stream.
	 * @throws IOException If an I/O error occurs.
	 */
	public static String readAll(InputStream input, int bufferSize, Charset charset) throws IOException
	{
		return readAllBytesAsStream(input, bufferSize).toString(charset.name());
	}

	/**
	 * Copies the entire content of an {@link InputStream} into an
	 * {@link OutputStream}.<br>
	 * The input and the output streams ARE NOT CLOSED at the end of the copying
	 * process.
	 * @param input The input stream.
	 * @param output The output stream.
	 * @param bufferSize The size of the read buffer.
	 * @throws IOException If an I/O error occurs.
	 */
	public static void copyAll(InputStream input, OutputStream output,
			int bufferSize) throws IOException
	{
		byte[] buffer = new byte[bufferSize];

		int len = -1;
		while ((len = input.read(buffer)) != -1)
		{
			output.write(buffer, 0, len);
			output.flush();
		}
	}
}
