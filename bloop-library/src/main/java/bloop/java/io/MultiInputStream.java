/*  
 * The Bloop Library.
 * Copyright (c) 2014 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bloop.java.io;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * An {@link InputStream} that reads data from several concatenated
 * {@link InputStream} objects.<br>
 * Basically, this stream will read the first stream, then the second stream and
 * so on until every stream is finished.
 * 
 * @author Benjamin Billet
 * @version 0.1
 */
public class MultiInputStream extends InputStream
{
	private InputStream[] inputs;
	private int current = 0;

	/**
	 * Creates a new {@code MultiInputStream} from a set of {@link InputStream}.
	 * @param inputs The streams.
	 * @throws IllegalArgumentException If {@code inputs} is empty.
	 */
	public MultiInputStream(List<InputStream> inputs)
	{
		this(inputs.toArray(new InputStream[0]));
	}

	/**
	 * Creates a new {@code MultiInputStream} from a set of {@link InputStream}.
	 * @param inputs The streams.
	 * @throws IllegalArgumentException If {@code inputs} is empty.
	 */
	public MultiInputStream(InputStream... inputs)
	{
		if (inputs.length == 0)
			throw new IllegalArgumentException();
		this.inputs = inputs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read() throws IOException
	{
		if (current >= inputs.length)
			return -1;

		int b = inputs[current].read();
		if (b == -1)
		{
			next();
			return read();
		}
		return b;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int available() throws IOException
	{
		if (current >= inputs.length)
			return 0;

		return inputs[current].available();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException
	{
		int oldCurrent = current;
		current = inputs.length; // closed

		IOException exception = null;
		for (int i = oldCurrent; i < inputs.length; i++)
		{
			try
			{
				inputs[i].close();
			}
			catch (IOException e)
			{
				if (exception != null)
					exception.addSuppressed(e);
				else
					exception = e;
			}
		}
	}

	private void next()
	{
		if (current < inputs.length)
			current++;
	}
}
