/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bloop.java.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 * A {@link PrintStream} that writes data into several {@link PrintStream}
 * objects. Basically, anything written into this stream will be written in each
 * {@link PrintStream} managed by this stream.
 * 
 * @author Benjamin Billet
 * @version 0.1
 */
public class MultiPrintStream extends PrintStream
{
	private Set<PrintStream> streams = new HashSet<>();
	private boolean enableSystemOut;

	/**
	 * Creates a new {@link MultiPrintStream} from a set of {@link PrintStream}.
	 * @param enableSystemOut If {@code true}, this {@link MultiPrintStream}
	 *            will also write date into {@code System.out}.
	 * @param streams The streams.
	 */
	public MultiPrintStream(boolean enableSystemOut, PrintStream... streams)
	{
		super(streams[0]);
		this.enableSystemOut = enableSystemOut;
		add(streams);
	}

	/**
	 * Creates a new {@link MultiPrintStream} that will write into a set of
	 * files.
	 * @param enableSystemOut If {@code true}, this {@link MultiPrintStream}
	 *            will also write date into {@code System.out}.
	 * @param outputFiles The output files.
	 */
	public MultiPrintStream(boolean enableSystemOut, String... outputFiles) throws FileNotFoundException
	{
		super(outputFiles[0]);
		this.enableSystemOut = enableSystemOut;
		add(outputFiles);
	}

	/**
	 * Creates a new {@link MultiPrintStream} that will write into a set of
	 * files.
	 * @param enableSystemOut If {@code true}, this {@link MultiPrintStream}
	 *            will also write date into {@code System.out}.
	 * @param outputFiles The output files.
	 */
	public MultiPrintStream(boolean enableSystemOut, File... outputFiles) throws FileNotFoundException
	{
		super(outputFiles[0]);
		this.enableSystemOut = enableSystemOut;
		add(outputFiles);
	}

	/**
	 * Creates a new {@link MultiPrintStream} that will write into a set of
	 * files.
	 * @param enableSystemOut If {@code true}, this {@link MultiPrintStream}
	 *            will also write date into {@code System.out}.
	 * @param outputFiles The output files.
	 */
	public MultiPrintStream(boolean enableSystemOut, Path... outputFiles) throws FileNotFoundException
	{
		super(outputFiles[0].toFile());
		this.enableSystemOut = enableSystemOut;
		add(outputFiles);
	}

	/**
	 * Adds several {@link PrintStream} to this {@link MultiPrintStream}.
	 */
	public void add(PrintStream... streams)
	{
		for (PrintStream stream : streams)
		{
			this.streams.add(stream);
		}
	}

	/**
	 * Adds several files as outputs of this {@link MultiPrintStream}.
	 */
	public void add(String... outputFiles) throws FileNotFoundException
	{
		for (String outputFile : outputFiles)
		{
			this.streams.add(new PrintStream(outputFile));
		}
	}

	/**
	 * Adds several files as outputs of this {@link MultiPrintStream}.
	 */
	public void add(File... outputFiles) throws FileNotFoundException
	{
		for (File outputFile : outputFiles)
		{
			this.streams.add(new PrintStream(outputFile));
		}
	}

	/**
	 * Adds several files as outputs of this {@link MultiPrintStream}.
	 */
	public void add(Path... outputFiles) throws FileNotFoundException
	{
		for (Path outputFile : outputFiles)
		{
			this.streams.add(new PrintStream(outputFile.toFile()));
		}
	}

	/**
	 * Removes a {@link PrintStream} of this {@link MultiPrintStream}.
	 */
	public void remove(PrintStream stream)
	{
		streams.remove(stream);
	}

	/**
	 * Removes all the {@link PrintStream}.
	 */
	public void removeAll()
	{
		streams.clear();
	}

	/**
	 * Enables {@code System.out} for this stream.
	 */
	public void enableSystemOut()
	{
		this.enableSystemOut = true;
	}

	/**
	 * Disables {@code System.out} for this stream.
	 */
	public void disableSystemOut()
	{
		this.enableSystemOut = false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(int b)
	{
		for (PrintStream stream : streams)
		{
			stream.write(b);
		}
		if (enableSystemOut)
			System.out.write(b);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(byte[] buf, int off, int len)
	{
		for (PrintStream stream : streams)
		{
			stream.write(buf, off, len);
		}
		if (enableSystemOut)
			System.out.write(buf, off, len);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void print(String s)
	{
		for (PrintStream stream : streams)
		{
			stream.print(s);
		}
		if (enableSystemOut)
			System.out.print(s);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void print(boolean b)
	{
		print(String.valueOf(b));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void print(char c)
	{
		print(String.valueOf(c));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void print(int i)
	{
		print(String.valueOf(i));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void print(long l)
	{
		print(String.valueOf(l));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void print(float f)
	{
		print(String.valueOf(f));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void print(double d)
	{
		print(String.valueOf(d));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void print(char[] s)
	{
		print(new String(s));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void print(Object obj)
	{
		print(String.valueOf(obj));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void println()
	{
		for (PrintStream stream : streams)
		{
			stream.println();
		}
		if (enableSystemOut)
			System.out.println();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void println(String s)
	{
		for (PrintStream stream : streams)
		{
			stream.println(s);
		}
		if (enableSystemOut)
			System.out.println(s);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void println(boolean b)
	{
		println(String.valueOf(b));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void println(char c)
	{
		println(String.valueOf(c));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void println(int i)
	{
		println(String.valueOf(i));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void println(long l)
	{
		println(String.valueOf(l));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void println(float f)
	{
		println(String.valueOf(f));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void println(double d)
	{
		println(String.valueOf(d));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void println(char[] s)
	{
		println(new String(s));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void println(Object obj)
	{
		println(String.valueOf(obj));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PrintStream printf(String format, Object... args)
	{
		return format(format, args);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PrintStream printf(Locale l, String format, Object... args)
	{
		return format(l, format, args);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PrintStream format(String format, Object... args)
	{
		for (PrintStream stream : streams)
		{
			stream.format(format, args);
		}
		if (enableSystemOut)
			System.out.format(format, args);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PrintStream format(Locale l, String format, Object... args)
	{
		for (PrintStream stream : streams)
		{
			stream.format(l, format, args);
		}
		if (enableSystemOut)
			System.out.format(l, format, args);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PrintStream append(CharSequence csq)
	{
		for (PrintStream stream : streams)
		{
			stream.append(csq);
		}
		if (enableSystemOut)
			System.out.append(csq);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PrintStream append(CharSequence csq, int start, int end)
	{
		for (PrintStream stream : streams)
		{
			stream.append(csq, start, end);
		}
		if (enableSystemOut)
			System.out.append(csq, start, end);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PrintStream append(char c)
	{
		for (PrintStream stream : streams)
		{
			stream.append(c);
		}
		if (enableSystemOut)
			System.out.append(c);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush()
	{
		for (PrintStream stream : streams)
		{
			stream.flush();
		}
		if (enableSystemOut)
			System.out.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close()
	{
		for (PrintStream stream : streams)
		{
			stream.close();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean checkError()
	{
		for (PrintStream stream : streams)
		{
			if (stream.checkError() == true)
				return true;
		}
		return false;
	}

	@Override
	protected void setError()
	{
		// unused
	}

	@Override
	protected void clearError()
	{
		// unused
	}
}
