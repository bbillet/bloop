/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.io;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * An {@link OutputStream} with byte counting capabilities.
 * 
 * @author Benjamin Billet
 * @version 0.1
 */
public final class CountingOutputStream extends FilterOutputStream
{
	private long count;

	/**
	 * Creates a new {@code CountingOutputStream} that wraps an existing
	 * {@link OutputStream}.
	 */
	public CountingOutputStream(OutputStream output)
	{
		super(output);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(byte[] bytes, int offset, int length) throws IOException
	{
		out.write(bytes, offset, length);
		count += length;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(int b) throws IOException
	{
		out.write(b);
		count++;
	}
	
	/**
	 * Returns the number of bytes written.
	 */
	public long getCount()
	{
		return count;
	}
}
