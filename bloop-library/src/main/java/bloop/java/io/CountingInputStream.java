/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.io;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * An {@link InputStream} with byte counting capabilities.
 * 
 * @author Benjamin Billet
 * @version 0.1
 */
public final class CountingInputStream extends FilterInputStream
{
	private long count = 0;
	private long mark = -1;

	/**
	 * Creates a new {@code CountingInputStream} that wraps an existing
	 * {@link InputStream}.
	 */
	public CountingInputStream(InputStream input)
	{
		super(input);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read() throws IOException
	{
		int b = in.read();
		if (b > -1)
			count++;
		return b;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read(byte[] bytes, int offset, int length) throws IOException
	{
		int read = in.read(bytes, offset, length);
		if (read > 0)
			count += read;
		return read;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long skip(long nb) throws IOException
	{
		long skipped = in.skip(nb);
		if (skipped > 0)
			count += skipped;
		return skipped;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void mark(int readlimit)
	{
		if (in.markSupported())
		{
			in.mark(readlimit);
			mark = count;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void reset() throws IOException
	{
		if (in.markSupported() == false)
			throw new IOException("mark is not supported");
		else if (mark == -1)
			throw new IOException("no mark");

		in.reset();
		count = mark;
	}

	/**
	 * Returns the number of bytes read.
	 */
	public long getCount()
	{
		return count;
	}
}
