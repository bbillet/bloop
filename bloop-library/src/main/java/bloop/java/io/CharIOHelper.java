package bloop.java.io;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

/**
 * Various helpers for reading from {@link Reader} and
 * writing to {@link Writer}.
 * 
 * @author Benjamin Billet
 * @version 0.1
 */
public final class CharIOHelper
{
	private CharIOHelper() { }

	/**
	 * Reads until the end of the reader and returns a string.
	 * @param reader The reader.
	 * @param bufferSize The size of the read buffer.
	 * @return The data read from the reader.
	 * @throws IOException If an I/O error occurs.
	 */
	public static String readAll(Reader reader, int bufferSize) throws IOException
	{
		char[] buffer = new char[bufferSize];
		StringBuilder builder = new StringBuilder();

		int len = -1;
		while ((len = reader.read(buffer)) != -1)
		{
			if (len > 0)
				builder.append(buffer, 0, len);
		}

		return builder.toString();
	}
	
	/**
	 * Copies the entire content of an {@link Reader} into an
	 * {@link Writer}.<br>
	 * The reader and the writer ARE NOT CLOSED at the end of the copying
	 * process.
	 * @param reader The reader.
	 * @param writer The writer.
	 * @param bufferSize The size of the read buffer.
	 * @throws IOException If an I/O error occurs.
	 */
	public static void copyAll(Reader reader, Writer writer, int bufferSize) throws IOException
	{
		char[] buffer = new char[bufferSize];

		int len = -1;
		while ((len = reader.read(buffer)) != -1)
		{
			writer.write(buffer, 0, len);
			writer.flush();
		}
	}
}
