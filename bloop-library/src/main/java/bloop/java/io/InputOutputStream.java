/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.java.io;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * A wrapper for an {@link InputStream} and an {@link OutputStream} that
 * provides methods to manage them like a single bidirectional stream.
 * 
 * @see InputStream
 * @see OutputStream
 * @author Benjamin Billet
 * @version 0.1
 */
public class InputOutputStream implements Closeable, Flushable
{
	private InputStream input;
	private OutputStream output;
	
	/**
	 * Creates a new {@code InputOutputStream} backed with an
	 * {@link InputStream} and an {@link OutputStream}.
	 */
	public InputOutputStream(InputStream input, OutputStream output)
	{
		this.input = input;
		this.output = output;
	}

	/**
	 * Returns the {@link InputStream} wrapped into this bidirectional stream.
	 */
	public InputStream getInputStream()
	{
		return input;
	}

	/**
	 * Returns the {@link OutputStream} wrapped into this bidirectional stream.
	 */
	public OutputStream getOutputStream()
	{
		return output;
	}

	/**
	 * @see InputStream#read()
	 */
	public int read() throws IOException
	{
		return input.read();
	}

	/**
	 * @see InputStream#read(byte[])
	 */
	public int read(byte[] bytes) throws IOException
	{
		return input.read(bytes);
	}
	
	/**
	 * @see InputStream#read(byte[], int, int)
	 */
	public int read(byte[] bytes, int offset, int length) throws IOException
	{
		return input.read(bytes, offset, length);
	}
	
	/**
	 * @see InputStream#skip(long)
	 */
	public long skip(long n) throws IOException
	{
		return input.skip(n);
	}

	/**
	 * @see InputStream#available()
	 */
	public int available() throws IOException
	{
		return input.available();
	}

	/**
	 * @see InputStream#reset()
	 */
	public void mark(int readlimit)
	{
		input.mark(readlimit);
	}

	/**
	 * @see InputStream#mark(int)
	 */
	public void reset() throws IOException
	{
		input.reset();
	}

	/**
	 * @see InputStream#markSupported()
	 */
	public boolean markSupported()
	{
		return input.markSupported();
	}

	/**
	 * @see OutputStream#write(int)
	 */
	public void write(int b) throws IOException
	{
		output.write(b);
	}

	/**
	 * @see OutputStream#write(byte[])
	 */
	public void write(byte[] bytes) throws IOException
	{
		output.write(bytes);
	}

	/**
	 * @see OutputStream#write(byte[], int, int)
	 */
	public void write(byte[] bytes, int offset, int length) throws IOException
	{
		output.write(bytes, offset, length);
	}
	
	/**
	 * Writes the content of a {@link ByteArrayOutputStream} into this stream.
	 * @throws IOException If an I/O error occurs.
	 * @see ByteArrayOutputStream
	 */
	public void write(ByteArrayOutputStream stream) throws IOException
	{
		stream.writeTo(output);
	}

	/**
	 * @see OutputStream#flush()
	 */
	@Override
	public void flush() throws IOException
	{
		output.flush();
	}

	/**
	 * Closes the internal {@link InputStream} and {@link OutputStream}.
	 * @throws IOException If an I/O error occurs.
	 * @see InputStream#close()
	 * @see OutputStream#close()
	 */
	@Override
	public void close() throws IOException
	{
		try
		{
			if (input != null)
				input.close();
		}
		finally
		{
			if (output != null)
				output.close();
		}
	}
}
