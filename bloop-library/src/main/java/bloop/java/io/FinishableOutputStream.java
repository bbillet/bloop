/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
//package bloop.java.io;
//
//import java.io.IOException;
//import java.io.OutputStream;
//
///**
// * A {@code FinishableOutputStream} is a type of {@link FilterOutputStream} that
// * can be finished (i.e., virtually closed) without closing the underlying
// * stream (e.g., in the case of reusable streams).<br>
// * Invoking {@link FinishableOutputStream#finish()} virtually closes the stream
// * (e.g., by writing a termination sequence) while invoking
// * {@link FinishableOutputStream#close()} physically closes the underlying
// * stream.<br>
// * <br>
// * The expected behavior of a {@link FinishableOutputStream} SHOULD respect the
// * following rules:
// * <ul>
// * <li>{@link FinishableOutputStream#finish()} can be called only once, and
// * additional calls must be ignored
// * <li>{@link FinishableOutputStream#close()} automatically calls
// * {@link FinishableOutputStream#finish()} before closing the underlying stream.
// * </ul>
// * 
// * @author Benjamin Billet
// */
//public abstract class FinishableOutputStream extends OutputStream
//{
//	private boolean finished = false;
//
//	/**
//	 * Finishes this output stream.<br>
//	 * Subclasses that overloads this method must call {@code super.finish()} at
//	 * the end of their body.
//	 * @exception IOException if an I/O error occurs.
//	 */
//	public void finish() throws IOException
//	{
//		finished = true;
//	}
//
//	public boolean isFinished()
//	{
//		return finished;
//	}
//
//	/**
//	 * Finishes and closes this output stream and releases any system resources
//	 * associated with the stream.<br>
//	 * Subclasses that overloads this method must call {@code super.close()} at
//	 * the end of their body.
//	 * @exception IOException if an I/O error occurs.
//	 */
//	@Override
//	public void close() throws IOException
//	{
//		finish();
//	}
//}
