/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.recycling;

/**
 * A skeletal implementation for {@link Recyclable}, introducing methods for
 * checking whether the object is recycled:
 * <ul>
 * <li>When {@link Recyclable#recycle(Recycler)} is called, the {@code recycle}
 * bit is set to 1.
 * <li>When {@link Recyclable#fill(Object...)} is called, the {@code recycle}
 * bit is set to 0.
 * <li>{@link Recyclable#isRecycled()} returns the current status of the
 * {@code recycle} bit.
 * </ul>
 * To use this skeletal implementation, you must override
 * {@link AbstractRecyclable#fill()}. Do not forget to call {@code super.fill()}
 * in your implementation.
 * 
 * <pre>{@code
 * public class SomeRecyclable extends AbstractRecyclable
 * {
 * 	public String stringAttribute;
 * 
 * 	...
 * 
 * 	public void fill(Object... arguments)
 * 	{
 * 		super.fill(arguments);
 * 		if(arguments.length == 1)
 * 			stringAttribute = (String) arguments[0];
 * 		else
 * 			throw new IllegalArgumentException();
 * 	}
 * };
 * }</pre>
 * 
 * @author Benjamin Billet
 * @version 0.1
 * @see Recyclable
 */
public abstract class AbstractRecyclable implements Recyclable
{
	private boolean recycleBit;

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <T extends Recyclable> void recycle(Recycler<T> recycler)
	{
		recycler.recycle((T) this);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isRecycled()
	{
		return recycleBit;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setRecycled()
	{
		recycleBit = true;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void fill(Object... arguments)
	{
		recycleBit = false;
	}
}
