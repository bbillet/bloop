/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.recycling;

/**
 * A recycler interface, for managing a pool of {@link Recyclable} objects.
 * 
 * @author Benjamin Billet
 * @param <T> The type of {@link Recyclable} that can be managed by this
 *        recycler.
 * @version 0.1
 * @see Recyclable
 */
public interface Recycler<T extends Recyclable>
{
	/**
	 * Recycles a {@link Recyclable} object and stores it in the recycle pool.
	 * If the pool is full, the {@code recyclable} may be ignored. In any case,
	 * the {@code recyclable} MUST be considered as freed and MUST NOT be used
	 * anymore.
	 * @param recyclable The recyclable object.
	 * @throws NullPointerException If {@code recyclable} is null.
	 */
	public void recycle(T recyclable);
	
    /**
	 * Recycles several {@link Recyclable} object and stores them in the recycle
	 * pool. If the pool is full, some or all the objects may be ignored. In any
	 * case, the {@code recyclables} MUST be considered as freed and MUST NOT be
	 * used anymore.
	 * @param recyclables The recyclable objects.
	 * @throws NullPointerException If an element of {@code recyclables} is
	 *         null.
	 */
	public void recycleAll(Iterable<T> recyclables);
	
	/**
	 * Returns a {@link Recyclable} object from the recycle pool. The returned
	 * object is removed from the pool. If the pool is empty, {@code null} is
	 * returned.
	 * @param arguments The arguments used to fill the recycled object.
	 * @return The recycled object, or null if this pool is empty.
	 * @see Recyclable#recycle(Object...)
	 */
	public T get(Object... arguments);
	
	/**
	 * Returns a {@link Recyclable} object from the recycle pool. The returned
	 * object is removed from the pool. If the pool is empty, a new instance is
	 * created using the default constructor.
	 * @param arguments The arguments used to fill the recycled object.
	 * @return The recycled object, or a new instance if the pool is empty.
	 * @throws RuntimeException If the class cannot be instantiated.
	 * @see Recyclable#fill(Object...)
	 */
	public T getOrCreate(Object... arguments);

    /**
     * Returns the size of this recycler's pool.
     */
	public int getSize();
	
    /**
     * Checks if this recycler's pool is empty.
     * @return {@code true} is the pool is empty, {@code false} otherwise.
     */
	public boolean isEmpty();
	
    /**
     * Returns the maximum number of {@link Recyclable} that can be stored in
     * this recycler's pool.
     */
	public int getLimit();
	
    /**
     * Removes all the {@link Recyclable} objects managed by this pool.
     */
	public void clear();
}
