/*  
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.recycling;

/**
 * A recyclable object can be reused to avoid allocating a new object.
 * Typically, recyclable objects are managed by a {@link Recycler}.
 * <b>Warning</b>: An implementation of {@link Recyclable} MUST provide a
 * default constructor (a constructor without arguments) in order to be managed
 * by a {@link Recycler}.
 * 
 * @author Benjamin Billet
 * @version 0.1
 * @see Recycler
 */
public interface Recyclable
{
	/**
	 * Fills this recyclable object with new data. This method SHOULD behave
	 * like a constructor by re-initializing each attribute of this object.
	 * @param arguments The arguments used to initialize this object.
	 * @throws IllegalArgumentException If the arguments are invalid for this
	 *         recyclable.
	 * @throws ClassCastException If the type of one argument is invalid.
	 */
	public void fill(Object... arguments);
	
	/**
	 * Uses a {@link Recycler} to recycle this object. Once this method returns,
	 * this object MUST be considered as freed and MUST NOT be used anymore.
	 * @param recycler The recycler.
	 */
	public <T extends Recyclable> void recycle(Recycler<T> recycler);
	
	/**
	 * Returns {@code true} if this object was recycled, {@code false}
	 * otherwise.
	 */
	public boolean isRecycled();
	
	/**
	 * Marks this object as recycled. Typically, you should not call this method
	 * directly.
	 */
	public void setRecycled();
}
