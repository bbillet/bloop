/*
 * The Bloop Library.
 * Copyright (c) 2013 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.recycling;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Default implementation of the {@link Recycler} interface. <b>Warning</b>: The
 * type of {@link Recyclable} managed by this recycler MUST provide a default
 * constructor (a constructor without arguments) in order to be managed by a
 * {@link Recycler}.
 * 
 * @author Benjamin Billet
 * @param <T> The type of {@link Recyclable} that can be managed by this
 *        recycler.
 * @version 0.1
 * @see Recyclable
 * @see Recycler
 */
public class SimpleRecycler<T extends Recyclable> implements Recycler<T>
{
	private Deque<T> recyclables;
	private Constructor<T> constructor;
	private int limit;
	
	/**
	 * Creates a new {@link Recycler}, limited to {@link Integer#MAX_VALUE}
	 * elements.
	 * @param recyclableClass The actual class of the recycled objects.
	 * @throws IllegalArgumentException If {@code max} is negative or zero, or
	 *         if the recyclable class does not have a default constructor.
	 */
	public SimpleRecycler(Class<T> recyclableClass)
	{
		this.limit = Integer.MAX_VALUE;
		this.recyclables = new ArrayDeque<T>();
		this.constructor = getConstructor(recyclableClass);
	}

	/**
	 * Creates a new {@link Recycler}.
	 * @param recyclableClass The actual class of the recycled objects.
	 * @param limit The maximum number of {@link Recyclable} in this pool.
	 * @throws IllegalArgumentException If {@code max} is negative or zero, or
	 *         if the recyclable class does not have a default constructor.
	 */
	public SimpleRecycler(Class<T> recyclableClass, int limit)
	{
		if (limit <= 0)
			throw new IllegalArgumentException("max is negative or zero");

		this.limit = limit;
		this.recyclables = new ArrayDeque<T>(limit);
		this.constructor = getConstructor(recyclableClass);
	}

	/**
	 * {@inheritDoc}
	 */
	public void recycle(T object)
	{
		if (object.isRecycled() == false && recyclables.size() < limit)
		{
			object.setRecycled();
			recyclables.add(object);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void recycleAll(Iterable<T> objects)
	{
		for (T object : objects)
		{
			if (object.isRecycled() == false)
			{
				if (recyclables.size() < limit)
				{
					object.setRecycled();
					recyclables.add(object);
				}
				else
					break;
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public T get(Object... arguments)
	{
		if (recyclables.size() != 0)
		{
			T object = recyclables.pollLast();
			object.fill(arguments);
			return object;
		}
		else
			return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public T getOrCreate(Object... arguments)
	{
		try
		{
			T obj = get(arguments);
			if (obj != null)
				return obj;
			
			obj = constructor.newInstance();
			obj.fill(arguments);
			return obj;
		}
		catch (ReflectiveOperationException e)
		{
			throw new RuntimeException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public int getSize()
	{
		return recyclables.size();
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isEmpty()
	{
		return recyclables.isEmpty();
	}

	/**
	 * {@inheritDoc}
	 */
	public int getLimit()
	{
		return limit;
	}

	/**
	 * {@inheritDoc}
	 */
	public void clear()
	{
		recyclables.clear();
	}

	private Constructor<T> getConstructor(Class<T> recyclableClass)
	{
		try
		{
			Constructor<T> constructor = recyclableClass.getDeclaredConstructor();
			if (Modifier.isPublic(constructor.getModifiers()) == false)
				constructor.setAccessible(true);

			return constructor;
		}
		catch (ReflectiveOperationException e)
		{
			throw new IllegalArgumentException(e);
		}
	}
}
