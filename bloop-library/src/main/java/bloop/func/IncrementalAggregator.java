/*  
 * The Bloop Library.
 * Copyright (c) 2015 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.func;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * An incremental aggregator, which aggregates a set of iterables into a single
 * result.<br>
 * For example, the following code sums up a list of integers:
 * 
 * <pre>{@code
 * IncrementalAggregationFunction<Integer, Integer> sumFunc = new IncrementalAggregationFunction()
 * {
 * 	public Integer aggregate(Integer element, Integer currentResult)
 * 	{
 * 		return currentResult + element;
 * 	}
 * };
 * 
 * List<Integer> integers = ...;
 * int sum = new IncrementalAggregator<>(integers, sumFunc).aggregate(0);
 * }</pre>
 * 
 * @author Benjamin Billet
 * @version 0.1
 * @see IncrementalAggregationFunction
 * @param <E>
 * @param <R>
 */
public class IncrementalAggregator<E, R>
{
	private List<Iterable<E>> iterables;
	private IncrementalAggregationFunction<E, R> function;
	
	public IncrementalAggregator(IncrementalAggregationFunction<E, R> function)
	{
		this.iterables = new ArrayList<>();
		this.function = function;
	}
	
	public IncrementalAggregator(Iterable<E> iterable, IncrementalAggregationFunction<E, R> function)
	{
		this(function);
		this.iterables.add(iterable);
	}
	
	public IncrementalAggregator(Collection<? extends Iterable<E>> iterables, IncrementalAggregationFunction<E, R> function)
	{
		this(function);
		this.iterables.addAll(iterables);
	}
	
	/**
	 * Adds a set of iterables into this aggregator.
	 */
	@SuppressWarnings("unchecked")
	public void add(Iterable<E>... iterables)
	{
		this.iterables.addAll(Arrays.asList(iterables));
	}
	
	/**
	 * Adds a set of iterables into this aggregator.
	 */
	public void addAll(Collection<? extends Iterable<E>> iterables)
	{
		this.iterables.addAll(iterables);
	}
	
	/**
	 * Adds an iterable into this aggregator.
	 */
	public void add(Iterable<E> iterable)
	{
		iterables.add(iterable);
	}
	
	/**
	 * Computes incrementally the aggregation of the values stored into this
	 * aggregator, starting from an initial state.
	 * @param initialState The initial state for the incremental aggregation.
	 * @return The result of the aggregation.
	 */
	public R aggregate(R initialState)
	{
		R result = initialState;
		for(Iterable<E> iterable : iterables)
		{
			for(E element : iterable)
			{
				result = function.aggregate(element, result);
			}
		}
		return result;
	}
}
