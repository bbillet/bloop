/*  
 * The Bloop Library.
 * Copyright (c) 2015 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.func;

import java.util.Collection;

/**
 * An aggregation function prototype.
 * For example, the following code concatenates a set of strings:
 * 
 * <pre>{@code
 * AggregationFunction<String, String> func = new AggregationFunction<String, String>()
 * {
 * 	public String aggregate(String... elements)
 * 	{
 * 		return aggregate(Arrays.asList(elements));
 * 	}
 * 	public String aggregate(Collection<String> elements)
 * 	{
 * 		StringBuilder builder = new StringBuilder();
 * 		for(String string : elements)
 * 		{
 * 			builder.append(string);
 * 		}
 * 		return builder.toString();
 * 	}
 * };
 * 
 * String concatenated = func.aggregate("abcde", "fghijk");
 * }</pre>
 * 
 * @author Benjamin Billet
 * @version 0.1
 * @param <E> The type of elements to be aggregated.
 * @param <R> The type of the result.
 */
public interface AggregationFunction<E, R>
{
	/**
	 * Applies the aggregation function to a set of elements.
	 * @param elements The set of elements to aggregate.
	 * @return The computed aggregate.
	 */
	@SuppressWarnings("unchecked")
	public R aggregate(E... elements);
	
	/**
	 * Applies the aggregation function to a set of elements.
	 * @param elements The set of elements to aggregate.
	 * @return The computed aggregate.
	 */
	public R aggregate(Collection<E> elements);
}
