/*  
 * The Bloop Library.
 * Copyright (c) 2015 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.func;

/**
 * An incremental aggregation function prototype.<br>
 * For example, the following code sums up a set of integers:
 * 
 * <pre>{@code
 * IncrementalAggregationFunction<Integer, Integer> sumFunc = new IncrementalAggregationFunction()
 * {
 * 	public Integer aggregate(Integer element, Integer currentResult)
 * 	{
 * 		return currentResult + element;
 * 	}
 * };
 * 
 * int[] values = ...;
 * int sum = 0;
 * for(int value : values)
 * {
 * 	sum = sumFunc.aggregate(value, sum);
 * }
 * }</pre>
 * 
 * @author Benjamin Billet
 * @version 0.1
 * @param <E> The type of elements to be aggregated.
 * @param <R> The type of the result.
 */
public interface IncrementalAggregationFunction<E, R>
{
	/**
	 * Computes a step of the aggregation by updating an existing aggregate with
	 * a new element.
	 * @param element The element to aggregate.
	 * @param currentResult The existing aggregate to update.
	 * @return The aggregate updated with the new element.
	 */
	public R aggregate(E element, R currentResult);
}
