/*  
 * The Bloop Library.
 * Copyright (c) 2015 Benjamin Billet.
 * http://benjaminbillet.fr/wiki/doku.php?id=bloop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package bloop.func;

/**
 * Interface of an adapter function, which adapts objects of type T1 into
 * objects of type T2.
 * @author Benjamin Billet
 * @version 0.1
 *
 * @param <T1> The type of the function input.
 * @param <T2> The type of the function output.
 */
public interface AdaptFunction<T1, T2>
{
	public T2 adapt(T1 element);
}